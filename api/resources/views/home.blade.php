@extends('layouts.app')

@section('content')
    <div class="row">
        <!--NOTIFICATIONS-->
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h3>Notifications</h3></div>
                <div class="panel-body">

                    @if(count($user->unreadNotifications) == 0)
                        <p>There is no new notifications!</p>
                    @else
                        @foreach($user->unreadNotifications as $notification)
                            <b>Message: </b>{{$notification->data['proposal']['message']}}
                            <b>User: </b> {{$notification->data['user']['name']}} <b>Email:
                            </b> {{$notification->data['user']['email']}}
                            {!! link_to_route('work-show', '(View)', [$notification->data['proposal']['work_id']]) !!}
                        @endforeach
                    @endif
                </div>
            </div>
        </div>


        <!--ABILITIES-->
        @if (count($user->ability) === 0)
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-warning">
                    <div class="panel-heading"><h3>Abilities</h3></div>
                    <div class="panel-body">
                        <p>No abilities assigned to your profile. Please add new skills {!! link_to_route('user-abilities', 'here', [$user->id]) !!}.</p>
                    </div>
                </div>
            </div>
        @endif

        <!--LOCATION-->
        @if (count($user->location) === 0)
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-warning">
                    <div class="panel-heading"><h3>Location</h3></div>
                    <div class="panel-body">
                        <p>No location defined on your profile. Please add your location {!! link_to_route('user-location', 'here', [$user->id]) !!}.</p>
                    </div>
                </div>
            </div>
        @endif

        <!--DOCUMENTS-->
        @if (count($user->document) === 0)
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-warning">
                    <div class="panel-heading"><h3>Documents</h3></div>
                    <div class="panel-body">
                        <p>No documents assigned to your profile. Please add new documents {!! link_to_route('user-documents', 'here', [$user->id]) !!}.</p>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection
