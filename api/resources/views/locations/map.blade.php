@extends('layouts.app')
@section('content')

<div class="container">
	<div class="well" style="background-color: white">
		<div class="page-header">
			<h1>Location <small><a href="#"></a></small></h1>
		</div>
    <div style="width:100%;height:350px;background-color:#f4f4f4;">
      {!! Mapper::render() !!}
    </div>
    <br><input class="btn btn-primary" type="submit" value="Submit" id="submit"><br>
	</div>
	<a href="{{ route('profile') }}" class="btn btn-default">Go Back</a><br><br>
</div>

<script type="text/javascript">

            dynamicallyCreatedMarkers = [];

            function addMarkerListener(map)
            {
                map.addListener('click', function(e) {
                    var marker = new google.maps.Marker({
                        position: e.latLng,
                        map: map
                    });

                    map.panTo(e.latLng);

                    dynamicallyCreatedMarkers.push({
                        position: e.latLng
                    });
                });
            }

            $('#submit').click(function() {
                $.ajax({
                    type: "POST",
                    url: '/holaMundoNuevo',
                    data: dynamicallyCreatedMarkers.serialize(),
                    success: function(response)
                    {
                        console.log('success')
                    }
										console.log('error')
                });
            });

        </script>
@endsection
