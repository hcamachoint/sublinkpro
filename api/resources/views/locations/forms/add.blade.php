{!!Form::hidden('able', $j, ['class' => 'form-control'])!!}
{!!Form::select('city', $cities, null, ['class' => 'form-control', 'required'=>'true', 'placeholder' => 'Select a city'])!!}
{!!Form::textarea('address', null, ['class'=>'form-control', 'placeholder'=>'Address','maxlength' => 50])!!}
{!!Form::text('zip', null, ['class'=>'form-control', 'placeholder'=>'Zip', 'required'=>'true','maxlength' => 50])!!}
{!!Form::button('<i class="glyphicon glyphicon-plus"></i> Save', ['class'=>'btn btn-success','type' => 'submit'])!!}
