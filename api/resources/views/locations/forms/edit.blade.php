{!!Form::hidden('able', $j, ['class' => 'form-control'])!!}
{!!Form::hidden('location_id', $user->location->id, ['class' => 'input-block-level'])!!}

{!!Form::label('City')!!}
{!!Form::select('city', $cities, $user->location->city_id, ['class' => 'form-control', 'required'=>'true'])!!}
<br>
{!!Form::label('Address')!!}
{!!Form::text('address', $user->location->address, ['class'=>'form-control', 'placeholder'=>'Address', 'maxlength' => 20])!!}
<br>
{!!Form::label('Zip')!!}
{!!Form::text('zip', $user->location->zip, ['class'=>'form-control', 'placeholder'=>'Zip', 'required'=>'true', 'maxlength' => 20])!!}
<br>
{!!Form::button('<i class="glyphicon glyphicon-refresh"></i> Save', ['class'=>'btn btn-success', 'type' => 'submit'])!!}
