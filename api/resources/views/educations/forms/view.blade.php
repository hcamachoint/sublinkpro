@foreach($user->education as $edu)
      <p class="lead"><b>Degree:</b> {{$edu->degree}}</p>
      <p class="lead"><b>School:</b> {{$edu->school}}</p>
      <p class="lead"><b>Period:</b> {{$edu->perioda}}/{{$edu->periodb}}</p>
      <p class="lead"><b>Active:</b> {{$edu->active}}</p>
      <p class="lead"><b>Area:</b> {{$edu->area}}</p>
      <p class="lead"><b>Description:</b> {{$edu->description}}</p>
      <br>
@endforeach
