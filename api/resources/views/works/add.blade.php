@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="well" style="background-color: white">
			{!!Form::open(['route'=>'work-store', 'method' => 'post'])!!}

				{!!Form::label('Project Pay Structure')!!}
				{!!Form::select('type', [1 => 'Piece Work', 2 => 'Hourly'], null, ['class' => 'form-control'])!!}<br>

				{!!Form::label('Project Budget ')!!}
				{!!Form::number('budget', null, ['class'=>'form-control', 'step'=>'any', 'placeholder'=>'Amount', 'required'=>'true'])!!}<br>

				{!!Form::label('Project Start Date')!!}<br>
				{!!Form::date('startd', \Carbon\Carbon::now(), ['class'=>'form-control'])!!}<br>

				{!!Form::label('Length of Project')!!}<br>
				{!!Form::select('esttime', [1 => 'One day', 5 => 'One Week', 15 => 'Half month', 30 => 'A month'], null, ['class'=>'form-control', 'required'=>'true'])!!}<br>

				{!!Form::label('Category')!!}
				{!!Form::select('category_id', $workcat, null, ['class' => 'form-control'])!!}<br>

				{!!Form::label('Difficulty at work')!!}
				{!!Form::select('level_id', $levcat, null, ['class' => 'form-control'])!!}<br>

				{!!Form::label('Proyect Type')!!}
				{!!Form::select('project_id', $procat, null, ['class' => 'form-control'])!!}<br>

				{!!Form::label('Personal Requireds to the work')!!}
				{!!Form::select('requireds', [1 => '1', 2 => '2', 3 => '3', 4 => '4', 5 => '5', 6 => '6', 7 => '7', 8 => '8', 9 => '9', 10 => '10'], null, ['class' => 'form-control'])!!}<br>

				{!!Form::label('Title')!!}
				{!!Form::text('title', null, ['class'=>'form-control', 'placeholder'=>'Title', 'required'=>'true', 'maxlength' => 100])!!}<br>

				{!!Form::label('Description')!!}
				{!!Form::textarea('description', null, ['class'=>'form-control', 'placeholder'=>'Description', 'required'=>'true'])!!}<br>

				{!!Form::button('<i class="glyphicon glyphicon-plus"></i> Save', ['class'=>'btn btn-success','type'=>'submit'])!!}
			{!!Form::close()!!}
		</div>
	</div>
@endsection
