@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="well" style="background-color: white">
			<div class="page-header">
				<h1>
					Contact Method Post
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
		              <i class="glyphicon glyphicon-plus-sign"> Add</i>
		            </button>
		            <!-- Modal -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            {!!Form::open(['route'=> ['contact-store', $work->id], 'method' => 'post'])!!}
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h3 class="modal-title" id="myModalLabel">New Contact method</h3>
                                </div>
                                <div class="modal-body">
                                    @include('contacts.forms.add')<br>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            {!!Form::close()!!}
                        </div>
                      </div>
                    </div>
				</h1>

			</div>
			@if (count($work->contact) === 0)
				<p>There is no data associated!</p>
			@else
				@foreach($work->contact as $cont)
					<h3>{{$cont->category->description}}</h3>
				  	{!!Form::open(['route'=>['contact-update', $cont->id], 'method' => 'put'])!!}
						@include('contacts.forms.edit')
					{!!Form::close()!!}
					{!!Form::open(['route'=>['contact-destroy', $cont->id], 'method' => 'delete'])!!}
						@include('contacts.forms.del')
					{!!Form::close()!!}
				@endforeach
			@endif




			<br>
		</div>
		<a href="{{ route('work-show', $work->id) }}" class="btn btn-default">Go Back</a>
	</div>
	<br>

@endsection
