@extends('layouts.app')

@section('content')
	<div class="container">
		<h1>Contact Method 3/4</h1>
		<div class="well" style="background-color: white">
			{!!Form::open(['route'=>'work-create4', 'method' => 'post'])!!}

				{!!Form::checkbox('email', true, false)!!}
				{!!Form::label('Registered Email')!!}<br>

				{!!Form::text('emaila', null, ['class'=>'form-control', 'placeholder'=>'Alternative Email', 'maxlength' => 100])!!}<br>

				{!!Form::text('phone', null, ['class'=>'form-control', 'placeholder'=>'Mobile Phone', 'maxlength' => 100])!!}<br>

				<br>{!!Form::button('<i class="glyphicon glyphicon-arrow-right"></i> Next', ['class'=>'btn btn-success','type'=>'submit'])!!}
			{!!Form::close()!!}
		</div>
	</div>
@endsection
