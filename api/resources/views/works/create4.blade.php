@extends('layouts.app')

@section('head')
	  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection

@section('content')
	<div class="container">
		<h1>Budget and Confirmation 4/4</h1>
		{!!Form::open(['route'=>'work-store', 'method' => 'post'])!!}

			<div class="well" style="background-color: white">
				<h3>Pay Structure</h3>
				<div class="btn-group" data-toggle="buttons">
				  <label class="btn btn-primary active">
				    <input type="radio" name="pstructure" id="option1" autocomplete="off" value="1" checked> Piece Work
				  </label>
				  <label class="btn btn-primary">
				    <input type="radio" name="pstructure" id="option2" autocomplete="off" value="2"> Hourly
				  </label>
				</div>

				<br><br>{!!Form::number('budget', null, ['class'=>'form-control', 'step'=>'any', 'placeholder'=>'Amount', 'required'=>'true'])!!}
			</div>

			<div class="well" style="background-color: white">
				<h3>Project Start Date</h3>
				{!!Form::date('startd', \Carbon\Carbon::now(), ['class'=>'date form-control'])!!}<br>

				<h3>Project Length</h3>
				<div class="btn-group" data-toggle="buttons">
				  <label class="btn btn-primary active">
				    <input type="radio" name="texpect" id="option1" autocomplete="off" value="1" checked> One day
				  </label>
				  <label class="btn btn-primary">
				    <input type="radio" name="texpect" id="option2" autocomplete="off" value="7"> One Week
				  </label>
				  <label class="btn btn-primary">
				    <input type="radio" name="texpect" id="option2" autocomplete="off" value="15"> Half month
				  </label>
				  <label class="btn btn-primary">
				    <input type="radio" name="texpect" id="option2" autocomplete="off" value="30"> A month
				  </label>
				  <label class="btn btn-primary">
				    <input type="radio" name="texpect" id="option2" autocomplete="off" value="90"> More than one month
				  </label>
				</div>

				<br><br>
				<h3>Required Tools</h3>
				{!!Form::number('requireds', null, ['class'=>'form-control', 'placeholder'=>'For example: 1', 'required'=>'true','maxlength' => 2])!!}
			</div>


			{!!Form::button('<i class="glyphicon glyphicon-ok"></i> Save', ['class'=>'btn btn-success','type'=>'submit'])!!}
		{!!Form::close()!!}
	</div>
@endsection
@section('footer')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
    $('.date').datepicker({
       format: 'mm-dd-yyyy'
     });
</script>
@endsection
