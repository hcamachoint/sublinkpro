@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="well" style="background-color: white">
			<div class="page-header">
				<h1>Post Info</h1>
			</div>
			{!!Form::open(['route'=> ['work-update', $work->id], 'method' => 'put'])!!}

				{!!Form::label('Project Structure')!!}
				{!!Form::select('pstructure', [1 => 'For Work', 2 => 'For Hour'], $work->pstructure, ['class'=>'form-control', 'placeholder' => 'Seleccione un sexo'])!!}<br>

				{!!Form::label('Budget')!!}
				{!!Form::number('budget', $work->budget, ['class'=>'form-control', 'step'=>'any', 'placeholder'=>'Amount', 'required'=>'true'])!!}<br>

				{!!Form::label('Start date')!!}<br>
				{!!Form::date('startd', $work->startd, ['class'=>'form-control'])!!}<br>

				{!!Form::label('Time Expected on Days')!!}<br>
				{!!Form::text('texpect', $work->texpect, ['class'=>'form-control'])!!}<br>

				{!!Form::label('Personal Requireds to the work')!!}
				{!!Form::number('requireds', $work->requireds, ['class'=>'form-control', 'placeholder'=>'For example: 1', 'required'=>'true','maxlength' => 2])!!}<br>

				{!!Form::label('Project Category')!!}
				{!!Form::select('category', $workcat, $work->category_id, ['class' => 'form-control'])!!}<br>

				{!!Form::label('Proyect Type')!!}
				{!!Form::select('project', $procat, $work->project_id, ['class' => 'form-control'])!!}<br>

				{!!Form::label('Proyect Level')!!}
				{!!Form::select('level', $levcat, $work->level_id, ['class' => 'form-control'])!!}<br>

				{!!Form::label('Title')!!}
				{!!Form::text('title', $work->title, ['class'=>'form-control', 'placeholder'=>'Title', 'required'=>'true', 'maxlength' => 100])!!}<br>

				{!!Form::label('Description')!!}
				{!!Form::textarea('description', $work->description, ['class'=>'form-control', 'placeholder'=>'Description', 'required'=>'true'])!!}<br>

				{!!Form::button('<i class="glyphicon glyphicon-plus"></i> Save', ['class'=>'btn btn-warning','type'=>'submit'])!!}
			{!!Form::close()!!}
		</div>
	</div>

	<div class="container">
		<div class="well" style="background-color: white">
			<div class="page-header">
				<h1>Skills</h1>
			</div>
				@if (count($work->ability) === 0)
					<p>There is no data associated!</p>
				@else
					@foreach($work->ability as $ab)
						{!!Form::open(['route'=> ['ability-destroy', $ab->id], 'method' => 'delete'])!!}
							<li>
								{{$ab->category->description}}
								{!!Form::hidden('able', $j, ['class' => 'form-control'])!!}
								{!!Form::button('<i class="glyphicon glyphicon-trash"></i> ', ['type'=>'submit'])!!}
							</li>
						{!!Form::close()!!}
					@endforeach
				@endif
				<br>
				{!!Form::open(['route'=> ['ability-store', $work->id], 'method' => 'post'])!!}
					@include('abilities.forms.add')
				{!!Form::close()!!}
			<br>
		</div>
		<a href="{{ route('work-show', $work->id) }}" class="btn btn-default">Go Back</a>
		<br><br>
	</div>

@endsection
