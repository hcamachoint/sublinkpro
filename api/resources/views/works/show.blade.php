@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="well" style="background-color: white">
		    <div class="page-header">
				<h1>{{$work->title}}
					@if($work->status === 1)
						<small><a href="/work/edit/{{$work->id}}"><span class="glyphicon glyphicon-pencil"></span></a></small>
					@endif
				</h1>
				<b>Amount:</b> {{$work->budget}} @if($work->pstructure == 1) $ @else $/hr @endif |
				<b>Category:</b> {{$work->category->description}} |
				<b>To start at</b> {{$work->startd}} |
				<b>Published:</b> {{$work->created_at}} |
				@if($work->user->id === Auth::user()->id) <b>You</b> @else {{$work->user->name1}} @endif
			</div>
			<div class="container-fluid">
				<p>{{$work->description}}</p>
				<p><b>Project Type: {{$work->project->description}}</b></p>
                <p><b>Level: {{$work->level->description}}</b></p>
                <p><b>Requireds: {{$work->requireds}}</b></p>
                <b>
					@if($work->status === 0)
        				<p>Ended</p>
        			@elseif($work->status === 1)
        				<p>Active</p>
        			@else
        				<p>Active Full</p>
        			@endif
				</b>
				@if($work->ability != '[]')
					<b>Required skills:</b>
						@foreach($work->ability as $ab)
						    <li>{{$ab->category->description}}</li>
						@endforeach
				@endif
			</div>
		</div>

		<div class="well" style="background-color: white">
			<div class="page-header">
				<h1>Contact Methods <small><a href="/work/contact/edit/{{$work->id}}"><span class="glyphicon glyphicon-pencil"></span></a></small></h1>
			</div>
				@if (count($work->contact) === 0)
			        <p>There is no data associated!</p>
			    @else
		          	@foreach($work->contact as $cont)
					    <p class="lead"><b>{{$cont->category->description}}:</b> {{$cont->address}}</p>
					@endforeach
	        	@endif
			<br>
		</div>

		<div class="well" style="background-color: white">
			<div class="page-header">
	            <h1>Proposals</h1>
	        </div>
	        <div class="container-fluid">
	            @if (count($work->proposal) === 0)
			        <p>There are no proposals for this work yet!</p>
			    @else


	            	<table class="table table-striped">
					    <thead>
					      <tr>
					        <th>Username</th>
					        <th>Message</th>
					        <th>Email</th>
					        <!--<th>Action</th>-->
					      </tr>
					    </thead>
					    <tbody>
					    	@foreach($work->proposal as $pro)
						    	<tr>
						        	<td>{{$pro->user->name}}</td>
						        	<td>{{$pro->message}}</td>
						        	<td><a href="mailto:{{$pro->user->email}}">{{$pro->user->email}}</a></td>
						        	<!--<td>

						        		@if($pro->status === 1)
											Contacted
										@elseif($pro->status === 0)
											<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
											  Contact
											</button>
										@endif
										<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
										  <div class="modal-dialog" role="document">
										    <div class="modal-content">
										      <div class="modal-header">
										        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										        <h4 class="modal-title" id="myModalLabel">{{$pro->user->name}}</h4>
										      </div>
										      <div class="modal-body">
										        {!!Form::open(['route'=>['work-contact', $pro->user->id, $pro->id], 'method' => 'post'])!!}
													{!!Form::textarea('message', null, ['class'=>'form-control', 'placeholder'=>'Your message here', 'required'=>'true'])!!}<br>
										      </div>
										      <div class="modal-footer">
										        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										        {!!Form::button('<i class="glyphicon glyphicon-plus"></i> Add', ['class'=>'btn btn-success','type'=>'submit'])!!}
										      </div>
										      {!!Form::close()!!}
										    </div>
										  </div>
										</div>
						        	</td>-->
						      	</tr>
					    	@endforeach
					    </tbody>
					</table>
				@endif
	        </div>
	    </div>
	    @if($work->status === 1)
	    	<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
              Delete Job
            </button>
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {!!Form::open(['route'=> ['work-destroy', $work->id], 'method' => 'delete'])!!}
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{$work->title}}</h4>
                        </div>
                        <div class="modal-body">
                            <p>Are you sure you want to delete this Post?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            {!!Form::button('<i class="glyphicon glyphicon-trash"></i> Delete', ['class'=>'btn btn-danger', 'type'=>'submit'])!!}
                        </div>
                    {!!Form::close()!!}
                </div>
              </div>
            </div>
		@elseif($work->status === 2)
			<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
              Close Job
            </button>
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {!!Form::open(['route'=> ['work-close', $work->id], 'method' => 'put'])!!}
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{$work->title}}</h4>
                        </div>
                        <div class="modal-body">
                            <p>Are you sure you want to close this post?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            {!!Form::button('<i class="glyphicon glyphicon-chevron-down"></i> Close Job', ['class'=>'btn btn-danger', 'type'=>'submit'])!!}
                        </div>
                    {!!Form::close()!!}
                </div>
              </div>
            </div>
		@endif
		<br><a href="{{ route('works') }}" class="btn btn-default">Go Back</a><br><br>
	</div>
@endsection
