@extends('layouts.app')

@section('content')
	<div class="container">
		<h1>Project Features 2/4</h1>
		<div class="well" style="background-color: white">
			{!!Form::open(['route'=>'work-create3', 'method' => 'post'])!!}

				{!!Form::label('Title')!!}
				{!!Form::text('title', null, ['class'=>'form-control', 'placeholder'=>'Title', 'required'=>'true', 'maxlength' => 100])!!}<br>

				{!!Form::label('Description')!!}
				{!!Form::textarea('description', null, ['class'=>'form-control', 'placeholder'=>'Description', 'required'=>'true'])!!}<br>

				{!!Form::label('Skills Requireds')!!}<br>
				@if (count($abicat) === 0)
					<p>There is no data associated!</p>
				@else
					@foreach($abicat as $ac)
						{!!Form::checkbox('abilities[]', $ac->id, false)!!}
						{!!Form::label($ac->description)!!}
					@endforeach
				@endif

				<!--{!!Form::label('Address')!!}
				{!!Form::text('address', null, ['class'=>'form-control', 'placeholder'=>'Title', 'required'=>'true', 'maxlength' => 100])!!}<br>-->

				<br><br>{!!Form::button('<i class="glyphicon glyphicon-arrow-right"></i> Next', ['class'=>'btn btn-success','type'=>'submit'])!!}
			{!!Form::close()!!}
		</div>
	</div>
@endsection
