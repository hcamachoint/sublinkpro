@extends('layouts.app')

@section('content')
	<div class="container">
		<h1>Project Type 1/4</h1>
		<div class="well" style="background-color: white">
			{!!Form::open(['route'=>'work-create2', 'method' => 'post'])!!}

				<h3>Category</h3>
				@if (count($workcat) === 0)
					<p>There is no data associated!</p>
				@else
					@foreach($workcat as $wc)
						{!!Form::radio('category', $wc->id, true)!!}
						{!!Form::label($wc->description)!!}<br>
					@endforeach
				@endif

				<h3>Project</h3>
				@if (count($workcat) === 0)
					<p>There is no data associated!</p>
				@else
					@foreach($procat as $pc)
						{!!Form::radio('project', $pc->id, true)!!}
						{!!Form::label($pc->description)!!}<br>
					@endforeach
				@endif

				<h3>Level</h3>
				@if (count($levcat) === 0)
					<p>There is no data associated!</p>
				@else
					@foreach($levcat as $lc)
						{!!Form::radio('level', $lc->id, true)!!}
						{!!Form::label($lc->description)!!}<br>
					@endforeach
				@endif

				<br>{!!Form::button('<i class="glyphicon glyphicon-arrow-right"></i> Next', ['class'=>'btn btn-success','type'=>'submit'])!!}
			{!!Form::close()!!}
		</div>
	</div>
@endsection