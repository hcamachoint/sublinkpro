@extends('layouts.app')

@section('content')
	<div class="container">

		@if($mensaje === false)
			<div class="well" style="background-color: white">
				<div class="container">
					<h3>Add new work <a href="{{ route('work-create') }}" class="btn btn-primary"> <i class="glyphicon glyphicon-plus-sign"></i> Here</a></h3>
				</div>
			</div>
		@else
			<div class="well" style="background-color: white">
				<div class="container">
					<h3>You can not add more works, first complete the active works.</h3>
				</div>
			</div>
		@endif

		@if (count($works) !== 0) 
			@foreach($works as $work)
				<div class="well" style="background-color: white">
				    <div class="page-header">
						<h1><a href="#">{!! link_to_route('work-show', $work->title, [$work->id]) !!}</a></h1>
						<b>Amount:</b> {{$work->budget}} @if($work->pstructure == 1) $ @else $/hr @endif |
						<b>Category:</b> {{$work->category->description}} | 
						<b>To start at</b> {{$work->startd}} |
						<b>Published:</b> {{$work->created_at}} |
						@if($work->user->id === Auth::user()->id) <b>You</b> @else {{$work->user->name}} @endif |
						<b>
							@if($work->status === 0)
		        				<p>Ended</p>
		        			@elseif($work->status === 1)
		        				<p>Active</p>
		        			@else
		        				<p>Active Full</p>
		        			@endif
						</b>
					</div>
					<div class="container-fluid">
						<p>{{$work->description}}</p>
					</div>
				</div>
			@endforeach
		@else
			<div class="well" style="background-color: white">
			    <div class="page-header">
					<h1>You don't have works posteds</h1>
				</div>
				<div>
					<p>Create a new work.</p>
				</div>
			</div>
		@endif
	</div>
@endsection