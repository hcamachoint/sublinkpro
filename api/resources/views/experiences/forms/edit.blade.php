{!!Form::text('company', $exp->company, ['class'=>'form-control', 'placeholder'=>'Company Name', 'required'=>'true','maxlength' => 100])!!}
{!!Form::text('title', $exp->title, ['class'=>'form-control', 'placeholder'=>'Title', 'required'=>'true','maxlength' => 50])!!}
{!!Form::text('description', $exp->description, ['class'=>'form-control', 'placeholder'=>'Description', 'required'=>'true'])!!}
{!!Form::text('role', $exp->role, ['class'=>'form-control', 'placeholder'=>'Role', 'required'=>'true','maxlength' => 200])!!}
{!!Form::select('category_id', $jobcat, $exp->category_id, ['class' => 'form-control'])!!}
{!!Form::date('perioda', $exp->perioda, ['class'=>'form-control'])!!}
{!!Form::date('periodb', $exp->periodb, ['class'=>'form-control'])!!}
{!!Form::button('<i class="glyphicon glyphicon-plus"></i> Update', ['class'=>'btn btn-success','type'=>'submit'])!!}
