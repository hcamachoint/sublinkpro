@extends('layouts.app')

@section('content')
    <div class='container'>
        <div class="row">
        	<!--<div class='col-md-2'>
            	<div class="well">
            		<h3>Filters</h3>
	                <ul class="nav nav-tabs nav-stacked">
	                  <li><a href='#'>Categories</a></li>
	                  <li><a href='#'>Abilities</a></li>
	                  <li><a href='#'>Date</a></li>
	                </ul>
            	</div>
            </div>-->
            <div class="col-md-12">
                @if(count($jobs) >= 1)
                    @foreach($jobs as $job)
                        <div class="well" style="background-color: white">
                            <div class="page-header">
                                <h1><a href="#">{!! link_to_route('job-show', $job->title, [$job->id]) !!}</a></h1>
                                    <b>Amount:</b> {{$job->budget}} @if($job->pstructure == 1) $ @else $/hr @endif |
                                    <b>Category:</b> {{$job->category->description}} | 
                                    <b>To start at</b> {{$job->startd}} |
                                    <b>Published:</b> {{$job->created_at}} |
                                    @if($job->user->id === Auth::user()->id) <b>You</b> @else {{$job->user->name}} @endif |
                                    <b>
                                        @if($job->status === 0)
                                            <p>Ended</p>
                                        @elseif($job->status === 1)
                                            <p>Active</p>
                                        @else
                                            <p>Active Full</p>
                                        @endif
                                    </b>
                            </div>
                            <div>
                                <p>{{$job->description}}</p>
                            </div>
                        </div>
                    @endforeach
                    <div align="center">{!!$jobs->links()!!}</div>
                @else
                	<div class="well" style="background-color: white">
                        <div class="page-header">
                            <h1>No jobs founds</h1>
                        </div>
                        <div>
                            <p>Sorry, no jobs found.</p>
                        </div>
                    </div>
                @endif
                
            </div>
            <!--<div class='col-md-2'>
                <h3>Right Sidebar</h3>
                <ul class="nav nav-tabs nav-stacked">
                  <li><a href='#'>Another Link 1</a></li>
                  <li><a href='#'>Another Link 2</a></li>
                  <li><a href='#'>Another Link 3</a></li>
                </ul>
            </div>-->
        </div>
    </div>
@endsection