@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="well" style="background-color: white">
            <div class="page-header">
               <h1>{{$job->title}}</h1>
                <b>Amount:</b> {{$job->budget}} @if($job->pstructure == 1) $ @else $/hr @endif |
                <b>Category:</b> {{$job->category->description}} | 
                <b>To start at</b> {{$job->startd}} |
                <b>Published:</b> {{$job->created_at}} |
                @if($job->user->id === Auth::user()->id) <b>You</b> @else {{$job->user->name1}} @endif
            </div>
            <div class="container-fluid">
                <p>{{$job->description}}</p>
                <p><b>Project Type: {{$job->project->description}}</b></p>
                <p><b>Level: {{$job->level->description}}</b></p>
                <p><b>Requireds: {{$job->requireds}}</b></p>
                <p>
                    @if($job->ability != '[]')
                        <b>Required abilities:
                            @foreach($job->ability as $ab)
                                #{{$ab->category->description}} 
                            @endforeach
                        </b>
                    @endif
                </p>

                <p><b>Contact Info:</b>
                    @if (count($job->contact) === 0)
                    <p>There is no data associated!</p>
                @else
                    @foreach($job->contact as $cont)
                        <br>* {{$cont->category->description}} - {{$cont->address}}
                    @endforeach
                @endif
                </p>
                <!-- Button trigger modal -->
                @if($job->user_id === Auth::user()->id)

                @elseif(empty($prop))
                    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                      Proposal
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            {!!Form::open(['route'=> ['job-proposal', $job->id], 'method' => 'post'])!!}
                                {{ csrf_field() }}
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Send Proposal</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Leave a message</p>
                                    {!!Form::textarea('message', null, ['class'=>'form-control', 'placeholder'=>'Description', 'required'=>'true'])!!}
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Send</button>
                                </div>
                            {!!Form::close()!!}
                        </div>
                      </div>
                    </div>
                @elseif(isset($prop))
                    <div class="well" style="background-color: white">
                        <div class="page-header">
                            <h1>Your Proposal</h1>
                        </div>
                        <div class="container">
                            <p>{{$prop->message}}</p>                  
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection