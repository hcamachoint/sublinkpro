@extends('layouts.admin')

@section('content')
	<div class="row">
        <div class="col-lg-12">
          <div class="page-header">
            <h3>Social Categories <small>A quick view of your website stats</small></h3>
          </div>
        </div>
    </div>
	<div class="container-fluid">
        <div class="well">
            <div class="page-header">
                <h1>{{$soc->name}}</h1>
            </div>
            <div class="container-fluid">
                {!!Form::open(['route'=>['soc-update', $soc->id], 'method' => 'put'])!!}
                    {{ csrf_field() }}
                    {!!Form::label('Name')!!}
                    {!!Form::text('name', $soc->name, ['class'=>'form-control', 'placeholder'=>'Name', 'required'=>'true','maxlength' => 50])!!}
                    {!!Form::label('Description')!!}
                    {!!Form::text('description', $soc->description, ['class'=>'form-control', 'placeholder'=>'Description', 'required'=>'true'])!!}
										{!!Form::label('Url')!!}
                    {!!Form::text('url', $soc->url, ['class'=>'form-control', 'placeholder'=>'Example: https://twitter.com/', 'required'=>'true'])!!}
                    {!!Form::button('<i class="glyphicon glyphicon-pencil"></i> Save', ['class'=>'btn btn-success', 'type'=>'submit'])!!}
                {!!Form::close()!!}
                {!!Form::open(['route'=>['soc-delete', $soc->id],'method' => 'delete'])!!}
                    {{ csrf_field() }}
                    {!!Form::button('<i class="glyphicon glyphicon-trash"></i> Delete', ['class'=>'btn btn-danger', 'type'=>'submit'])!!}
                {!!Form::close()!!}
            </div>
            <br>
            <a href="{{ URL::previous() }}" class="btn btn-primary">Go Back</a>
        </div>
    </div>
@endsection
