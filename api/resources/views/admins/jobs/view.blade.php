@extends('layouts.admin')

@section('content')
	<div class="row">
        <div class="col-lg-12">
          <div class="page-header">
            <h3>Jobs <small>A quick view of your website stats</small></h3>
          </div>
        </div>
    </div>
	<div class="container-fluid">
        <div class="well">
            <div class="page-header">
                        <h1>{{$job->title}}</h1>
                        {{$job->budget}} @if($job->type == 1) $ @else $/hr @endif |
                        {{$job->category->description}} | 
                        {{$job->esttime}} days. |
                        Published: {{$job->created_at}} |
                        @if($job->user->id === Auth::user()->id) <b>You</b> @else {{$job->user->name}} @endif
            </div>
            <div class="container-fluid">
                <p>{{$job->description}}</p>
                <p><b>Project Type: {{$job->project->description}}</b></p>
                <p><b>Level: {{$job->level->description}}</b></p>
                <p><b>Requireds: {{$job->requireds}}</b></p>
                <b>
                    @if($job->status === 0)
                        <p>Ended</p>
                    @elseif($job->status === 1)
                        <p>Active</p>
                    @else
                        <p>Active Full</p>
                    @endif
                </b>
                <p>
                    @if($job->ability != '[]')
                        <b>Required abilities:
                            @foreach($job->ability as $ab)
                                <p>#{{$ab->category->description}}</p>
                            @endforeach
                        </b>
                    @endif
                </p>
                        <div class="row">
                            {!!Form::open(['route'=> ['job-close', $job->id], 'method' => 'post'])!!}
                                {{ csrf_field() }}
                                {!!Form::button('<i class="glyphicon glyphicon-chevron-down"></i> Close Job', ['class'=>'btn btn-warning', 'type'=>'submit'])!!}
                            {!!Form::close()!!}
                            {!!Form::open(['route'=> ['job-report', $job->id], 'method' => 'post'])!!}
                                {{ csrf_field() }}
                                {!!Form::button('<i class="glyphicon glyphicon-remove-sign"></i> Report Job', ['class'=>'btn btn-danger', 'type'=>'submit'])!!}
                            {!!Form::close()!!}
                        </div>
                    </div>
                    <br>
                    <a href="{{ URL::previous() }}" class="btn btn-primary">Go Back</a>
        </div>
    </div>
@endsection