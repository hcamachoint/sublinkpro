@extends('layouts.admin')

@section('content')
	<div class="row">
        <div class="col-lg-12">
          <div class="page-header">
            <h3>Jobs <small>A quick view of your website stats</small></h3>
          </div>
        </div>
    </div>
    @if (count($jobs) !== 0)
    	<table class="table">
        	<thead>
        		<tr>
        			<th>Id</th>
        			<th>Title</th>
        			<th>Budget</th>
        			<th>Requireds</th>
        		</tr>
        	</thead>
        	<tbody>
        		@foreach($jobs as $job)
        			<tr>
    	    			<td>{{$job->id}}</td>
    	    			<td>{!! link_to_route('job-view', $job->title, [$job->id]) !!}</a></td>
    	    			<td>{{$job->budget}}</td>
    	    			<td>{{$job->requireds}}</td>
    	    		</tr>
    			@endforeach
        	</tbody>
        </table>
    @else
        <div class="well" style="background-color: white">
                <h1>There is no job's posteds</h1>
        </div>
    @endif
@endsection