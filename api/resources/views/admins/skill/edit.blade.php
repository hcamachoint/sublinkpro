@extends('layouts.admin')

@section('content')
	<div class="row">
        <div class="col-lg-12">
          <div class="page-header">
            <h3>Skills Categories <small>A quick view of your website stats</small></h3>
          </div>
        </div>
    </div>
	<div class="container-fluid">
        <div class="well">
            <div class="page-header">
                <h1>{{$abi->name}}</h1>
            </div>
            <div class="container-fluid">
                {!!Form::open(['route'=>['abi-update', $abi->id], 'method' => 'put'])!!}
                    {{ csrf_field() }}
                    {!!Form::label('Name')!!}
                    {!!Form::text('name', $abi->name, ['class'=>'form-control', 'placeholder'=>'Name', 'required'=>'true'])!!}
                    {!!Form::label('Description')!!}
                    {!!Form::text('description', $abi->description, ['class'=>'form-control', 'placeholder'=>'Description', 'required'=>'true','maxlength' => 50])!!}<br>
                    {!!Form::button('<i class="glyphicon glyphicon-pencil"></i> Save', ['class'=>'btn btn-success', 'type'=>'submit'])!!}
                {!!Form::close()!!}
                {!!Form::open(['route'=>['abi-delete', $abi->id],'method' => 'delete'])!!}
                    {{ csrf_field() }}
                    {!!Form::button('<i class="glyphicon glyphicon-trash"></i> Delete', ['class'=>'btn btn-danger', 'type'=>'submit'])!!}
                {!!Form::close()!!}
            </div>
            <br>
            <a href="{{ URL::previous() }}" class="btn btn-primary">Go Back</a>
        </div>
    </div>
@endsection
