@extends('layouts.admin')

@section('head')
<link href="{{ url('/css/chartist.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<dashboard></dashboard>
	<!--<div class="row">
        <div class="col-lg-12">
          <div class="page-header">
            <h3>Dashboard <small>A quick view of your website stats</small></h3>
          </div>
        </div>
    </div>
    <table class="table">
    	<thead>
    		<tr>
    			<th>Active Users</th>
    		</tr>
    	</thead>
    	<tbody>
			<tr>
    			<td>{{$users}}</td>
    		</tr>
    	</tbody>
    </table>

    <table class="table">
    	<thead>
    		<tr>
    			<th>Active Jobs</th>
    		</tr>
    	</thead>
    	<tbody>
			<tr>
    			<td>{{$jobs}}</td>
    		</tr>
    	</tbody>
    </table>-->
@endsection

@section('footer')
<script src="{{ url('/js/chartist.min.js') }}"></script>
@endsection