@extends('layouts.admin')

@section('content')
	<div class="row">
        <div class="col-lg-12">
          <div class="page-header">
            <h3>Users <small>A quick view of your website stats</small></h3>
          </div>
        </div>
    </div>
    <div class="row">
        <div class="well">
            <div class="page-header">
                <h1>{{ $user->name }}</h1>
            </div>
            <div class="container-fluid">
                <img src="/avatars/{{ $user->image_path }}" style="width:150px; height:150px; float:left; border-radius:50%; margin-right:25px;">
                @include('users.forms.view')
                <b>Role: </b> {{$role->title}}
            </div>
        </div>
        <div class="well">
            <div class="container-fluid">
                {!!Form::open(['route'=> ['user-delete', $user->id], 'method' => 'post'])!!}
                    {{ csrf_field() }}
                    {!!Form::button('<i class="glyphicon glyphicon-trash"></i> Delete User', ['class'=>'btn btn-danger', 'type'=>'submit'])!!}
                {!!Form::close()!!}
                {!!Form::open(['route'=> ['user-ban', $user->id], 'method' => 'post'])!!}
                    {{ csrf_field() }}
                    {!!Form::button('<i class="glyphicon glyphicon-ban-circle"></i> Ban User', ['class'=>'btn btn-warning', 'type'=>'submit'])!!}
                {!!Form::close()!!}
                @if($role->id != 1)
                    {!!Form::open(['route'=> ['user-adm', $user->id], 'method' => 'post'])!!}
                        {{ csrf_field() }}
                        {!!Form::button('<i class="glyphicon glyphicon-plus"></i> Make Administrador', ['class'=>'btn btn-success', 'type'=>'submit'])!!}
                    {!!Form::close()!!}
                @elseif($role->id == 1)
                    {!!Form::open(['route'=> ['user-noadm', $user->id], 'method' => 'post'])!!}
                        {{ csrf_field() }}
                        {!!Form::button('<i class="glyphicon glyphicon-minus-sign"></i> Remove Administrador', ['class'=>'btn btn-success', 'type'=>'submit'])!!}
                    {!!Form::close()!!}
                @endif
            </div>
        </div>
        <a href="{{ URL::previous() }}" class="btn btn-primary">Go Back</a>
    </div>
@endsection