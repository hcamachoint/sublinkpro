@extends('layouts.admin')

@section('content')
	<div class="row">
        <div class="col-lg-12">
          <div class="page-header">
            <h3>Users <small>A quick view of your website stats</small></h3>
          </div>
        </div>
    </div>
    <table class="table">
    	<thead>
    		<tr>
    			<th>Id</th>
    			<th>Name</th>
    			<th>Last Name</th>
    			<th>Email</th>
    		</tr>
    	</thead>
    	<tbody>
    		@foreach($users as $user)
    			<tr>
	    			<td>{{$user->id}}</td>
	    			<td>{!! link_to_route('user-view', $user->name, [$user->id]) !!}</td>
	    			<td>{{$user->name2}}</td>
	    			<td>{{$user->email}}</td>
	    		</tr>
			@endforeach
    	</tbody>
    </table>
@endsection