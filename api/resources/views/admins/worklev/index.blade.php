@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-lg-12">
          <div class="page-header">
            <h3>Work Lelvels
                <small>A quick view of your website stats</small>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                      <i class="glyphicon glyphicon-plus-sign"> Add</i>
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            {!!Form::open(['route'=> ['wol-store'], 'method' => 'post'])!!}
                                {{ csrf_field() }}
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h3 class="modal-title" id="myModalLabel">New Work Level</h3>
                                </div>
                                <div class="modal-body">

                                    {!!Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Name', 'required'=>'true', 'maxlength' => 100])!!}<br>

                                    {!!Form::text('description', null, ['class'=>'form-control', 'placeholder'=>'Description', 'required'=>'true', 'maxlength' => 100])!!}<br>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            {!!Form::close()!!}
                        </div>
                      </div>
                    </div>
                </h3>
          </div>
        </div>
    </div>
    <table class="table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
            @foreach($wols as $wol)
                <tr>
                    <td>{{$wol->id}}</td>
                    <td>{!! link_to_route('wol-edit', $wol->name, [$wol->id]) !!}</a></td>
                    <td>{{$wol->description}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
