@extends('layouts.admin')

@section('content')
	<div class="row">
        <div class="col-lg-12">
          <div class="page-header">
            <h3>Work Categories <small>A quick view of your website stats</small></h3>
          </div>
        </div>
    </div>
	<div class="container-fluid">
        <div class="well">
            <div class="page-header">
                <h1>{{$woc->name}}</h1>
            </div>
            <div class="container-fluid">
                {!!Form::open(['route'=>['woc-update', $woc->id], 'method' => 'put'])!!}
                    {{ csrf_field() }}
                    {!!Form::label('Name')!!}
                    {!!Form::text('name', $woc->name, ['class'=>'form-control', 'placeholder'=>'Name', 'required'=>'true'])!!}
                    {!!Form::label('Description')!!}
                    {!!Form::text('description', $woc->description, ['class'=>'form-control', 'placeholder'=>'Description', 'required'=>'true','maxlength' => 50])!!}
                    {!!Form::label('Slug')!!}
                    {!!Form::text('slug', $woc->slug, ['class'=>'form-control', 'placeholder'=>'Slug', 'required'=>'true'])!!}<br>
                    {!!Form::button('<i class="glyphicon glyphicon-pencil"></i> Save', ['class'=>'btn btn-success', 'type'=>'submit'])!!}
                {!!Form::close()!!}
                {!!Form::open(['route'=>['woc-delete', $woc->id],'method' => 'delete'])!!}
                    {{ csrf_field() }}
                    {!!Form::button('<i class="glyphicon glyphicon-trash"></i> Delete', ['class'=>'btn btn-danger', 'type'=>'submit'])!!}
                {!!Form::close()!!}
            </div>
            <br>
            <a href="{{ URL::previous() }}" class="btn btn-primary">Go Back</a>
        </div>
    </div>
@endsection
