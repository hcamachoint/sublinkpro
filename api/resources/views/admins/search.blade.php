@extends('layouts.admin')

@section('content')
        <div class="row">
            <div class="col-md-12">
                <!--JOBS SECTION-->
                @if(count($jobs) >= 1)
                    @foreach($jobs as $job)
                        <div class="well" style="background-color: white">
                            <div class="page-header">
                                <h1><a href="#">{!! link_to_route('job-view', $job->title, [$job->id]) !!}</a></h1>
                                    <b>Amount:</b> {{$job->budget}} @if($job->pstructure == 1) $ @else $/hr @endif |
                                    <b>Category:</b> {{$job->category->description}} | 
                                    <b>To start at</b> {{$job->startd}} |
                                    <b>Published:</b> {{$job->created_at}} |
                                    @if($job->user->id === Auth::user()->id) <b>You</b> @else {{$job->user->name}} @endif |
                                    <b>
                                        @if($job->status === 0)
                                            <p>Ended</p>
                                        @elseif($job->status === 1)
                                            <p>Active</p>
                                        @else
                                            <p>Active Full</p>
                                        @endif
                                    </b>
                            </div>
                            <div>
                                <p>{{$job->description}}</p>
                            </div>
                        </div>
                    @endforeach
                    <div align="center">{!!$jobs->links()!!}</div>
                @elseif(count($users) >= 1)

                @else
                	<div class="well" style="background-color: white">
                        <div class="page-header">
                            <h1>No jobs founds</h1>
                        </div>
                        <div>
                            <p>Sorry, no jobs found.</p>
                        </div>
                    </div>
                @endif
                <!--END JOBS SECTION-->

                <!--USERS SECTION-->
                @if(count($users) >= 1)
                    @foreach($users as $user)
                        <div class="well" style="background-color: white">
                            <div class="page-header">
                                <h1>{!! link_to_route('user-view', $user->email, [$user->id]) !!}</a></h1>
                            </div>
                            <div>
                                <b>IDN: </b>{{$user->idn}}
                                <b>Name: </b>{{$user->name}}
                                <b>Last Name: </b>{{$user->name2}}
                                <b>Sex: </b>{{$user->sex}}
                            </div>
                        </div>
                    @endforeach
                @elseif(count($jobs) >= 1)

                @else
                    <div class="well" style="background-color: white">
                        <div class="page-header">
                            <h1>No users founds</h1>
                        </div>
                        <div>
                            <p>Sorry, no users found.</p>
                        </div>
                    </div>
                @endif
                <!--USERS JOBS SECTION-->
                
            </div>
        </div>
@endsection