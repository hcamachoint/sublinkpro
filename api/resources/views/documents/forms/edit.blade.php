{!!Form::hidden('able', $j, ['class' => 'form-control'])!!}
{!!Form::text('category', $doc->category->description, ['class'=>'form-control', 'placeholder'=>'Category', 'readonly' => 'true', 'required'=>'true'])!!}
{!!Form::text('description', $doc->description, ['class'=>'form-control', 'placeholder'=>'Description', 'required'=>'true','maxlength' => 50])!!}
<p class="lead">Current file:<a href="{{$doc->file_path}}" target="_blank">{{$doc->file_name}}</a></p>
<!--{!!Form::file('document', null, ['class'=>'form-control','required'=>'true'])!!}-->
{!!Form::button('<i class="glyphicon glyphicon-floppy-disk"></i> Save', ['class'=>'btn btn-success', 'type'=>'submit'])!!}
