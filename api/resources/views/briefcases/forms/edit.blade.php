{!!Form::text('title', $bri->title, ['class'=>'form-control', 'placeholder'=>'Title', 'required'=>'true','maxlength' => 30])!!}
{!!Form::text('description', $bri->description, ['class'=>'form-control', 'placeholder'=>'Description', 'required'=>'true'])!!}
{!!Form::date('date', $bri->date, ['class'=>'form-control'])!!}
{!!Form::text('url', $bri->url, ['class'=>'form-control', 'placeholder'=>'Url like: http://myweb.com'])!!}
{!!Form::button('<i class="glyphicon glyphicon-plus"></i> Save', ['class'=>'btn btn-success','type'=>'submit'])!!}
