{!!Form::text('title', null, ['class'=>'form-control', 'placeholder'=>'Title', 'required'=>'true','maxlength' => 30])!!}
{!!Form::text('description', null, ['class'=>'form-control', 'placeholder'=>'Description', 'required'=>'true'])!!}
{!!Form::date('date', \Carbon\Carbon::now(), ['class'=>'form-control'])!!}
{!!Form::text('url', null, ['class'=>'form-control', 'placeholder'=>'Url like: http://myweb.com'])!!}
{!!Form::button('<i class="glyphicon glyphicon-plus"></i> Save', ['class'=>'btn btn-success','type'=>'submit'])!!}
