{!!Form::text('category', $soc->category->description, ['class'=>'form-control', 'placeholder'=>'Category', 'readonly' => 'true', 'required'=>'true'])!!}
{!!Form::text('username', $soc->username, ['class'=>'form-control', 'placeholder'=>'Username', 'required'=>'true','maxlength' => 50])!!}
{!!Form::button('<i class="glyphicon glyphicon-floppy-disk"></i> Save', ['class'=>'btn btn-success', 'type'=>'submit'])!!}
