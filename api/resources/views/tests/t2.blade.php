@extends('layouts.app')

@section('content')


<!-- Sidebar -->
<div class="container">
		<div class="col-sm-3 col-md-3 col-xs-12 sidebar">
			<div class="list-group">
					<a href="#collapse" class="list-group-item list-group-item-success" data-toggle="collapse" data-parent="#Sidebar">Categorias <i class="fa fa-caret-down"></i></a>
				<div class="collapse" id="collapse">
					<a href="#" class="list-group-item">Piscina</a>
					<a href="#" class="list-group-item">Tapizado</a>
				</div>
			</div>
		</div>

<!-- #Sidebar -->


	<div class="col-sm-7 col-md-7 col-xs-12">
		<div class="well">
			<h1><u><b>Empleo 1</b></u></h1>
				<br>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			 	<br>

			 	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			 	<br>

			 	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				<br>

			 	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				<br>

			<!--Buttom-->
			<ul class="pager" align="center">
				<li><a href="http://127.0.0.2:8000/test1">Back</a></li>
				<li><a href="#">Apply</a></li>
			</ul>
			<!--#Buttom-->

		</div>	
	</div>
		
	<div class="col-sm-2 col-md-2 col-xs-12">
		<h3><b>Paga</b></h3>
		<p><u><b>40$/hr</b></u></p>

		<h3><b>Tiempo Estimado</b></h3>
		<p><u><b>2-4 Semanas</b></u></p>

		<h3><b>Contacto</b></h3>
		<p><u><b>0414-000 0000</b></u></p>
	</div>
</div>


		<!-- Footer -->
	<div class="panel-footer" style="background-color: white">
		<div class="container">
			<div class="col-md-4 col-sm-4 col-xs-12">
				<h2>Redes</h2>
					<ul>				
						<li><icon>Facebook</icon></li>
						<li>Twitter</li>
						<li>Instagram</li>
						<li>Reddit</li>
					</ul>
			</div>

				<div class="col-md-4 col-sm-4 col-xs-12">
					<h2>Contacto</h2>
						<ul>
							<li>Contact Us</li>
							<li>About Us</li>
						</ul>
				</div>

			<div class="col-md-4 col-sm-4 col-xs-12">
				<h2>Licenses</h2>
					<ul>
						<li>Lorem Ipsum</li>
						<li>Lorem Ipsum</li>
						<li>Lorem Ipsum</li>
					</ul>
			</div>
		</div>
	</div>
	<!-- #Footer -->	
@endsection
