@extends('layouts.app')

@section('content')

	<!-- Sidebar -->
<div class="container">
		<div class="col-sm-3 col-md-3 col-xs-12 sidebar">
			<div class="list-group">
				<buttom type="buttom" class="list-group-item" data-toggle="collapse" data-target="#busquedas"><b><u>Busquedas</u></b> <i class="fa fa-caret-down"></i></buttom>
				
					<div id="busquedas" class="collapse">
						<a href="#" class="list-group-item">Piscina</a>
						<a href="#" class="list-group-item">Tapiceria</a>
					</div>
			
			
				<buttom type="buttom" class="list-group-item" data-toggle="collapse" data-target="#perfil"><b><u>Perfil</u></b> <i class="fa fa-caret-down"></i></buttom>

					<div id="perfil" class="collapse in">
						<a href="#" class="list-group-item">Information</a>
						<a href="#" class="list-group-item">Contacs</a>
						<a href="#" class="list-group-item">Abilities</a>
						<a href="#" class="list-group-item">Location</a>
						<a href="#" class="list-group-item">Documents</a>
						<a href="#" class="list-group-item">Education</a>
						<a href="#" class="list-group-item">Briefcases</a>
						<a href="#" class="list-group-item">Experiences</a>
					</div>
			</div>

		</div>
	<!-- #Sidebar -->

	<div class="container col-md-9 col-sm-9 col-xs-12">
		<div class="well" style="background-color: white">
			<div class="page-header">
				<h1>Information <small><span class="glyphicon glyphicon-pencil"></span></a></small></h1>
			</div>

				<!-- Information -->
			<div class="form-group">			
				<label for="usr">Name:</label>
				<input type="text" class="form-control" id="usr" placeholder="Name">
					
			</div>


			<div class="form-group">
				
				<label for="lastname">Last Name:</label>
				<input type="text" class="form-control" id="lastname" placeholder="Last name">
				
			</div>

			<div class="form-group">
				<label for="idn">IDN:</label>
				<input type="number" class="form-control" id="idn" placeholder="IDN">
				
			</div>
				

			<div class="form-group">
				<label for="sex">Sex:</label>
					<select class="form-control" id="sex">
						<option>Male</option>
						<option>Female</option>
						<option>Other</option>
					</select>
							
			</div>
				
			<div class="form-group">
				<label for="email">Email:</label>
				<input type="text" class="form-control" id="email" placeholder="Email">
			</div>

				<button style="float: center">Update</button>
					<!-- #Information -->
		</div>


			<!-- Contacts -->
		<div class="well" style="background-color: white">
			<div class="page-header">
				<h1>Contacts <small><span class="glyphicon glyphicon-pencil"></span></a></small></h1>
			</div>

			<div class="form-group">			
			
				<label for="number">Phone:</label>
				<input type="number" class="form-control" id="number" placeholder="+5841200000">
			
			</div>

			<button>Update</button>	
		</div>
			<!-- #Contacts -->


			<!-- Abilities -->
		<div class="well" style="background-color: white">
			<div class="page-header">
				<h1>Abilities <small><span class="glyphicon glyphicon-pencil"></span></a></small></h1>
			</div>

			<div class="form-group">
				<label for="abilities">Abilities: </label>
					<select class="form-control" id="abilities">
					<option>Programming</option>
					<option>Music</option>
					<option>Video Editing</option>
					<option>Arrangement</option>
					<option>Write</option>
					<option>Php</option>
					<option>Javascript</option>
					<option>Python</option>
					</select>
			</div>
			<button>Add</button>
			<button>Remove</button>
		</div>

			<!-- #Abilities -->


			<!-- Location -->
		<div class="well" style="background-color: white">
			<div class="page-header">
				<h1>Location <small><span class="glyphicon glyphicon-pencil"></span></a></small></h1>
			</div>

			<div class="form-group">
				<label for="location">Country: </label>
				<select class="form-control" id="location">
					<option>Venezuela</option>
					<option>United States</option>
					<option>Ukraine</option>
					<option>Ecuador</option>
				</select>
			</div>

			<div class="form-group">
				<label for="city">City: </label>
				<select class="form-control" id="city">
					<option>1</option>
					<option>2</option>
					<option>3</option>
					<option>4</option>
				</select>
			</div>

			<div class="form-group">
				<label for="timezone">Timezone: </label>
					<select class="form-control" id="timezone">
						<option>(UTC-04:00) Asunción</option>
						<option>(UTC-04:30) Venezuela</option>
					</select>
			</div>

			<div class="form-group">
				<label for="address">Address: </label>
				<input type="text" class="form-control" id="address" placeholder="Ej. Roosevelt Ave & 74th Street, Queens, NY 11372, EE. UU.">
			</div>

			<div class="form-group">
				<label for="zip">Zip Code: </label>
				<input type="number" class="form-control" id="zip" placeholder="Ej. 10023">
			</div>

			<button>Update</button>
		</div>
			<!-- #Location -->

			<!-- Documents -->
		<div class="well" style="background-color: white">
			<div class="page-header">
				<h1>Documents <small><span class="glyphicon glyphicon-pencil"></span></a></small></h1>
			</div>


			<div class="form-group">
				<input type="file" multiple>
			</div>

			<button>Upload</button>

		</div>

			<!-- #Documents -->








	</div>
</div>




	<!-- Footer -->
<div class="panel-footer" style="background-color: white">
		<div class="container">
			<div class="col-md-4 col-sm-4 col-xs-12">
				<h2>Redes</h2>
					<ul>				
						<li><icon>Facebook</icon></li>
						<li>Twitter</li>
						<li>Instagram</li>
						<li>Reddit</li>
					</ul>
			</div>

				<div class="col-md-4 col-sm-4 col-xs-12">
					<h2>Contacto</h2>
						<ul>
							<li>Contact Us</li>
							<li>About Us</li>
						</ul>
				</div>

			<div class="col-md-4 col-sm-4 col-xs-12">
				<h2>Licenses</h2>
					<ul>
						<li>Lorem Ipsum</li>
						<li>Lorem Ipsum</li>
						<li>Lorem Ipsum</li>
					</ul>
			</div>
		</div>
</div>
	<!-- #footer -->







@endsection