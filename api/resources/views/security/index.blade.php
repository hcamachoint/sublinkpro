@extends('layouts.app')
@section('content')

<div class="container">
	<div class="well" style="background-color: white">
		<div class="page-header">
			<h1>Security <small><a href="#"></a></small></h1>
		</div>
			{!! link_to_route('security-password', 'Password Change') !!}
	</div>
	<a href="{{ route('profile') }}" class="btn btn-default">Go Back</a><br><br>
</div>

@endsection