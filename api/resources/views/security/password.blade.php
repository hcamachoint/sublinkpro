@extends('layouts.app')
@section('content')

<div class="container">
	<div class="well" style="background-color: white">
		<div class="page-header">
			<h1>Password Change<small><a href="#"></a></small></h1>
		</div>
		<div align="center">
			{!! Form::open(['route' => ['security-passwordu'], 'method' => 'put']) !!}
				{{ csrf_field() }}
				<table class="table table-borderless">
					<tr>
						<td class="col-md-3">{!!Form::label('Old Password')!!}</td>
						<td class="col-md-9">{!!Form::password('old_password', null, ['class'=>'form-control', 'placeholder'=>'Old Password', 'required'=>'true', 'maxlength' => 100])!!}</td>
					</tr>
					<tr>
						<td class="col-md-3">{!!Form::label('New Password')!!}</td>
						<td class="col-md-9">{!!Form::password('password', null, ['class'=>'form-control', 'placeholder'=>'New Password', 'required'=>'true', 'maxlength' => 100])!!}</td>
					</tr>
					<tr>
						<td class="col-md-3">{!!Form::label('Password Confirmation')!!}</td>
						<td class="col-md-9">{!!Form::password('password_confirmation', null, ['class'=>'form-control', 'placeholder'=>'Password Confirmation', 'required'=>'true', 'maxlength' => 100])!!}</td>
					</tr>
					<tr>
						<td>{!!Form::button('<i class="glyphicon glyphicon-refresh"></i> Change', ['class'=>'btn btn-success','type'=>'submit'])!!}</td>
					</tr>
				</table>
			{!! Form::close() !!}			
		</div>
	</div>
	<a href="{{ route('security-index') }}" class="btn btn-default">Go Back</a><br><br>
</div>

@endsection