@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Notifications</div>

                <div class="panel-body">
                    @if(count($user->notifications) >= 1)
                        @foreach($user->notifications as $notification)
                            <b>Message: </b>{{$notification->data['proposal']['message']}} 
                            <b>User: </b> {{$notification->data['user']['name']}} <b>Email: 
                            </b> {{$notification->data['user']['email']}}
                            {!! link_to_route('work-show', '(View)', [$notification->data['proposal']['work_id']]) !!}<br>
                            {{$notification->markAsRead()}}
                        @endforeach
                    @else
                        <div>
                            <h1>No notifications founds</h1>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
