<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Sublinkpro') }}</title>

    <!-- Styles -->
    <link href="{{ url('/css/app.css') }}" rel="stylesheet">
    <link href="{{ url('/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ url('/css/adminpan.css') }}" rel="stylesheet">
    <!--<link href="{{ url('/css/material.css') }}" rel="stylesheet">-->
    <!--<link href="{{ url('/css/theme.css') }}" rel="stylesheet">-->
    @yield('head')
</head>
<body>
    <div id="app">
        <!--NAVBAR-->
        <nav class="navbar navbar-icon-top navbar-inverse">
            <div class="container">
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bootboard-nav">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="{{ route('home') }}">Sublinkpro<span>Panel</span></a>
                </div>

                <ul class="nav navbar-nav navbar-right">
                    <li>
                        {!!Form::open(['route'=>'admin-search', 'method' => 'post', 'class' => 'navbar-form navbar-left'])!!}
                            {{ csrf_field() }}
                            {!!Form::text('search', null, ['class' => 'form-control', 'placeholder' => 'Search Posts & Users'])!!}
                        {!!Form::close()!!}
                    </li>
                    <notification :userid="{{auth()->id()}}" :unreads="{{auth()->user()->unreadNotifications}}"></notification>
                  <li class="dropdown">

                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <img class="img-responsive img-circle" src="/avatars/{{ Auth::user()->image_path }}" style="width:45px; height:45px;">
                                {{ substr(Auth::user()->name, 0, 6) }} <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li class="{{ Request::is('home') ? 'active' : '' }}"><a href="{{ route('home') }}">User Page</a></li>
                                <li class="{{ Request::is('profile') ? 'active' : '' }}"><a href="{{ route('profile') }}">My Profile</a></li>
                                <li class="{{ Request::is('security*') ? 'active' : '' }}"><a href="{{ route('security-index') }}">Settings</a></li>
                                <li class="{{ Request::is('supports') ? 'active' : '' }}"><a href="{{ route('supports') }}">Support</a></li>
                                <li class="{{ Request::is('help') ? 'active' : '' }}"><a href="{{ route('help') }}">Help</a></li>
                                <li>
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                </ul>
            </div>
        </nav>
        <!--ALERTS-->
        @include('alerts.success')
        @include('alerts.request')
        @include('alerts.errors')
        <!--CONTENT-->
        <div class="container-fluid">
            <div class="row">
                <!-- sidebar //-->
                <div class="col-md-3">
                    <div class="collapse navbar-collapse" id="bootboard-nav">
                        <ul class="nav nav-pills nav-stacked">
                            <li class="navigation-header"><h4>Control Panel</h4></li>
                            <li class="{{ Request::is('panel') ? 'active' : '' }}"><a href="{{ route('admin-home') }}">Dashboard</a></li>
                            <li class="{{ Request::is('panel/jobs*') ? 'active' : '' }}"><a href="{{ route('job-list') }}">Jobs</a></li>
                            <!--<li class="{{ Request::is('panel/analytics') ? 'active' : '' }}"><a href="{{ route('analytic-index') }}">Analytics</a></li>-->
                            <!--<li class="navigation-header">Accounts</li>-->
                            <li class="{{ Request::is('panel/users*') ? 'active' : '' }}"><a href="{{ route('user-list') }}">Users</a></li>
                            <hr>
                            <li class="{{ Request::is('panel/abilities*') ? 'active' : '' }}"><a href="{{ route('abi-list') }}">Skills Categories</a></li>
                            <li class="{{ Request::is('panel/procat*') ? 'active' : '' }}"><a href="{{ route('poc-list') }}">Project Categories</a></li>
                            <li class="{{ Request::is('panel/doccat*') ? 'active' : '' }}"><a href="{{ route('doc-list') }}">Documents Categories</a></li>
                            <li class="{{ Request::is('panel/soccat*') ? 'active' : '' }}"><a href="{{ route('soc-list') }}">Social Categories</a></li>
                            <li class="{{ Request::is('panel/workcat*') ? 'active' : '' }}"><a href="{{ route('woc-list') }}">Work Categories</a></li>
                            <li class="{{ Request::is('panel/worklev*') ? 'active' : '' }}"><a href="{{ route('wol-list') }}">Work Levels</a></li>
                            <!--<li class="{{ Request::is('panel/settings') ? 'active' : '' }}"><a href="{{ route('setting-index') }}">Settings</a></li>-->
                            <hr>
                            <li>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9">
                    @yield('content')
                </div>
            </div>
        </div>
        <!--FOOTER-->
        <!--<footer>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="list-inline">
                            <li><a href="#"><i class="fa fa-facebook-official"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest-square"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
                            <li>&copy; 2017 Sublinkpro</li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>-->

    </div>

    <!-- Scripts -->
    <script src="{{ url('/js/app.js') }}"></script>
    <!--PANEL ADMIN-->
    @yield('footer')
</body>
</html>
