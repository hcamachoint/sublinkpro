<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEducationsTable extends Migration {

	public function up()
	{
		Schema::create('educations', function(Blueprint $table) {
			$table->increments('id');
			$table->string('school', 100);
			$table->string('degree', 100);
			$table->date('perioda');
			$table->date('periodb')->nullable();
			$table->boolean('active')->nullable();
			$table->string('area', 100)->nullable();
			$table->text('description')->nullable();
			$table->integer('user_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('educations');
	}
}