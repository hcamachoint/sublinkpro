<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLocationsTable extends Migration {

	public function up()
	{
		Schema::create('locations', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('city_id')->unsigned();
			$table->string('zip', 10)->nullable();
			$table->text('address')->nullable();
			$table->integer('locationable_id');
			$table->string('locationable_type');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('locations');
	}
}
