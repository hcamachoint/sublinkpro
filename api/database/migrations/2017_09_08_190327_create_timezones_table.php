<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTimezonesTable extends Migration {

	public function up()
	{
		Schema::create('timezones', function(Blueprint $table) {
			$table->increments('id');
			$table->string('title', 50)->unique();
			$table->boolean('tz_pn');
			$table->time('tz_time');
			$table->boolean('dst_pn')->nullable();
			$table->time('dst_time')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('timezones');
	}
}