<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAbilitiesTable extends Migration {

	public function up()
	{
		Schema::create('abilities', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('category_id')->unsigned();
			$table->integer('abilityable_id');
			$table->string('abilityable_type');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('abilities');
	}
}