<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserChatTable extends Migration {

	public function up()
	{
		Schema::create('user_chat', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->integer('chat_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('user_chat');
	}
}