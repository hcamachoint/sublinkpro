<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDocumentsTable extends Migration {

	public function up()
	{
		Schema::create('documents', function(Blueprint $table) {
			$table->increments('id');
			$table->text('description')->nullable();
			$table->integer('type')->default('1')->comment('1:image,2:pdf,3:document,4:zip');
			$table->string('file_path',150)->nullable();
			$table->string('file_name',150)->nullable();
			$table->integer('category_id')->unsigned();
			$table->integer('documentable_id');
			$table->string('documentable_type');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('documents');
	}
}
