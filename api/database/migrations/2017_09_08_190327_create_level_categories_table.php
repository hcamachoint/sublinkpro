<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLevelCategoriesTable extends Migration {

	public function up()
	{
		Schema::create('level_categories', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 50);
			$table->text('description');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('level_categories');
	}
}