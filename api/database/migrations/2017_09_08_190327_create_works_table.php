<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWorksTable extends Migration {

	public function up()
	{
		Schema::create('works', function(Blueprint $table) {
			$table->increments('id');
			$table->string('title', 100);
			$table->text('description');

			$table->integer('pstructure');
			$table->decimal('budget', 10,2);

			$table->date('startd')->nullable();
			$table->integer('texpect');

			$table->integer('requireds');
			$table->integer('status')->comment('0:created,1:active,2:With proposals,3:full,8:closed,9:finished');


			$table->integer('category_id')->unsigned();
			$table->integer('project_id')->unsigned();
			$table->integer('level_id')->unsigned();
			$table->integer('user_id')->unsigned();

			$table->softDeletes();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('works');
	}
}
