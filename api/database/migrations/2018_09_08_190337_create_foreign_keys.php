<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('works', function(Blueprint $table) {
			$table->foreign('category_id')->references('id')->on('work_categories')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('works', function(Blueprint $table) {
			$table->foreign('project_id')->references('id')->on('project_categories')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('works', function(Blueprint $table) {
			$table->foreign('level_id')->references('id')->on('level_categories')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('works', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('briefcases', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('role_user', function(Blueprint $table) {
			$table->foreign('role_id')->references('id')->on('roles')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('role_user', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('educations', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('contacts', function(Blueprint $table) {
			$table->foreign('category_id')->references('id')->on('contact_categories')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('login_attempts', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('abilities', function(Blueprint $table) {
			$table->foreign('category_id')->references('id')->on('ability_categories')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('experiences', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('experiences', function(Blueprint $table) {
			$table->foreign('category_id')->references('id')->on('work_categories')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('states', function(Blueprint $table) {
			$table->foreign('country_id')->references('id')->on('countries')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('cities', function(Blueprint $table) {
			$table->foreign('state_id')->references('id')->on('states')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('locations', function(Blueprint $table) {
			$table->foreign('city_id')->references('id')->on('cities')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('documents', function(Blueprint $table) {
			$table->foreign('category_id')->references('id')->on('document_categories')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('socials', function(Blueprint $table) {
			$table->foreign('category_id')->references('id')->on('social_categories')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('feedbacks', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('feedbacks', function(Blueprint $table) {
			$table->foreign('work_id')->references('id')->on('works')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
	}

	public function down()
	{
		Schema::table('works', function(Blueprint $table) {
			$table->dropForeign('works_category_id_foreign');
		});
		Schema::table('works', function(Blueprint $table) {
			$table->dropForeign('works_project_id_foreign');
		});
		Schema::table('works', function(Blueprint $table) {
			$table->dropForeign('works_level_id_foreign');
		});
		Schema::table('works', function(Blueprint $table) {
			$table->dropForeign('works_user_id_foreign');
		});
		Schema::table('briefcases', function(Blueprint $table) {
			$table->dropForeign('briefcases_user_id_foreign');
		});
		Schema::table('role_user', function(Blueprint $table) {
			$table->dropForeign('role_user_role_id_foreign');
		});
		Schema::table('role_user', function(Blueprint $table) {
			$table->dropForeign('role_user_user_id_foreign');
		});
		Schema::table('educations', function(Blueprint $table) {
			$table->dropForeign('educations_user_id_foreign');
		});
		Schema::table('contacts', function(Blueprint $table) {
			$table->dropForeign('contacts_category_id_foreign');
		});
		Schema::table('login_attempts', function(Blueprint $table) {
			$table->dropForeign('login_attempts_user_id_foreign');
		});
		Schema::table('abilities', function(Blueprint $table) {
			$table->dropForeign('abilities_category_id_foreign');
		});
		Schema::table('experiences', function(Blueprint $table) {
			$table->dropForeign('experiences_user_id_foreign');
		});
		Schema::table('experiences', function(Blueprint $table) {
			$table->dropForeign('experiences_category_id_foreign');
		});
		Schema::table('states', function(Blueprint $table) {
			$table->dropForeign('states_country_id_foreign');
		});
		Schema::table('cities', function(Blueprint $table) {
			$table->dropForeign('cities_state_id_foreign');
		});
		Schema::table('locations', function(Blueprint $table) {
			$table->dropForeign('locations_city_id_foreign');
		});
		Schema::table('documents', function(Blueprint $table) {
			$table->dropForeign('documents_category_id_foreign');
		});
		Schema::table('socials', function(Blueprint $table) {
			$table->dropForeign('socials_category_id_foreign');
		});
		Schema::table('feedbacks', function(Blueprint $table) {
			$table->dropForeign('feedbacks_user_id_foreign');
		});
		Schema::table('feedbacks', function(Blueprint $table) {
			$table->dropForeign('feedbacks_work_id_foreign');
		});
	}
}
