<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBriefcasesTable extends Migration {

	public function up()
	{
		Schema::create('briefcases', function(Blueprint $table) {
			$table->increments('id');
			$table->string('title', 30);
			$table->text('description');
			$table->date('date');
			$table->string('url')->nullable();
			$table->integer('user_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('briefcases');
	}
}