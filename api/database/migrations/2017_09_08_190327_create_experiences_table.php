<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExperiencesTable extends Migration {

	public function up()
	{
		Schema::create('experiences', function(Blueprint $table) {
			$table->increments('id');
			$table->string('company', 100);
			$table->string('title', 50);
			$table->string('role', 200);
			$table->text('description')->nullable();
			$table->date('perioda');
			$table->date('periodb')->nullable();
			$table->boolean('active')->nullable();
			$table->integer('user_id')->unsigned();
			$table->integer('category_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('experiences');
	}
}