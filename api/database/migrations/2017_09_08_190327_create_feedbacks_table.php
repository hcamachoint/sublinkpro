<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFeedbacksTable extends Migration {

	public function up()
	{
		Schema::create('feedbacks', function(Blueprint $table) {
			$table->increments('id');
			$table->text('message');
			$table->integer('points');
			$table->integer('pointa');
			$table->integer('pointb');
			$table->integer('pointc');
			$table->integer('type');
			$table->integer('user_id')->unsigned();
			$table->integer('work_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('feedbacks');
	}
}