<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProposalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proposals', function(Blueprint $table) {
            $table->increments('id');
            $table->decimal('amount', 10,2)->nullable();
            $table->integer('esttime')->nullable();
            $table->text('message');
            $table->integer('status')->default(0);
            $table->integer('user_id')->unsigned();
            $table->integer('work_id')->unsigned();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proposals');
    }
}
