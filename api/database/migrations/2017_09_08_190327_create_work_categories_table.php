<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWorkCategoriesTable extends Migration {

	public function up()
	{
		Schema::create('work_categories', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 50)->unique();
			$table->text('description');
			$table->string('slug', 20)->unique();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('work_categories');
	}
}