<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContactsTable extends Migration {

	public function up()
	{
		Schema::create('contacts', function(Blueprint $table) {
			$table->increments('id');
			$table->string('address', 200);
			$table->integer('category_id')->unsigned();
			$table->integer('contactable_id');
			$table->string('contactable_type');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('contacts');
	}
}