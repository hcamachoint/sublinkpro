<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLoginAttemptsTable extends Migration {

	public function up()
	{
		Schema::create('login_attempts', function(Blueprint $table) {
			$table->increments('id');
			$table->string('ip', 15);
			$table->string('browser_agent', 100)->nullable();
			$table->boolean('success');
			$table->integer('user_id')->unsigned();
			$table->timestamps();
			$table->text('event');
		});
	}

	public function down()
	{
		Schema::drop('login_attempts');
	}
}