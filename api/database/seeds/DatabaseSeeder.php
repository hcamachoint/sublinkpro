<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CountriesTableSeeder::class);
        $this->call(StatesTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(TimeZonesTableSeeder::class);
        $this->call(ContactCategoriesTableSeeder::class);
        $this->call(AbilityCategoriesTableSeeder::class);
        $this->call(DocumentCategoriesTableSeeder::class);
        $this->call(WorkCategoriesTableSeeder::class);
        $this->call(LevelCategoriesTableSeeder::class);
        $this->call(ProjectCategoriesTableSeeder::class);
        $this->call(SocialCategoriesTableSeeder::class);
        $this->call(UsersTableSeeder::class); //DEFAULT ADMIN
    }
}
