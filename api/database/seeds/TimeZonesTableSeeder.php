<?php

use Illuminate\Database\Seeder;

class TimeZonesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('timezones')->insert([
        'title' => 'Caracas',
        'tz_pn' => FALSE,
        'tz_time' => '4:30',
        'dst_pn' => TRUE,
        'dst_time' => '4:30',
      ]);
    }
}
