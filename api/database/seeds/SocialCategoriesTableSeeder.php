<?php

use Illuminate\Database\Seeder;

class SocialCategoriesTableSeeder extends Seeder
{
  public function run()
  {
    DB::table('social_categories')->insert([
        'name' => 'facebook',
        'description' => 'Facebook',
        'url' => 'https://www.facebook.com',
    ]);

    DB::table('social_categories')->insert([
        'name' => 'twitter',
        'description' => 'Twitter',
        'url' => 'https://twitter.com',
    ]);
  }
}
