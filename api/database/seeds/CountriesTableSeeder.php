<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->insert([
          'title' => 'Afghanistan',
          'slug' => 'afghanistan',
        ]);

        DB::table('countries')->insert([
          'title' => 'Albania',
          'slug' => 'albania',
        ]);

        DB::table('countries')->insert([
          'title' => 'Algeria',
          'slug' => 'algeria',
        ]);

        DB::table('countries')->insert([
          'title' => 'Andorra',
          'slug' => 'andorra',
        ]);

        DB::table('countries')->insert([
          'title' => 'Angola',
          'slug' => 'angola',
        ]);

        DB::table('countries')->insert([
          'title' => 'Antigua and Barbuda',
          'slug' => 'antigua_and_barbuda',
        ]);

        DB::table('countries')->insert([
          'title' => 'Argentina',
          'slug' => 'argentina',
        ]);

        DB::table('countries')->insert([
          'title' => 'Armenia',
          'slug' => 'armenia',
        ]);

        DB::table('countries')->insert([
          'title' => 'Australia',
          'slug' => 'australia',
        ]);

        DB::table('countries')->insert([
          'title' => 'Austria',
          'slug' => 'austria',
        ]);

        DB::table('countries')->insert([
          'title' => 'Azerbaijan',
          'slug' => 'azerbaijan',
        ]);

        DB::table('countries')->insert([
          'title' => 'Bahamas',
          'slug' => 'bahamas',
        ]);

        DB::table('countries')->insert([
          'title' => 'Bahrain',
          'slug' => 'bahrain',
        ]);

        DB::table('countries')->insert([
          'title' => 'Bangladesh',
          'slug' => 'bangladesh',
        ]);

        DB::table('countries')->insert([
          'title' => 'Barbados',
          'slug' => 'barbados',
        ]);

        DB::table('countries')->insert([
          'title' => 'Belarus',
          'slug' => 'belarus',
        ]);

        DB::table('countries')->insert([
          'title' => 'Belgium',
          'slug' => 'belgium',
        ]);

        DB::table('countries')->insert([
          'title' => 'Belize',
          'slug' => 'belize',
        ]);

        DB::table('countries')->insert([
          'title' => 'Benin',
          'slug' => 'benin',
        ]);

        DB::table('countries')->insert([
          'title' => 'Bhutan',
          'slug' => 'bhutan',
        ]);

        DB::table('countries')->insert([
          'title' => 'Bolivia',
          'slug' => 'bolivia',
        ]);

        DB::table('countries')->insert([
          'title' => 'Bosnia and Herzegovina',
          'slug' => 'bosnia_and_herzegovina',
        ]);

        DB::table('countries')->insert([
          'title' => 'Botswana',
          'slug' => 'botswana',
        ]);

        DB::table('countries')->insert([
          'title' => 'Brazil',
          'slug' => 'brazil',
        ]);

        DB::table('countries')->insert([
          'title' => 'Brunei',
          'slug' => 'brunei',
        ]);

        DB::table('countries')->insert([
          'title' => 'Bulgaria',
          'slug' => 'bulgaria',
        ]);

        DB::table('countries')->insert([
          'title' => 'Burkina Faso',
          'slug' => 'burkina_faso',
        ]);

        DB::table('countries')->insert([
          'title' => 'Burundi',
          'slug' => 'burundi',
        ]);

        DB::table('countries')->insert([
          'title' => 'Cabo Verde',
          'slug' => 'cabo_verde',
        ]);

        DB::table('countries')->insert([
          'title' => 'Cambodia',
          'slug' => 'cambodia',
        ]);

        DB::table('countries')->insert([
          'title' => 'Cameroon',
          'slug' => 'cameroon',
        ]);

        DB::table('countries')->insert([
          'title' => 'Canada',
          'slug' => 'canada',
        ]);

        DB::table('countries')->insert([
          'title' => 'Central African Republic (CAR)',
          'slug' => 'central_african_republic',
        ]);

        DB::table('countries')->insert([
          'title' => 'Chad',
          'slug' => 'chad',
        ]);

        DB::table('countries')->insert([
          'title' => 'Chile',
          'slug' => 'chile',
        ]);

        DB::table('countries')->insert([
          'title' => 'Colombia',
          'slug' => 'colombia',
        ]);

        DB::table('countries')->insert([
          'title' => 'Comoros',
          'slug' => 'comoros',
        ]);

        DB::table('countries')->insert([
          'title' => 'Democratic Republic of the Congo',
          'slug' => 'democratic_republic_of_the_congo',
        ]);

        DB::table('countries')->insert([
          'title' => 'Republic of the Congo',
          'slug' => 'republic_of_the_congo',
        ]);

        DB::table('countries')->insert([
          'title' => 'Costa Rica',
          'slug' => 'costa_rica',
        ]);

        DB::table('countries')->insert([
          'title' => 'Cote dIvoire',
          'slug' => 'cote_divoire',
        ]);

        DB::table('countries')->insert([
          'title' => 'Croatia',
          'slug' => 'croatia',
        ]);

        DB::table('countries')->insert([
          'title' => 'Cuba',
          'slug' => 'cuba',
        ]);

        DB::table('countries')->insert([
          'title' => 'Cyprus',
          'slug' => 'cyprus',
        ]);

        DB::table('countries')->insert([
          'title' => 'Czech Republic',
          'slug' => 'czech_republic',
        ]);

        DB::table('countries')->insert([
          'title' => 'Denmark',
          'slug' => 'denmark',
        ]);

        DB::table('countries')->insert([
          'title' => 'Djibouti',
          'slug' => 'djibouti',
        ]);

        DB::table('countries')->insert([
          'title' => 'Dominica',
          'slug' => 'dominica',
        ]);

        DB::table('countries')->insert([
          'title' => 'Dominican Republic',
          'slug' => 'dominican_republic',
        ]);

        DB::table('countries')->insert([
          'title' => 'Ecuador',
          'slug' => 'ecuador',
        ]);

        DB::table('countries')->insert([
          'title' => 'Egypt',
          'slug' => 'egypt',
        ]);

        DB::table('countries')->insert([
          'title' => 'El Salvador',
          'slug' => 'el_salvador',
        ]);

        DB::table('countries')->insert([
          'title' => 'Equatorial Guinea',
          'slug' => 'equatorial_guinea',
        ]);

        DB::table('countries')->insert([
          'title' => 'Eritrea',
          'slug' => 'eritrea',
        ]);

        DB::table('countries')->insert([
          'title' => 'Estonia',
          'slug' => 'estonia',
        ]);

        DB::table('countries')->insert([
          'title' => 'Ethiopia',
          'slug' => 'ethiopia',
        ]);

        DB::table('countries')->insert([
          'title' => 'Fiji',
          'slug' => 'fiji',
        ]);

        DB::table('countries')->insert([
          'title' => 'Finland',
          'slug' => 'finland',
        ]);

        DB::table('countries')->insert([
          'title' => 'France',
          'slug' => 'france',
        ]);

        DB::table('countries')->insert([
          'title' => 'Gabon',
          'slug' => 'gabon',
        ]);

        DB::table('countries')->insert([
          'title' => 'Gambia',
          'slug' => 'gambia',
        ]);

        DB::table('countries')->insert([
          'title' => 'Georgia',
          'slug' => 'georgia',
        ]);

        DB::table('countries')->insert([
          'title' => 'Germany',
          'slug' => 'germany',
        ]);

        DB::table('countries')->insert([
          'title' => 'Ghana',
          'slug' => 'ghana',
        ]);

        DB::table('countries')->insert([
          'title' => 'Greece',
          'slug' => 'greece',
        ]);

        DB::table('countries')->insert([
          'title' => 'Grenada',
          'slug' => 'grenada',
        ]);

        DB::table('countries')->insert([
          'title' => 'Guatemala',
          'slug' => 'guatemala',
        ]);

        DB::table('countries')->insert([
          'title' => 'Guinea',
          'slug' => 'guinea',
        ]);

        DB::table('countries')->insert([
          'title' => 'Guinea-Bissau',
          'slug' => 'guinea_bissau',
        ]);

        DB::table('countries')->insert([
          'title' => 'Guyana',
          'slug' => 'guyana',
        ]);

        DB::table('countries')->insert([
          'title' => 'Haiti',
          'slug' => 'haiti',
        ]);

        DB::table('countries')->insert([
          'title' => 'Honduras',
          'slug' => 'honduras',
        ]);

        DB::table('countries')->insert([
          'title' => 'Hungary',
          'slug' => 'hungary',
        ]);

        DB::table('countries')->insert([
          'title' => 'Iceland',
          'slug' => 'iceland',
        ]);

        DB::table('countries')->insert([
          'title' => 'India',
          'slug' => 'india',
        ]);

        DB::table('countries')->insert([
          'title' => 'Indonesia',
          'slug' => 'indonesia',
        ]);

        DB::table('countries')->insert([
          'title' => 'Iran',
          'slug' => 'iran',
        ]);

        DB::table('countries')->insert([
          'title' => 'Iraq',
          'slug' => 'iraq',
        ]);

        DB::table('countries')->insert([
          'title' => 'Ireland',
          'slug' => 'ireland',
        ]);

        DB::table('countries')->insert([
          'title' => 'Israel',
          'slug' => 'israel',
        ]);

        DB::table('countries')->insert([
          'title' => 'Italy',
          'slug' => 'italy',
        ]);

        DB::table('countries')->insert([
          'title' => 'Jamaica',
          'slug' => 'jamaica',
        ]);

        DB::table('countries')->insert([
          'title' => 'Japan',
          'slug' => 'japan',
        ]);

        DB::table('countries')->insert([
          'title' => 'Jordan',
          'slug' => 'jordan',
        ]);

        DB::table('countries')->insert([
          'title' => 'Kazakhstan',
          'slug' => 'kazakhstan',
        ]);

        DB::table('countries')->insert([
          'title' => 'Kenya',
          'slug' => 'kenya',
        ]);

        DB::table('countries')->insert([
          'title' => 'Kiribati',
          'slug' => 'kiribati',
        ]);

        DB::table('countries')->insert([
          'title' => 'Kosovo',
          'slug' => 'kosovo',
        ]);

        DB::table('countries')->insert([
          'title' => 'Kuwait',
          'slug' => 'kuwait',
        ]);

        DB::table('countries')->insert([
          'title' => 'Kyrgyzstan',
          'slug' => 'kyrgyzstan',
        ]);

        DB::table('countries')->insert([
          'title' => 'Laos',
          'slug' => 'laos',
        ]);

        DB::table('countries')->insert([
          'title' => 'Latvia',
          'slug' => 'latvia',
        ]);

        DB::table('countries')->insert([
          'title' => 'Lebanon',
          'slug' => 'lebanon',
        ]);

        DB::table('countries')->insert([
          'title' => 'Lesotho',
          'slug' => 'lesotho',
        ]);

        DB::table('countries')->insert([
          'title' => 'Liberia',
          'slug' => 'liberia',
        ]);

        DB::table('countries')->insert([
          'title' => 'Libya',
          'slug' => 'libya',
        ]);

        DB::table('countries')->insert([
          'title' => 'Liechtenstein',
          'slug' => 'liechtenstein',
        ]);

        DB::table('countries')->insert([
          'title' => 'Lithuania',
          'slug' => 'lithuania',
        ]);

        DB::table('countries')->insert([
          'title' => 'Luxembourg',
          'slug' => 'luxembourg',
        ]);

        DB::table('countries')->insert([
          'title' => 'Macedonia (FYROM)',
          'slug' => 'macedonia',
        ]);

        DB::table('countries')->insert([
          'title' => 'Madagascar',
          'slug' => 'madagascar',
        ]);

        DB::table('countries')->insert([
          'title' => 'Malawi',
          'slug' => 'malawi',
        ]);

        DB::table('countries')->insert([
          'title' => 'Malaysia',
          'slug' => 'malaysia',
        ]);

        DB::table('countries')->insert([
          'title' => 'Maldives',
          'slug' => 'maldives',
        ]);

        DB::table('countries')->insert([
          'title' => 'Mali',
          'slug' => 'mali',
        ]);

        DB::table('countries')->insert([
          'title' => 'Malta',
          'slug' => 'malta',
        ]);

        DB::table('countries')->insert([
          'title' => 'Marshall Islands',
          'slug' => 'marshall_islands',
        ]);

        DB::table('countries')->insert([
          'title' => 'Mauritania',
          'slug' => 'mauritania',
        ]);

        DB::table('countries')->insert([
          'title' => 'Mauritius',
          'slug' => 'mauritius',
        ]);

        DB::table('countries')->insert([
          'title' => 'Mexico',
          'slug' => 'mexico',
        ]);

        DB::table('countries')->insert([
          'title' => 'Micronesia',
          'slug' => 'micronesia',
        ]);

        DB::table('countries')->insert([
          'title' => 'Moldova',
          'slug' => 'moldova',
        ]);

        DB::table('countries')->insert([
          'title' => 'Monaco',
          'slug' => 'monaco',
        ]);

        DB::table('countries')->insert([
          'title' => 'Mongolia',
          'slug' => 'mongolia',
        ]);

        DB::table('countries')->insert([
          'title' => 'Montenegro',
          'slug' => 'montenegro',
        ]);

        DB::table('countries')->insert([
          'title' => 'Morocco',
          'slug' => 'morocco',
        ]);

        DB::table('countries')->insert([
          'title' => 'Mozambique',
          'slug' => 'mozambique',
        ]);

        DB::table('countries')->insert([
          'title' => 'Myanmar (Burma)',
          'slug' => 'myanmar',
        ]);

        DB::table('countries')->insert([
          'title' => 'Namibia',
          'slug' => 'namibia',
        ]);

        DB::table('countries')->insert([
          'title' => 'Nauru',
          'slug' => 'nauru',
        ]);

        DB::table('countries')->insert([
          'title' => 'Nepal',
          'slug' => 'nepal',
        ]);

        DB::table('countries')->insert([
          'title' => 'Netherlands',
          'slug' => 'netherlands',
        ]);

        DB::table('countries')->insert([
          'title' => 'New Zealand',
          'slug' => 'new_zealand',
        ]);

        DB::table('countries')->insert([
          'title' => 'Nicaragua',
          'slug' => 'nicaragua',
        ]);

        DB::table('countries')->insert([
          'title' => 'Niger',
          'slug' => 'niger',
        ]);

        DB::table('countries')->insert([
          'title' => 'Nigeria',
          'slug' => 'nigeria',
        ]);

        DB::table('countries')->insert([
          'title' => 'North Korea',
          'slug' => 'north_korea',
        ]);

        DB::table('countries')->insert([
          'title' => 'Norway',
          'slug' => 'norway',
        ]);

        DB::table('countries')->insert([
          'title' => 'Oman',
          'slug' => 'oman',
        ]);

        DB::table('countries')->insert([
          'title' => 'Pakistan',
          'slug' => 'pakistan',
        ]);

        DB::table('countries')->insert([
          'title' => 'Palau',
          'slug' => 'palau',
        ]);

        DB::table('countries')->insert([
          'title' => 'Palestine',
          'slug' => 'palestine',
        ]);

        DB::table('countries')->insert([
          'title' => 'Panama',
          'slug' => 'panama',
        ]);

        DB::table('countries')->insert([
          'title' => 'Papua New Guinea',
          'slug' => 'papua_new_guinea',
        ]);

        DB::table('countries')->insert([
          'title' => 'Paraguay',
          'slug' => 'paraguay',
        ]);

        DB::table('countries')->insert([
          'title' => 'Peru',
          'slug' => 'peru',
        ]);

        DB::table('countries')->insert([
          'title' => 'Philippines',
          'slug' => 'philippines',
        ]);

        DB::table('countries')->insert([
          'title' => 'Poland',
          'slug' => 'poland',
        ]);

        DB::table('countries')->insert([
          'title' => 'Portugal',
          'slug' => 'portugal',
        ]);

        DB::table('countries')->insert([
          'title' => 'Qatar',
          'slug' => 'qatar',
        ]);

        DB::table('countries')->insert([
          'title' => 'Romania',
          'slug' => 'romania',
        ]);

        DB::table('countries')->insert([
          'title' => 'Russia',
          'slug' => 'russia',
        ]);

        DB::table('countries')->insert([
          'title' => 'Rwanda',
          'slug' => 'rwanda',
        ]);

        DB::table('countries')->insert([
          'title' => 'Saint Kitts and Nevis',
          'slug' => 'saint_kitts_and_nevis',
        ]);

        DB::table('countries')->insert([
          'title' => 'Saint Lucia',
          'slug' => 'saint_lucia',
        ]);

        DB::table('countries')->insert([
          'title' => 'Saint Vicent and the Grenadines',
          'slug' => 'saint_vicent_and_the_grenadines',
        ]);

        DB::table('countries')->insert([
          'title' => 'Samoa',
          'slug' => 'samoa',
        ]);

        DB::table('countries')->insert([
          'title' => 'San Marino',
          'slug' => 'san_marino',
        ]);

        DB::table('countries')->insert([
          'title' => 'Sao Tome and Principe',
          'slug' => 'sao_tome_and_principe',
        ]);

        DB::table('countries')->insert([
          'title' => 'Saudi Arabia',
          'slug' => 'saudi_arabia',
        ]);

        DB::table('countries')->insert([
          'title' => 'Senegal',
          'slug' => 'senegal',
        ]);

        DB::table('countries')->insert([
          'title' => 'Serbia',
          'slug' => 'serbia',
        ]);

        DB::table('countries')->insert([
          'title' => 'Seychelles',
          'slug' => 'seychelles',
        ]);

        DB::table('countries')->insert([
          'title' => 'Sierra Leone',
          'slug' => 'sierra_leone',
        ]);

        DB::table('countries')->insert([
          'title' => 'Singapore',
          'slug' => 'singapore',
        ]);

        DB::table('countries')->insert([
          'title' => 'Slovakia',
          'slug' => 'slovakia',
        ]);

        DB::table('countries')->insert([
          'title' => 'Slovenia',
          'slug' => 'slovenia',
        ]);

        DB::table('countries')->insert([
          'title' => 'Solomon Islands',
          'slug' => 'solomon_islands',
        ]);

        DB::table('countries')->insert([
          'title' => 'Somalia',
          'slug' => 'somalia',
        ]);

        DB::table('countries')->insert([
          'title' => 'South Africa',
          'slug' => 'south_africa',
        ]);

        DB::table('countries')->insert([
          'title' => 'South Korea',
          'slug' => 'south_korea',
        ]);

        DB::table('countries')->insert([
          'title' => 'South Sudan',
          'slug' => 'south_sudan',
        ]);

        DB::table('countries')->insert([
          'title' => 'Spain',
          'slug' => 'spain',
        ]);

        DB::table('countries')->insert([
          'title' => 'Sri Lanka',
          'slug' => 'sri_lanka',
        ]);

        DB::table('countries')->insert([
          'title' => 'Sudan',
          'slug' => 'sudan',
        ]);

        DB::table('countries')->insert([
          'title' => 'Suriname',
          'slug' => 'suriname',
        ]);

        DB::table('countries')->insert([
          'title' => 'Swaziland',
          'slug' => 'swaziland',
        ]);

        DB::table('countries')->insert([
          'title' => 'Sweden',
          'slug' => 'sweden',
        ]);

        DB::table('countries')->insert([
          'title' => 'Switzerland',
          'slug' => 'switzerland',
        ]);

        DB::table('countries')->insert([
          'title' => 'Syria',
          'slug' => 'syria',
        ]);

        DB::table('countries')->insert([
          'title' => 'Taiwan',
          'slug' => 'taiwan',
        ]);

        DB::table('countries')->insert([
          'title' => 'Tajikistan',
          'slug' => 'tajikistan',
        ]);

        DB::table('countries')->insert([
          'title' => 'Tanzania',
          'slug' => 'tanzania',
        ]);

        DB::table('countries')->insert([
          'title' => 'Thailand',
          'slug' => 'thailand',
        ]);

        DB::table('countries')->insert([
          'title' => 'Timor-Leste',
          'slug' => 'timor_leste',
        ]);

        DB::table('countries')->insert([
          'title' => 'Togo',
          'slug' => 'togo',
        ]);

        DB::table('countries')->insert([
          'title' => 'Tonga',
          'slug' => 'tonga',
        ]);

        DB::table('countries')->insert([
          'title' => 'Trinidad and Tobago',
          'slug' => 'trinidad_and_tobago',
        ]);

        DB::table('countries')->insert([
          'title' => 'Tunisia',
          'slug' => 'tunisia',
        ]);

        DB::table('countries')->insert([
          'title' => 'Turkey',
          'slug' => 'turkey',
        ]);

        DB::table('countries')->insert([
          'title' => 'Turkmenistan',
          'slug' => 'turkmenistan',
        ]);

        DB::table('countries')->insert([
          'title' => 'Tuvalu',
          'slug' => 'tuvalu',
        ]);

        DB::table('countries')->insert([
          'title' => 'Uganda',
          'slug' => 'uganda',
        ]);

        DB::table('countries')->insert([
          'title' => 'Ukraine',
          'slug' => 'ukraine',
        ]);

        DB::table('countries')->insert([
          'title' => 'United Arab Emirates (UAE)',
          'slug' => 'united_arab_emirates',
        ]);

        DB::table('countries')->insert([
          'title' => 'United Kingdom (UK)',
          'slug' => 'united_kingdom',
        ]);

        DB::table('countries')->insert([
          'title' => 'United States of America',
          'slug' => 'united_states_of_america',
        ]);

        DB::table('countries')->insert([
          'title' => 'Uruguay',
          'slug' => 'uruguay',
        ]);

        DB::table('countries')->insert([
          'title' => 'Uzbekistan',
          'slug' => 'uzbekistan',
        ]);

        DB::table('countries')->insert([
          'title' => 'Vanuatu',
          'slug' => 'vanuatu',
        ]);

        DB::table('countries')->insert([
          'title' => 'Vatican City (Holy See)',
          'slug' => 'vatican_city',
        ]);

        DB::table('countries')->insert([
          'title' => 'Venezuela',
          'slug' => 'venezuela',
        ]);

        DB::table('countries')->insert([
          'title' => 'Vietnam',
          'slug' => 'vietnam',
        ]);

        DB::table('countries')->insert([
          'title' => 'Yemen',
          'slug' => 'yemen',
        ]);

        DB::table('countries')->insert([
          'title' => 'Zambia',
          'slug' => 'zambia',
        ]);

        DB::table('countries')->insert([
          'title' => 'Zimbabwe',
          'slug' => 'zimbabwe',
        ]);
    }
}
