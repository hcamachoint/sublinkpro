<?php

use Illuminate\Database\Seeder;

class WorkCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('work_categories')->insert([
          'name' => 'construction',
          'description' => 'Construction',
          'slug' => 'construction',
      ]);

      DB::table('work_categories')->insert([
          'name' => 'remodeling',
          'description' => 'Remodeling',
          'slug' => 'remodeling',
      ]);

      DB::table('work_categories')->insert([
          'name' => 'installation',
          'description' => 'MInstallation',
          'slug' => 'installation',
      ]);

      DB::table('work_categories')->insert([
          'name' => 'other',
          'description' => 'Other',
          'slug' => 'other',
      ]);
    }
}
