<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      //USERS
      $ummc = DB::table('users')->insert([
        'idn' => '00000000',
        'name' => 'Rafael Morros',
        'url' => 'https://mmconstructionllc.us',
        'email' => 'rafaelemorros@gmail.com',
        'type' => 2,
        'is_verified' => 1,
        'password' => bcrypt('sublink'),
        'created_at' => Carbon\Carbon::now(),
        'updated_at' => Carbon\Carbon::now(),
      ]);

      $uadmin = DB::table('users')->insert([
        'idn' => '00000001',
        'name' => 'Admin',
        'sex' => 'male',
        'email' => 'admin@local.dev',
        'is_verified' => 1,
        'password' => bcrypt('asd123'),
        'type' => 1,
        'created_at' => Carbon\Carbon::now(),
        'updated_at' => Carbon\Carbon::now(),
      ]);

      //ROLES
      $radmin = DB::table('roles')->insert([
        'title' => 'Administrador',
        'description' => 'Administrador del sistema',
        'slug' => 'admin',
        'created_at' => Carbon\Carbon::now(),
        'updated_at' => Carbon\Carbon::now(),
      ]);

      $ruser = DB::table('roles')->insert([
        'title' => 'User',
        'description' => 'Usuario del sistema',
        'slug' => 'user',
        'created_at' => Carbon\Carbon::now(),
        'updated_at' => Carbon\Carbon::now(),
      ]);

      //USER ROLES
      DB::table('role_user')->insert([
        'role_id' => 1,
        'user_id' => 1,
        'created_at' => Carbon\Carbon::now(),
        'updated_at' => Carbon\Carbon::now(),
      ]);

      DB::table('role_user')->insert([
        'role_id' => 1,
        'user_id' => 2,
        'created_at' => Carbon\Carbon::now(),
        'updated_at' => Carbon\Carbon::now(),
      ]);

      /*$faker = Faker\Factory::create();

      $limit = 33;

      for ($i = 0; $i < $limit; $i++) {
          DB::table('document_categories')->insert([ //,
              'name' => $faker->unique()->name,
              'description' => $faker->paragraph,
          ]);
      }*/

    }
}
