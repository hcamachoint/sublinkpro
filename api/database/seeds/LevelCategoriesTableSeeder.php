<?php

use Illuminate\Database\Seeder;

class LevelCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('level_categories')->insert([
          	'name' => 'easy',
          	'description' => 'Easy Work',
	    ]);

	    DB::table('level_categories')->insert([
	        'name' => 'intermetiate',
	        'description' => 'Intermetiate Work',
	    ]);
    }
}
