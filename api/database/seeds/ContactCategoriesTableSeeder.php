<?php

use Illuminate\Database\Seeder;

class ContactCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('contact_categories')->insert([
          'name' => 'email',
          'description' => 'Correo Electronico',
      ]);

      DB::table('contact_categories')->insert([
          'name' => 'telefonom',
          'description' => 'Telefono Movil',
      ]);

      DB::table('contact_categories')->insert([
          'name' => 'telefonol',
          'description' => 'Telefono Local',
      ]);

      DB::table('contact_categories')->insert([
          'name' => 'telefonoo',
          'description' => 'Telefono Oficina',
      ]);
    }
}
