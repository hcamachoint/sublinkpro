<?php

use Illuminate\Database\Seeder;

class AbilityCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('ability_categories')->insert([
          'name' => 'developer',
          'description' => 'Web Developer',
      ]);

      DB::table('ability_categories')->insert([
          'name' => 'designer',
          'description' => 'Web Designer',
      ]);
    }
}
