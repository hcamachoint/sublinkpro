<?php

use Illuminate\Database\Seeder;

class DocumentCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('document_categories')->insert([
          'name' => 'drive_license',
          'description' => 'Driver License',
      ]);

      DB::table('document_categories')->insert([
          'name' => 'insurance',
          'description' => 'Insurance',
      ]);
    }
}
