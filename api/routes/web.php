<?php

//Rutas comunes
Route::get('/',function(){return view('welcome');});
Route::get('/veterans', ['uses' => function(){return view('auth.veteran');}, 'middleware' => 'guest'])->name('veterans');
Route::get('/about',function(){return view('about');})->name('about');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/help', 'HelpController@index')->name('help');
Route::get('/support', 'SupportController@index')->name('supports');

//Rutas de login y confirmacion
Auth::routes();
Route::post('/veterans/register', 'Auth\RegisterController@registerVeteran')->name('register-veteran');
Route::get('/users/confirmation/{token}', 'Auth\RegisterController@confirmation')->name('confirmation');
Route::post('/login', ['uses' => 'Auth\LoginController@login', 'middleware' => 'checkstatus']);

//Rutas de notificaciones
Route::get('/notifications', 'NotificationController@index')->name('notifications');
Route::get('/markAsRead',function(){auth()->user()->unreadNotifications->markAsRead();});

//Rutas del modelo polimorfico Contact
Route::group(['prefix' => 'contact'], function () {
	Route::post('/create/{id}', 'ContactController@store')->name('contact-store');
	Route::put('/update/{id}', 'ContactController@update')->name('contact-update');
	Route::delete('/destroy/{id}', 'ContactController@destroy')->name('contact-destroy');
});

//Rutas del modelo polimorfico Ability
Route::group(['prefix' => 'ability'], function () {
	Route::post('/create/{id}', 'AbilityController@store')->name('ability-store');
	Route::delete('/destroy/{id}', 'AbilityController@destroy')->name('ability-destroy');
});

//Rutas del modelo polimorfico Location
Route::group(['prefix' => 'location'], function () {
	Route::post('/create/{id}', 'LocationController@store')->name('location-store');
	Route::put('/update/{id}', 'LocationController@update')->name('location-update');
});

//Rutas del modelo polimorfico Document
Route::group(['prefix' => 'document'], function () {
	Route::post('/create/{id}', 'DocumentController@store')->name('document-store');
	Route::put('/update/{id}', 'DocumentController@update')->name('document-update');
	Route::delete('/destroy/{id}', 'DocumentController@destroy')->name('document-destroy');
});

//Rutas del modulo de usuarios
Route::group(['prefix' => 'profile'], function () {
	Route::get('/', 'UserController@index')->name('profile');
	Route::get('/edit/{id}', 'UserController@edit')->name('profile-edit');
	Route::put('/update', 'UserController@update')->name('profile-update');
	Route::delete('/desactive', 'UserController@destroy')->name('profile-desactive');
	Route::post('/picture', 'UserController@update_picture')->name('profile-picture');

	Route::get('/userpic', 'UserController@userPicture')->name('user-picture');
	Route::get('/userinc', 'UserController@userInformation')->name('user-information');
	Route::get('/usercon', 'UserController@userContact')->name('user-contact');
	Route::get('/userabi', 'UserController@userAbilities')->name('user-abilities');
	Route::get('/userloc', 'UserController@userLocation')->name('user-location');
	Route::get('/userdoc', 'UserController@userDocuments')->name('user-documents');
  Route::get('/usersoc', 'UserController@userSocials')->name('user-social');

	Route::post('/skill/create', 'AbilityController@store')->name('abilityp-store');
	Route::put('/skill/update/{id}', 'AbilityController@update')->name('abilityp-update');
	Route::delete('/skill/destroy/{id}', 'AbilityController@destroy')->name('abilityp-destroy');

	Route::post('/social/create', 'SocialController@store')->name('social-store');
	Route::put('/social/update/{id}', 'SocialController@update')->name('social-update');
	Route::delete('/social/destroy/{id}', 'SocialController@destroy')->name('social-destroy');

	Route::get('/list', 'UserController@list');
});

Route::group(['prefix' => 'security'], function () {
  Route::get('/security', 'SecurityController@index')->name('security-index');
  Route::get('/security/password', 'SecurityController@password')->name('security-password');
  Route::put('/security/password', 'SecurityController@passwordUpdate')->name('security-passwordu');
});

//Rutas del modulo de trabajos
Route::group(['prefix' => 'work'], function () {
	Route::get('/', 'WorkController@index')->name('works');
	Route::get('/show/{id}', 'WorkController@show')->name('work-show');

	Route::get('/create', 'WorkController@create')->name('work-create');

	Route::get('/create2', 'WorkController@create2');
	Route::post('/create2', 'WorkController@create2')->name('work-create2');

	Route::get('/create3', 'WorkController@create3');
	Route::post('/create3', 'WorkController@create3')->name('work-create3');

	Route::get('/create4', 'WorkController@create4');
	Route::post('/create4', 'WorkController@create4')->name('work-create4');

	Route::post('/store', 'WorkController@store')->name('work-store');

	Route::get('/edit/{id}', 'WorkController@edit')->name('work-edit');
	Route::put('/update/{id}', 'WorkController@update')->name('work-update');
	Route::delete('/destroy/{id}', 'WorkController@destroy')->name('work-destroy');
	Route::put('/close/{id}', 'WorkController@close')->name('work-close');

	Route::get('/contact/edit/{id}', 'WorkController@contacte')->name('work-contacte');
	Route::post('/contact/{id}/{proposal}', 'WorkController@contact')->name('work-contact');
});

//Rutas del modulo de busqueda de empleo
Route::group(['prefix' => 'jobs'], function () {
	Route::get('/', 'JobController@index')->name('jobs');
	Route::post('/', 'JobController@search')->name('job-search');
	Route::get('/show/{id}', 'JobController@show')->name('job-show');
	Route::post('/proposal/{id}', 'JobController@proposal')->name('job-proposal');
});

//Rutas del panel administrativo
Route::group(['prefix' => 'panel'], function() {
	Route::get('/', 'AdminController@index')->name('admin-home');
	Route::post('/', 'AdminController@search')->name('admin-search');
	Route::get('/dashboard', 'AdminController@dashboardMenu')->name('admin-dash');

	Route::get('/jobs', 'AdminController@jobList')->name('job-list');
	Route::get('/jobs/{id}', 'AdminController@jobView')->name('job-view');
	Route::post('/job/close/{id}', 'AdminController@jobClose')->name('job-close');
	Route::post('/job/report/{id}', 'AdminController@jobReport')->name('job-report');

	Route::get('/users', 'AdminController@userList')->name('user-list');
	Route::get('/users/{id}', 'AdminController@userView')->name('user-view');
	Route::post('/users/admin/{id}', 'AdminController@userAdmin')->name('user-adm');
	Route::post('/users/noadm/{id}', 'AdminController@userNoAdmin')->name('user-noadm');
	Route::post('/users/delete/{id}', 'AdminController@userDelete')->name('user-delete');
	Route::post('/users/ban/{id}', 'AdminController@userBan')->name('user-ban');

	Route::get('/skill', 'AdminController@skillList')->name('abi-list');
	Route::post('/skill', 'AdminController@skillStore')->name('abi-store');
	Route::get('/skill/{id}', 'AdminController@skillEdit')->name('abi-edit');
	Route::put('/skill/update/{id}', 'AdminController@skillUpdate')->name('abi-update');
	Route::delete('/skill/delete/{id}', 'AdminController@skillDelete')->name('abi-delete');

  Route::get('/doccat', 'AdminController@doccatList')->name('doc-list');
	Route::post('/doccat', 'AdminController@doccatStore')->name('doc-store');
	Route::get('/doccat/{id}', 'AdminController@doccatEdit')->name('doc-edit');
	Route::put('/doccat/update/{id}', 'AdminController@doccatUpdate')->name('doc-update');
	Route::delete('/doccat/delete/{id}', 'AdminController@doccatDelete')->name('doc-delete');

  Route::get('/soccat', 'AdminController@soccatList')->name('soc-list');
	Route::post('/soccat', 'AdminController@soccatStore')->name('soc-store');
	Route::get('/soccat/{id}', 'AdminController@soccatEdit')->name('soc-edit');
	Route::put('/soccat/update/{id}', 'AdminController@soccatUpdate')->name('soc-update');
	Route::delete('/soccat/delete/{id}', 'AdminController@soccatDelete')->name('soc-delete');

	Route::get('/workcat', 'AdminController@workcatList')->name('woc-list');
	Route::post('/workcat', 'AdminController@workcatStore')->name('woc-store');
	Route::get('/workcat/{id}', 'AdminController@workcatEdit')->name('woc-edit');
	Route::put('/workcat/update/{id}', 'AdminController@workcatUpdate')->name('woc-update');
	Route::delete('/workcat/delete/{id}', 'AdminController@workcatDelete')->name('woc-delete');

	Route::get('/worklev', 'AdminController@worklevList')->name('wol-list');
	Route::post('/worklev', 'AdminController@worklevStore')->name('wol-store');
	Route::get('/worklev/{id}', 'AdminController@worklevEdit')->name('wol-edit');
	Route::put('/worklev/update/{id}', 'AdminController@worklevUpdate')->name('wol-update');
	Route::delete('/worklev/delete/{id}', 'AdminController@worklevDelete')->name('wol-delete');

	Route::get('/procat', 'AdminController@procatList')->name('poc-list');
	Route::post('/procat', 'AdminController@procatStore')->name('poc-store');
	Route::get('/procat/{id}', 'AdminController@procatEdit')->name('poc-edit');
	Route::put('/procat/update/{id}', 'AdminController@procatUpdate')->name('poc-update');
	Route::delete('/procat/delete/{id}', 'AdminController@procatDelete')->name('poc-delete');

	Route::get('/analytics', 'AdminController@analytics')->name('analytic-index');
	Route::get('/settings', 'AdminController@settings')->name('setting-index');
});

//Rutas de propuestas del modulo de busqueda de empleo
Route::group(['prefix' => 'proposals'], function () {
	Route::post('/store/{id}', 'ProposalController@store')->name('proposal-store');
	Route::post('/user/{id}', 'FeedbackController@user')->name('feedback-user');
	Route::post('/contractor/{id}', 'FeedbackController@contractor')->name('feedback-contractor');
});
