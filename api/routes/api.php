<?php

//PUBLIC
Route::post('register', 'Api\AuthController@register');
Route::post('login', 'Api\AuthController@login');
Route::post('recover', 'Api\AuthController@recover');

//AUTH
Route::group(['middleware' => ['jwt.auth']], function() {
    Route::get('logout', 'Api\AuthController@logout');
    Route::put('passchange', 'Api\AuthController@passwordUpdate');
});

//PROFILE
Route::group(['prefix' => 'profile', 'middleware' => ['jwt.auth']], function () {
	Route::put('/', 'Api\ProfileController@update');
  Route::delete('/desactive', 'Api\ProfileController@desactive');
	Route::post('/picture', 'Api\ProfileController@update_picture');
});

//CATEGORIAS
Route::get('skillcat', 'Api\DataListController@skill');
Route::get('procat', 'Api\DataListController@project');
Route::get('doccat', 'Api\DataListController@document');
Route::get('workcat', 'Api\DataListController@workcat');
Route::get('worklev', 'Api\DataListController@worklev');
Route::get('soccat', 'Api\DataListController@social');
Route::get('cities', 'Api\DataListController@city');

//SOCIALS
Route::group(['prefix' => 'social', 'middleware' => ['jwt.auth']], function() {
    Route::get('/', 'Api\SocialProfileController@list');
    Route::get('/{cod}', 'Api\SocialProfileController@view');
    Route::post('/{id}', 'Api\SocialProfileController@store');
    Route::put('/{id}', 'Api\SocialProfileController@update');
    Route::delete('/{id}', 'Api\SocialProfileController@destroy');
});

//DOCUMENTS (POLIMORFICO)
Route::group(['prefix' => 'document', 'middleware' => ['jwt.auth']], function() {
    Route::get('/', 'Api\DocumentController@list');
    Route::get('/{cod}', 'Api\DocumentController@view');
    Route::post('/{id}', 'Api\DocumentController@store');
    Route::put('/{id}/{cod}', 'Api\DocumentController@update');
    Route::delete('/{id}/{cod}', 'Api\DocumentController@destroy');
});

//CONTACT METHODS (POLIMORFICO)
Route::group(['prefix' => 'contact', 'middleware' => ['jwt.auth']], function() {
    Route::get('/', 'Api\ContactMethodController@list');
    Route::get('/{cod}', 'Api\ContactMethodController@view');
    Route::post('/{id}', 'Api\ContactMethodController@store');
    Route::put('/{id}/{cod}', 'Api\ContactMethodController@update');
    Route::delete('/{id}/{cod}', 'Api\ContactMethodController@destroy');
});

//LOCATION (POLIMORFICO)
Route::group(['prefix' => 'location', 'middleware' => ['jwt.auth']], function() {
    Route::get('/{cod}', 'Api\LocationController@view');
    Route::post('/{id}', 'Api\LocationController@store');
    Route::put('/{id}', 'Api\LocationController@update');
});

//SKILLS (POLIMORFICO)
Route::group(['prefix' => 'skill', 'middleware' => ['jwt.auth']], function() {
    Route::get('/', 'Api\SkillController@list');
    Route::get('/{cod}', 'Api\SkillController@view');
    Route::post('/{id}', 'Api\SkillController@store');
    Route::delete('/{id}/{cod}', 'Api\SkillController@destroy');
});

//POSTS
Route::group(['prefix' => 'work', 'middleware' => ['jwt.auth']], function() {
    Route::get('/', 'Api\WorkController@list');
    Route::get('/{cod}', 'Api\WorkController@view');
    Route::post('/', 'Api\WorkController@store');
    Route::put('/{id}/{cod}', 'Api\WorkController@update');
    Route::delete('/{cod}', 'Api\WorkController@destroy');
    Route::get('/close/{cod}', 'Api\WorkController@close');
});

//FIND WORK
Route::group(['prefix' => 'job', 'middleware' => ['jwt.auth']], function() {
    Route::get('{text?}{skill?}', 'Api\JobController@search');
    Route::get('/{cod}', 'Api\JobController@view');
    Route::post('/{cod}', 'Api\JobController@proposal');
});
