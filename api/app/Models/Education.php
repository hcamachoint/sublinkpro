<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Education extends Model 
{

    protected $table = 'educations';
    public $timestamps = true;
    protected $fillable = array('school', 'degree', 'perioda', 'periodb', 'active', 'area', 'description', 'user_id');

}