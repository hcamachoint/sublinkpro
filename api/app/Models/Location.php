<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{

    protected $table = 'locations';
    public $timestamps = true;
    protected $fillable = array('city_id', 'zip', 'address', 'locationable_id', 'locationable_type');

    public function locationable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->morphMany('App\Models\User', 'id', 'locationable_id');
    }

    public function timezone()
    {
        return $this->hasOne('App\Models\TimeZone', 'id', 'timezone_id');
    }

    public function city()
    {
        return $this->hasOne('App\Models\City', 'id', 'city_id');
    }

    public function job()
    {
        return $this->morphMany('App\Models\Job', 'id', 'locationable_id');
    }

}
