<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
  protected $table = 'states';
  public $timestamps = true;
  protected $fillable = array('title', 'slug', 'country_id');

  public function country()
  {
      return $this->hasOne('App\Models\Country', 'id', 'country_id');
  }
}
