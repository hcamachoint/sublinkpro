<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LevelCategory extends Model 
{

    protected $table = 'level_categories';
    public $timestamps = true;
    protected $fillable = array('name', 'description');

}