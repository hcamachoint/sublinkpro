<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ability extends Model 
{

    protected $table = 'abilities';
    public $timestamps = true;
    protected $fillable = array('category_id', 'abilityable_id', 'abilityable_type');

    public function abilityable()
    {
        return $this->morphTo();
    }

    public function category()
    {
        return $this->hasOne('App\Models\AbilityCategory', 'id', 'category_id');
    }

    public function work()
    {
        return $this->belongsTo('App\Models\Work', 'abilityable_id', 'id');
    }

}