<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model 
{

    protected $table = 'contacts';
    public $timestamps = true;
    protected $fillable = array('address', 'category_id', 'contactable_id', 'contactable_type');

    public function category()
    {
        return $this->hasOne('App\Models\ContactCategory', 'id', 'category_id');
    }

    public function contactable()
    {
        return $this->morphTo();
    }

    public function work()
    {
        return $this->belongsTo('App\Models\Work', 'contactable_id', 'id');
    }

}