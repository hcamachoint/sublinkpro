<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Briefcase extends Model 
{

    protected $table = 'briefcases';
    public $timestamps = true;
    protected $fillable = array('title', 'description', 'date', 'url', 'user_id');

}