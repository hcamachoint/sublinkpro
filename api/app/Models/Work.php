<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Work extends Model 
{

    protected $table = 'works';
    public $timestamps = true;

    use SoftDeletes, Notifiable;

    protected $dates = ['deleted_at'];
    protected $fillable = array('title', 'description', 'pstructure', 'budget', 'startd', 'texpect', 'requireds', 'category_id', 'project_id', 'level_id', 'user_id');

    public function ability()
    {
        return $this->morphMany('App\Models\Ability', 'abilityable');
    }

    public function category()
    {
        return $this->hasOne('App\Models\WorkCategory', 'id', 'category_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function file()
    {
        return $this->morphMany('App\Models\File');
    }

    public function location()
    {
        return $this->morphOne('App\Models\Location', 'locationable');
    }

    public function project()
    {
        return $this->hasOne('App\Models\ProjectCategory', 'id', 'project_id');
    }

    public function level()
    {
        return $this->hasOne('App\Models\LevelCategory', 'id', 'level_id');
    }

    public function feedback()
    {
        return $this->hasMany('App\Models\Feedback');
    }

    public function proposal()
    {
        return $this->hasMany('App\Models\Proposal');
    }

    public function contact()
    {
        return $this->morphMany('App\Models\Contact', 'contactable');
    }

}