<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoginAttempts extends Model 
{

    protected $table = 'login_attempts';
    public $timestamps = true;
    protected $fillable = array('ip', 'browser_agent', 'success', 'user_id', 'event');

}