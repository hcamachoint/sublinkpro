<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Proposal extends Model
{
    protected $table = 'proposals';
    public $timestamps = true;
    protected $fillable = array('amount', 'esttime', 'message');

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function work()
    {
        return $this->hasOne('App\Models\Work', 'id', 'work_id');
    }

}
