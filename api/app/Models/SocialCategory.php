<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SocialCategory extends Model
{
  protected $table = 'social_categories';
  public $timestamps = true;
  protected $fillable = array('name', 'description', 'url');
}
