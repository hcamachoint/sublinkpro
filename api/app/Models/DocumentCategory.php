<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentCategory extends Model 
{

    protected $table = 'document_categories';
    public $timestamps = true;
    protected $fillable = array('name', 'description');

    public function document()
    {
        return $this->hasMany('App\Models\Document', 'id');
    }

}