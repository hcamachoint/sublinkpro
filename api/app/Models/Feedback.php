<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model 
{

    protected $table = 'feedbacks';
    public $timestamps = true;
    protected $fillable = array('message', 'pointa', 'pointb', 'pointc', 'type', 'user_id', 'job_id');

    public function user()
    {
        return $this->hasOne('App\Models\User');
    }

    public function job()
    {
        return $this->hasOne('App\Models\Job');
    }

}