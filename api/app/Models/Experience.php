<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Experience extends Model 
{

    protected $table = 'experiences';
    public $timestamps = true;
    protected $fillable = array('company', 'title', 'role', 'description', 'perioda', 'periodb', 'active', 'user_id', 'category_id');

    public function category()
    {
        return $this->hasOne('App\Models\WorkCategory', 'id', 'category_id');
    }

}