<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
  protected $table = 'socials';
  public $timestamps = true;
  protected $fillable = array('username');

  public function category()
  {
      return $this->hasOne('App\Models\SocialCategory', 'id', 'category_id');
  }
}
