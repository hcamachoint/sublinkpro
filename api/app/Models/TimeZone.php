<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TimeZone extends Model 
{

    protected $table = 'timezones';
    public $timestamps = true;
    protected $fillable = array('title', 'tz_pn', 'tz_time', 'dst_pn', 'dst_time');

}