<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model 
{

    protected $table = 'roles';
    public $timestamps = true;
    protected $fillable = array('title', 'description', 'slug');

    public function user()
    {
        return $this->belongsToMany('App\Models\User');
    }

}