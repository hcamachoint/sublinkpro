<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banned extends Model
{
	public $timestamps = false;
    protected $table = 'banneds';
    protected $fillable = array('user_id', 'banned_by', 'reason', 'expires', 'lifted', 'lifted_by');
}
