<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectCategory extends Model 
{

    protected $table = 'project_categories';
    public $timestamps = true;
    protected $fillable = array('name', 'description');

}