<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactCategory extends Model 
{

    protected $table = 'contact_categories';
    public $timestamps = true;
    protected $fillable = array('name', 'description');

    public function contact()
    {
        return $this->hasMany('App\Models\Contact', 'category_id');
    }

}