<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{

    protected $table = 'documents';
    public $timestamps = true;
    protected $fillable = array('description', 'type', 'file_path','file_name', 'category_id', 'documentable_id', 'documentable_type');

    public function documentable()
    {
        return $this->morphTo();
    }

    public function category()
    {
        return $this->hasOne('App\Models\DocumentCategory', 'id', 'category_id');
    }

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

}
