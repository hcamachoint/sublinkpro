<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AbilityCategory extends Model 
{

    protected $table = 'ability_categories';
    public $timestamps = true;
    protected $fillable = array('name', 'description');

    public function ability()
    {
        return $this->hasMany('App\Models\Ability', 'category_id');
    }

}