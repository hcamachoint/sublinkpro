<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Role_User;

class AdminPolicy
{
    public function root(User $user)
    {
        return $user->isAdmin();
    }
}
