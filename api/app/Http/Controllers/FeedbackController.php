<?php

namespace App\Http\Controllers;

use App\Models\Feedback;
use App\Models\Contract;
use Illuminate\Http\Request;
use Session;
use Redirect;

class FeedbackController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function user(Request $request, $id)
    {
        $this->validate($request, [
           'description' => 'required | string',
           'pointa' => 'required | digits_between:1,1',
           'pointb' => 'required | digits_between:1,1',
           'pointc' => 'required | digits_between:1,1',
        ]);
        
        try {
        	$contract = Contract::find($id);
        	$tmp = false;
        	if(isset($contract->feedback)){
	            foreach ($contract->feedback as $feedback) {
	                if ($feedback->user->id === $contract->user->id) {
	                    $tmp = true;
	                }
	            }
	        }

	        if ($tmp === false) {
	        	$feed = new Feedback;
	            $feed->fill($request->all());

	            $feed->type = 0;
	            $feed->points = $request->input('pointa') + $request->input('pointb') + $request->input('pointc');
	            $feed->contract_id = $contract->id;
	            $feed->user_id = $contract->user_id;
	            $feed->save();

	            Session::flash('message', 'Successful feedback!');
	            return Redirect::back();
	        }else{
	        	Session::flash('message-error', 'Already this contract has qualification!');
           		return Redirect::back();
	        }
            
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request.');
            return Redirect::back();
        }
    }

    public function contractor(Request $request, $id)
    {
        $this->validate($request, [
           'description' => 'required | string',
           'pointa' => 'required | digits_between:1,1',
           'pointb' => 'required | digits_between:1,1',
           'pointc' => 'required | digits_between:1,1',
        ]);
        
        try {
        	$contract = Contract::find($id);
        	$tmp = false;
        	if(isset($contract->feedback)){
	            foreach ($contract->feedback as $feedback) {
	                if ($feedback->user->id !== $contract->user->id) {
	                    $tmp = true;
	                }
	            }
	        }

	        if ($tmp === false) {
	            $feed = new Feedback;
	            $feed->fill($request->all());

	            $feed->type = 0;
	            $feed->points = $request->input('pointa') + $request->input('pointb') + $request->input('pointc');
	            $feed->contract_id = $contract->id;
	            $feed->user_id = $contract->job->user_id;
	            $feed->save();

	            Session::flash('message', 'Successful feedback!');
	            return Redirect::back();
	        }else{
	        	Session::flash('message-error', 'Already this contract has qualification!');
           		return Redirect::back();
	        }
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request.');
            return Redirect::back();
        }
    }
}
