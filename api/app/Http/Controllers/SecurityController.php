<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Session;
use Redirect;
use Image;
use DB;

class SecurityController extends Controller
{
    public function index()
    {
        return view('security.index');
    }

    public function password()
    {
    	return view('security.password');
    }

    public function passwordUpdate(Request $request)
    {
    	 $rules = [
    		    'old_password' => 'required',
            'password' => 'required|confirmed|min:6|max:18',
        ];

        $messages = [
            'old_password.required' => 'El campo es requerido',
            'password.required' => 'El campo es requerido',
            'password.confirmed' => 'Los passwords no coinciden',
            'password.min' => 'El mínimo permitido son 6 caracteres',
            'password.max' => 'El máximo permitido son 18 caracteres',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()){
            return Redirect::back()->withErrors($validator);
        }
        else{
            if (Hash::check($request->old_password, Auth::user()->password)){
                $user = new User;
                $user->where('email', '=', Auth::user()->email)
                     ->update(['password' => bcrypt($request->password)]);
                return Redirect()->route('security-index')->with('message', 'Password cambiado con éxito');
            }
            else
            {
                return Redirect()->route('security-password')->with('message-error', 'Credenciales incorrectas');
            }
        }

        //OLD
      	$user = auth()->user();
      	$old = $request->input('old_password');
      	$new = $request->input('password');

      	if (Hash::check($user->password, $old)) {
    			$user->password = bcrypt($new);
            	$user->save();
            	Session::flash('message', 'Your password has been successfully updated!');
        		return view('security.password', ['user' => auth()->user()]);
    		}else{
    			Session::flash('message-error', 'An error occurred while trying to process your request');
                return Redirect::back();
    		}
    }
}
