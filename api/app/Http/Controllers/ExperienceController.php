<?php

namespace App\Http\Controllers;

use App\Models\Experience;
use Illuminate\Http\Request;
use Session;
use Redirect;
use App\Http\Controllers\Controller;

class ExperienceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'company' => 'required | string | between:3,100',
            'title' => 'required | string | between:3,50',
            'role' => 'required | string | between:3,200',
            'description' => 'string',
            'perioda' => 'required | date',
            'periodb' => 'required | date',
        ]);

        if (auth()->user()->id != $id) {
            return abort(403, 'Unauthorized action.');
        }
        
        try {
            $exp = new Experience;
            $exp->fill($request->all());
            $exp->user_id = $id;
            $exp->save();
            Session::flash('message', 'Study assigned correctly!');
            return Redirect::back();
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request.');
            return Redirect::back();
        }
        return abort(403, 'Unauthorized action.');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'company' => 'required | string | between:3,100',
            'title' => 'required | string | between:3,50',
            'role' => 'required | string | between:3,200',
            'description' => 'string',
            'perioda' => 'required | date',
            'periodb' => 'required | date',
        ]);

        $exp = Experience::find($id);
        if (auth()->user()->id != $exp->user_id) {
            return abort(403, 'Unauthorized action.');
        }

        try {
            $exp = Experience::find($id);
            if (auth()->user()->id === $exp->user_id) {
                $exp->fill($request->all());
                $exp->save();
                Session::flash('message', 'Experience updated properly!');  
                return Redirect::back();
            }else{
                return abort(403, 'Unauthorized action.');
            }
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request');
            return Redirect::back();
        }
    }

    public function destroy($id)
    {
        $exp = Experience::find($id);
        if (auth()->user()->id != $exp->user_id) {
            return abort(403, 'Unauthorized action.');
        }

        try {
            $exp = Experience::find($id);
            if (auth()->user()->id === $exp->user_id) {
                $exp->delete();
                Session::flash('message', 'Experience removed properly!');
                return Redirect::back();
            }else{
                return abort(403, 'Unauthorized action.');
            }
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request.');
            return Redirect::back();
        } 
    }
}
