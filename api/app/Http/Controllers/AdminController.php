<?php

namespace App\Http\Controllers;

use App\Models\Work;
use App\Models\User;
use App\Models\Role_User;
use App\Models\Role;
use App\Models\Banned;
use App\Models\AbilityCategory;
use App\Models\WorkCategory;
use App\Models\SocialCategory;
use App\Models\LevelCategory;
use App\Models\ProjectCategory;
use App\Models\DocumentCategory;
use Illuminate\Http\Request;
use Session;
use Redirect;
use Auth;
use DB;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:root');
    }

    public function index()
    {
        $users = User::all();
        $jobs = Work::all();
        //$this->authorize('admin-index');
        return view('admins.view', ['users' => count($users), 'jobs' => count($jobs)]);
    }

    public function dashboardMenu(Request $request)
    {
        if ($request->ajax()) {  //CHEQUEA QUE LA SOLICITUD SEA API
            $authUserId = auth()->user()->id;
            $users = DB::table('users')
                    ->get();
            return count($users);
        }
    }

    public function search(Request $request)
    {
        if ($request->input('search') == '') {
            return view('admins.search', ['jobs' => [], 'users' => []]);
        }else{
            $jobs = Work::where('title', 'LIKE', '%'.$request->input('search').'%')->orWhere('description', 'LIKE', '%'.$request->input('search').'%')->where('status', 1)->orderByDesc('created_at')->paginate(20);
            $users = User::where('name', 'LIKE', '%'.$request->input('search').'%')->orWhere('name2', 'LIKE', '%'.$request->input('search').'%')->orWhere('email', 'LIKE', '%'.$request->input('search').'%')->orderByDesc('created_at')->paginate(20);
            return view('admins.search', ['jobs' => $jobs, 'users' => $users]);
        }
    }

    public function analytics()
    {
    	return view('admins.analytics');
    }

    public function settings()
    {
    	return view('admins.settings');
    }

    //USERS SECTION
    public function userList(Request $request)
    {
        $this->middleware('can:root');
        $authUserId = auth()->user()->id;
        $users = User::where('users.id','!=',$authUserId)
                ->whereNull('users.deleted_at')
                ->paginate();
        return view('admins.users.list', ['users' => $users]);
    }

    public function userView($id)
    {
        $user = User::find($id);
        $role = $user->role()->first();
        return view('admins.users.view', ['user' => $user, 'role' => $role]);
    }

    public function userAdmin(Request $request, $id)
    {
        try {
            $role = Role_User::create([
                'role_id' => 1,
                'user_id' => $id,
            ]);
            Session::flash('message', 'User now is admin!');
            return Redirect()->route('user-list');
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request.');
            return Redirect::back();
        }

    }

    public function userNoAdmin(Request $request, $id)
    {
        try {
            $rolu = Role_User::where('user_id', $id)->where('role_id', 1);
            $rolu->delete();

            $user = User::find($id);
            $role = $user->role()->first();
            var_dump($role);
            if ($role == null) {
                $role = Role_User::create([
                    'role_id' => 2,
                    'user_id' => $id,
                ]);

            }

            Session::flash('message', 'User now is user!');
            return Redirect()->route('user-list');
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request.');
            return Redirect::back();
        }

    }

    public function userDelete($id)
    {
        try {
            $user = User::find($id);
            $user->delete();
            //$user->restore(); (PARA RESTAURARLO)
            Session::flash('message', 'User removed properly!');
            return Redirect()->route('user-list');
          }catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request.');
            return Redirect::back();
        }
    }

    public function userBan($id)
    {
        if ($id != auth()->user()->id) {
            $user = User::find($id);
            $ban = new Banned;
            $ban->user_id = $user->id;
            $ban->banned_by = auth()->user()->id;
            $ban->reason = 'Default Rule';
            $ban->expires = '2020-01-01 10:00:00';
            $ban->save();
            return Redirect()->route('user-list');
        }else{
            Session::flash('message-error', 'An error occurred while trying to process your request.');
            return Redirect::back();
        }
    }

    //JOBS SECTION
    public function jobList()
    {
        $this->middleware('can:root');
        $jobs = Work::where('status', 1)->orWhere('status', 2)->orderByDesc('created_at')->paginate(10);
        return view('admins.jobs.index', ['jobs' => $jobs]);
    }

    public function jobView($id)
    {
        $this->middleware('can:root');
        $job = Work::find($id);
        return view('admins.jobs.view', ['job' => $job]);
    }

    public function jobClose($id)
    {
        $this->middleware('can:root');
        try {
            $work = Work::find($id);
            $work->status = 0;
            $work->save();
            Session::flash('message', 'Your Work has been successfully closed!');
            return Redirect()->route('job-list');
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request.');
            return Redirect::back();
        }
    }

    public function jobReport($id)
    {
        $this->middleware('can:root');
        try {
            $work = Work::find($id);
            $work->status = 9;
            $work->save();
            Session::flash('message', 'Your Work has been successfully reported!');
            return Redirect()->route('job-list');
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request.');
            return Redirect::back();
        }
    }

    //ABILITIES SECTION
    public function skillList()
    {
        $this->middleware('can:root');
        $abis = AbilityCategory::all();
        return view('admins.skill.index', ['abis' => $abis]);
    }

    public function skillStore(Request $request)
    {
        $this->middleware('can:root');
        $this->validate($request, [
            'name' => 'required | string | between:1,50',
            'description' => 'required | string',
        ]);

        $abi = new AbilityCategory;
        try {
            $abi->fill($request->all());
            $abi->save();
            Session::flash('message', 'Your Ability has been successfully created!');
            return Redirect()->route('abi-list');
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request');
            return Redirect::back();
        }
    }

    public function skillEdit($id)
    {
        $this->middleware('can:root');
        $abi = AbilityCategory::find($id);
        return view('admins.skill.edit', ['abi' => $abi]);
    }

    public function skillUpdate(Request $request, $id)
    {
        $this->middleware('can:root');
        $this->validate($request, [
            'name' => 'required | string | between:1,50',
            'description' => 'required | string',
        ]);

        $abi = AbilityCategory::find($id);
        try {
            $abi->fill($request->all());
            $abi->save();
            Session::flash('message', 'Ability updated properly!');
            return Redirect()->route('abi-list');
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request');
            return Redirect::back();
        }
    }

    public function skillDelete($id)
    {
        $this->middleware('can:root');
        $abi = AbilityCategory::find($id);
        try {
            $abi->delete();
            Session::flash('message', 'Ability removed properly!');
            return Redirect()->route('abi-list');
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request.');
            return Redirect::back();
        }
    }

    //DOCUMENTS CATEGORIES SECTION
    public function doccatList()
    {
        $this->middleware('can:root');
        $docs = DocumentCategory::all();
        return view('admins.doccat.index', ['docs' => $docs]);
    }

    public function doccatStore(Request $request)
    {
        $this->middleware('can:root');
        $this->validate($request, [
            'name' => 'required | string | between:1,50',
            'description' => 'required | string',
        ]);

        $woc = new DocumentCategory;
        try {
            $woc->fill($request->all());
            $woc->save();
            Session::flash('message', 'Your Document Category has been successfully created!');
            return Redirect()->route('doc-list');
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request');
            return Redirect::back();
        }
    }

    public function doccatEdit($id)
    {
        $this->middleware('can:root');
        $doc = DocumentCategory::find($id);
        return view('admins.doccat.edit', ['doc' => $doc]);
    }

    public function doccatUpdate(Request $request, $id)
    {
        $this->middleware('can:root');
        $this->validate($request, [
            'name' => 'required | string | between:1,50',
            'description' => 'required | string',
        ]);

        $woc = DocumentCategory::find($id);
        try {
            $woc->fill($request->all());
            $woc->save();
            Session::flash('message', 'Document Category updated properly!');
            return Redirect()->route('doc-list');
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request');
            return Redirect::back();
        }
    }

    public function doccatDelete($id)
    {
        $this->middleware('can:root');
        $woc = DocumentCategory::find($id);
        try {
            $woc->delete();
            Session::flash('message', 'Document Category removed properly!');
            return Redirect()->route('doc-list');
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request.');
            return Redirect::back();
        }
    }

    //SOCIAL CATEGORIES SECTION
    public function soccatList()
    {
        $this->middleware('can:root');
        $soccats = SocialCategory::all();
        return view('admins.soccat.index', ['socs' => $soccats]);
    }

    public function soccatStore(Request $request)
    {
        $this->middleware('can:root');
        $this->validate($request, [
            'name' => 'required | string | between:1,50',
            'description' => 'required | string',
            'url' => 'required | url',
        ]);

        $woc = new SocialCategory;
        try {
            $woc->fill($request->all());
            $woc->save();
            Session::flash('message', 'Your Social Category has been successfully created!');
            return Redirect()->route('soc-list');
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request');
            return Redirect::back();
        }
    }

    public function soccatEdit($id)
    {
        $this->middleware('can:root');
        $soc = SocialCategory::find($id);
        return view('admins.soccat.edit', ['soc' => $soc]);
    }

    public function soccatUpdate(Request $request, $id)
    {
        $this->middleware('can:root');
        $this->validate($request, [
            'name' => 'required | string | between:1,50',
            'description' => 'required | string',
            'url' => 'required | url',
        ]);

        $soc = SocialCategory::find($id);
        try {
            $soc->fill($request->all());
            $soc->save();
            Session::flash('message', 'Social Category updated properly!');
            return Redirect()->route('soc-list');
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request');
            return Redirect::back();
        }
    }

    public function soccatDelete($id)
    {
        $this->middleware('can:root');
        $soc = SocialCategory::find($id);
        try {
            $soc->delete();
            Session::flash('message', 'Social Category removed properly!');
            return Redirect()->route('soc-list');
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request.');
            return Redirect::back();
        }
    }

    //WORK CATEGORIES SECTION
    public function workcatList()
    {
        $this->middleware('can:root');
        $wocs = WorkCategory::all();
        return view('admins.workcat.index', ['wocs' => $wocs]);
    }

    public function workcatStore(Request $request)
    {
        $this->middleware('can:root');
        $this->validate($request, [
            'name' => 'required | string | between:1,50',
            'description' => 'required | string',
            'slug' => 'required | string | between:1,20',
        ]);

        $woc = new WorkCategory;
        try {
            $woc->fill($request->all());
            $woc->save();
            Session::flash('message', 'Your Work Category has been successfully created!');
            return Redirect()->route('woc-list');
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request');
            return Redirect::back();
        }
    }

    public function workcatEdit($id)
    {
        $this->middleware('can:root');
        $woc = WorkCategory::find($id);
        return view('admins.workcat.edit', ['woc' => $woc]);
    }

    public function workcatUpdate(Request $request, $id)
    {
        $this->middleware('can:root');
        $this->validate($request, [
            'name' => 'required | string | between:1,50',
            'description' => 'required | string',
            'slug' => 'required | string | between:1,20',
        ]);

        $woc = WorkCategory::find($id);
        try {
            $woc->fill($request->all());
            $woc->save();
            Session::flash('message', 'Work Category updated properly!');
            return Redirect()->route('woc-list');
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request');
            return Redirect::back();
        }
    }

    public function workcatDelete($id)
    {
        $this->middleware('can:root');
        $woc = WorkCategory::find($id);
        try {
            $woc->delete();
            Session::flash('message', 'Work Category removed properly!');
            return Redirect()->route('woc-list');
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request.');
            return Redirect::back();
        }
    }

    //WORK LEVELS SECTION
    public function worklevList()
    {
        $this->middleware('can:root');
        $wols = LevelCategory::all();
        return view('admins.worklev.index', ['wols' => $wols]);
    }

    public function worklevStore(Request $request)
    {
        $this->middleware('can:root');
        $this->validate($request, [
            'name' => 'required | string | between:1,50',
            'description' => 'required | string',
        ]);

        $wol = new LevelCategory;
        try {
            $wol->fill($request->all());
            $wol->save();
            Session::flash('message', 'Your Work Level has been successfully created!');
            return Redirect()->route('wol-list');
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request');
            return Redirect::back();
        }
    }

    public function worklevEdit($id)
    {
        $this->middleware('can:root');
        $wol = LevelCategory::find($id);
        return view('admins.worklev.edit', ['wol' => $wol]);
    }

    public function worklevUpdate(Request $request, $id)
    {
        $this->middleware('can:root');
        $this->validate($request, [
            'name' => 'required | string | between:1,50',
            'description' => 'required | string',
        ]);

        $wol = LevelCategory::find($id);
        try {
            $wol->fill($request->all());
            $wol->save();
            Session::flash('message', 'Work Category updated properly!');
            return Redirect()->route('wol-list');
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request');
            return Redirect::back();
        }
    }

    public function worklevDelete($id)
    {
        $this->middleware('can:root');
        $wol = LevelCategory::find($id);
        try {
            $wol->delete();
            Session::flash('message', 'Work Category removed properly!');
            return Redirect()->route('wol-list');
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request.');
            return Redirect::back();
        }
    }

    //PROJECT CATEGORIES SECTION
    public function procatList()
    {
        $this->middleware('can:root');
        $pocs = ProjectCategory::all();
        return view('admins.procat.index', ['pocs' => $pocs]);
    }

    public function procatStore(Request $request)
    {
        $this->middleware('can:root');
        $this->validate($request, [
            'name' => 'required | string | between:1,50',
            'description' => 'required | string',
        ]);

        $poc = new ProjectCategory;
        try {
            $poc->fill($request->all());
            $poc->save();
            Session::flash('message', 'Your Project Category has been successfully created!');
            return Redirect()->route('poc-list');
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request');
            return Redirect::back();
        }
    }

    public function procatEdit($id)
    {
        $this->middleware('can:root');
        $poc = ProjectCategory::find($id);
        return view('admins.procat.edit', ['poc' => $poc]);
    }

    public function procatUpdate(Request $request, $id)
    {
        $this->middleware('can:root');
        $this->validate($request, [
            'name' => 'required | string | between:1,50',
            'description' => 'required | string',
        ]);

        $poc = ProjectCategory::find($id);
        try {
            $poc->fill($request->all());
            $poc->save();
            Session::flash('message', 'Work Category updated properly!');
            return Redirect()->route('poc-list');
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request');
            return Redirect::back();
        }
    }

    public function procatDelete($id)
    {
        $this->middleware('can:root');
        $poc = ProjectCategory::find($id);
        try {
            $poc->delete();
            Session::flash('message', 'Work Category removed properly!');
            return Redirect()->route('poc-list');
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request.');
            return Redirect::back();
        }
    }
}
