<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\RoleUser;
use App\Models\Role_User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Mail;
use DB;

class RegisterController extends Controller
{

    use RegistersUsers;

    protected $redirectTo = '/home';

    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function validator(array $data)
    {
        if ($data['company'] == 0) {
            return Validator::make($data, [
                'name' => 'required|string|max:100',
                'name2' => 'required|string|max:100',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6|confirmed',
                'company' => 'required | integer | between:0,1',
            ]);
        }else{
            return Validator::make($data, [
                'name' => 'required|string|max:100',
                'name2' => 'required|string|max:100',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6|confirmed',
                'company' => 'required | integer | between:0,1',
                'url'   => 'required | regex: "/^((?:www\.)(?:[-a-z0-9]+\.)*[-a-z0-9]+.*)$/"',
            ]);
        }

    }


    protected function create(array $data)
    {
        if($data['company'] == 0){

            $user = User::create([
                'name' => $data['name'],
                'name2' => $data['name2'],
                'company' => 0,
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
            ]);

            $role = Role_User::create([
                'role_id' => 2,
                'user_id' => $user->id,
            ]);

            return $user;

        }else{

            $user = User::create([
                'name' => $data['name'],
                'name2' => $data['name2'],
                'url' => $data['url'],
                'company' => 1,
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
            ]);

            $role = Role_User::create([
                'role_id' => 2,
                'user_id' => $user->id,
            ]);

            return $user;
        }

    }

    public function register(Request $request)
    {
      $input = $request->all();
      $validator = $this->validator($input);

      if ($validator->passes()) {
        $data = $this->create($input)->toArray();
        $data['token'] = str_random(25);

        DB::table('user_verifications')->insert(['user_id'=>$data['id'],'token'=>$data['token']]);

        Mail::send('email.confirmation', $data, function($message) use($data){
          $message->to($data['email']);
          $message->subject('Registration Confirmation');
        });
        return redirect(route('login'))->with('message', 'Confirmation email has been send. Please check your email.');
      }
      return redirect(route('register'))->with('message-error', $validator->errors());
    }

    public function registerVeteran(Request $request)
    {
      $input = $request->all();
      $validator = $this->validator($input);

      if ($validator->passes()) {
        $data = $this->create($input)->toArray();
        $data['token'] = str_random(25);

        $user = User::find($data['id']);
        $user->veteran = 1;
        $user->save();

        DB::table('user_verifications')->insert(['user_id'=>$data['id'],'token'=>$data['token']]);

        Mail::send('email.confirmation', $data, function($message) use($data){
          $message->to($data['email']);
          $message->subject('Registration Confirmation');
        });
        return redirect(route('login'))->with('message', 'Confirmation email has been send. Please check your email.');
      }
      return redirect(route('register'))->with('message-error', $validator->errors());
    }

    public function confirmation($verification_code)
    {
      $check = DB::table('user_verifications')->where('token',$verification_code)->first();
      if (!is_null($check)) {
        $user = User::find($check->user_id);
        if($user->is_verified == 1){
            return redirect(route('login'))->with('message', 'This account has already been verified.');
        }
        $user->update(['is_verified' => 1]);
        DB::table('user_verifications')->where('token',$verification_code)->delete();
        return redirect(route('login'))->with('message', 'Your activation is completed.');
      }
      return redirect(route('login'))->with('message-error', 'Verification code is invalid.');
    }
}
