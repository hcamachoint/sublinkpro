<?php
namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;
use App\Models\Work;
use App\Models\User;
use Session;
use Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request, $id)
    {
        if ($request->input('concat') == 1) {
            $this->validate($request, [
                'address' => 'required | email | string | between:3,200',
                'able' => 'required | integer | between:1,2'
            ]);
        }else if($request->input('concat') == 2 || $request->input('concat') == 3 || $request->input('concat') == 4){
            $this->validate($request, [
                'address' => 'required | digits_between:10,200',
                'able' => 'required | integer | between:1,2'
            ]);
        }

        if (Input::get('able') == 1) {
            $able = 'App\Models\User';
            $rt = 'profile';
            $ac = User::find($id);
            if (auth()->user()->id != $id) {
                return abort(403, 'Unauthorized action.');
            }
        }else{
            $able = 'App\Models\Work';
            $rt = 'work-show';
            $ac = Work::find($id);
            if (auth()->user()->id != $ac->user->id) {
                return abort(403, 'Unauthorized action.');
            }
        }

        $chk = false;
        foreach ($ac->contact()->get() as $ct)
        {
            if ($ct->category_id == $request->input('concat') && $ct->address == $request->input('address'))
            {
                $chk = true;
            }
        }

        if ($chk != true) {
            try {
                $con = new Contact;
                $con->address = $request->input('address');
                $con->category_id = $request->input('concat');
                $con->contactable_id = $id;
                $con->contactable_type = $able;
                $con->save();

                return Redirect()->route($rt, ['id' => $id])->with('message', 'Contact method assigned correctly!');
            } catch (Exception $e) {
                Session::flash('message-error', 'An error occurred while trying to process your request.');
                return Redirect::back();
            }
        }else{
            Session::flash('message-error', 'That assigned contact already exists.');
            return Redirect::back();
        }
    }

    public function update(Request $request, $id)
    {
        if ($request->input('concat') == 1) {
            $this->validate($request, [
                'address' => 'required | email | string | between:3,200'
            ]);
        }else if($request->input('concat') == 2 || $request->input('concat') == 3 || $request->input('concat') == 4){
            $this->validate($request, [
                'address' => 'required | digits_between:10,200'
            ]);
        }

        $con = Contact::find($id);

        if (Input::get('able') == 1) {
            $ac = User::find($con->contactable_id);
            if (auth()->user()->id != $con->contactable_id) {
                return abort(403, 'Unauthorized action.');
            }
        }else{
            $ac = Work::find($con->work->id);
            if (auth()->user()->id != $work->user_id) {
                return abort(403, 'Unauthorized action.');
            }
        }

        $chk = false;
        foreach ($ac->contact()->get() as $ct)
        {
            if ($ct->category_id == $con->category_id && $ct->address == $request->input('address'))
            {
                $chk = true;
            }
        }

        if ($chk != true) {
            try {
                $con->address = $request->input('address');
                $con->save();
                Session::flash('message', 'Contact updated properly!');
                return Redirect::back();
            } catch (Exception $e) {
                Session::flash('message-error', 'An error occurred while trying to process your request');
                return Redirect::back();
            }
        }elseif ($con->address == $request->input('address')) {
            return Redirect::back();
        }else{
            Session::flash('message-error', 'That assigned contact already exists.');
            return Redirect::back();
        }
    }

    public function destroy(Request $request, $id)
    {
        $con = Contact::find($id);

        if (Input::get('able') == 1) {
            if (auth()->user()->id != $con->contactable_id) {
                return abort(403, 'Unauthorized action.');
            }
        }else{
            $work = Work::find($con->work->id);
            if (auth()->user()->id != $work->user_id) {
                return abort(403, 'Unauthorized action.');
            }
        }

        try {
            $con = Contact::find($id);
            $con->delete();
            Session::flash('message', 'Contact removed properly!');
            return Redirect::back();
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request.');
            return Redirect::back();
        }
    }
}
