<?php
namespace App\Http\Controllers;

use App\Models\Document;
use App\Models\Work;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Session;
use Redirect;
use App\Http\Controllers\Controller;

class DocumentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'description' => 'string | between:1,50',
            'document' => 'required | mimes:jpg,jpeg,png,doc,docx,pdf,odt | max:4096',
            'category' => 'required | integer',
            'able' => 'required | integer | between:1,2'
        ]);

        if (Input::get('able') == 1) {
            $able = 'App\Models\User';
            $rt = 'profile';
            $ac = User::find($id);
            if (auth()->user()->id != $id) {
                return abort(403, 'Unauthorized action.');
            }
        }else{
            $able = 'App\Models\Work';
            $ac = Work::find($id);
            $rt = 'work-show';
            if (auth()->user()->id != $ac->user->id) {
                return abort(403, 'Unauthorized action.');
            }
        }


        $file = $request->file('document');
        if (!empty($file)){
          $file = $request->file('document');
          $fileName = $file->getClientOriginalName();
          $filePath = url('uploads/documents/'.$fileName);
          $destinationPath = 'uploads/documents';

          if ($file->extension() == 'jpg' || $file->extension() == 'jpeg' || $file->extension() == 'png') {
            $fileType = 1;
          } elseif ($file->extension() == 'pdf') {
            $fileType = 2;
          } elseif ($file->extension() == 'doc' || $file->extension() == 'docx') {
            $fileType = 3;
          }

          try {
            if($file->move($destinationPath,$fileName)) {
              $doc = new Document;
              $doc->description = $request->input('description');
              $doc->category_id = $request->input('category');
              $doc->file_path = $filePath;
              $doc->file_name = $fileName;
              $doc->type = $fileType;
              $doc->documentable_id = $id;
              $doc->documentable_type = $able;
              $doc->save();
              return Redirect()->route($rt, ['id' => $id])->with('message', 'Document assigned correctly!');
            }
          } catch (Exception $e) {
              Session::flash('message-error', 'An error occurred while trying to process your request.');
              return Redirect::back();
          }
        }else{
          Session::flash('message-error', 'That file document is empty.');
          return Redirect::back();
        }
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'description' => 'required | string | between:1,50',
        ]);

        $doc = Document::find($id);

        if (Input::get('able') == 1) {
            $ac = User::find($doc->documentable_id);
            $rt = 'profile';
            if (auth()->user()->id != $doc->documentable_id) {
                return abort(403, 'Unauthorized action.');
            }
        }else{
            $ac = Work::find($doc->work->id);
            $rt = 'work-show';
            if (auth()->user()->id != $work->user_id) {
                return abort(403, 'Unauthorized action.');
            }
        }

        try {
            $doc = Document::find($id);
            $doc->description = $request->input('description');

            /*if ($request->input('document')) {
              $this->validate($request, [
                  'document' => 'required | mimes:jpg,jpeg,png,doc,docx,pdf,odt | max:4096',
              ]);
              $file = $request->file('document');
              $fileName = $file->getClientOriginalName();
              $filePath = url('uploads/documents/'.$fileName);
              $destinationPath = 'uploads/documents';

              if ($file->extension() == 'jpg' || $file->extension() == 'jpeg' || $file->extension() == 'png') {
                $fileType = 1;
              } elseif ($file->extension() == 'pdf') {
                $fileType = 2;
              } elseif ($file->extension() == 'doc' || $file->extension() == 'docx') {
                $fileType = 3;
              }

              $doc->file_path = $filePath;
              $doc->file_name = $fileName;
              $doc->type = $fileType;

            }*/
            $doc->save();
            return Redirect()->route($rt, ['id' => $id])->with('message', 'Document updated correctly!');
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request');
            return Redirect::back();
        }
    }

    public function destroy(Request $request, $id)
    {
        $doc = Document::find($id);

        if (Input::get('able') == 1) {
            if (auth()->user()->id != $doc->documentable_id) {
                return abort(403, 'Unauthorized action.');
            }
        }else{
            $work = Work::find($doc->work->id);
            if (auth()->user()->id != $work->user_id) {
                return abort(403, 'Unauthorized action.');
            }
        }

        try {
            $doc = Document::find($id);
            $doc->delete();
            Session::flash('message', 'Document removed properly!');
            return Redirect::back();
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request.');
            return Redirect::back();
        }
    }
}
