<?php

namespace App\Http\Controllers;

use App\Models\Location;
use Session;
use Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class LocationController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request, $id) //CAMBIAR A STORE
    {
        $this->validate($request, [
            'city' => 'required | string | between:1,20',
            'zip' => 'required | string | between:1,10',
        ]);

        if (Input::get('able') == 1) {
            $able = 'App\Models\User';
            $rt = 'profile';
            if (auth()->user()->id != $id) {
                return abort(403, 'Unauthorized action.');
            }
        }else{
            $able = 'App\Models\Work';
            $rt = 'work-show';
            $work = Work::find($id);
            if (auth()->user()->id != $work->user->id) {
                return abort(403, 'Unauthorized action.');
            }
        }

        try {
            $loc = new Location;
            $loc->city_id = $request->input('city');
            $loc->address = $request->input('address');
            $loc->zip = $request->input('zip');
            $loc->locationable_id = $id;
            $loc->locationable_type = $able;
            $loc->save();

            return Redirect()->route($rt, ['id' => $id])->with('message', 'Registered Location Correctly!');
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request.');
            return Redirect::back();
        }
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'city' => 'required | string | between:1,20',
            'zip' => 'required | string | between:1,10',
        ]);

        $loc = Location::find($id);

        if (Input::get('able') == 1) {
            $rt = 'profile';
            if (auth()->user()->id != $loc->locationable_id) {
                return abort(403, 'Unauthorized action.');
            }
        }else{
            $work = Work::find($loc->work->id);
            $rt = 'work-show';
            if (auth()->user()->id != $work->user_id) {
                return abort(403, 'Unauthorized action.');
            }
        }

        try {
            $loc = Location::find($id);
            $loc->city_id = $request->input('city');
            $loc->address = $request->input('address');
            $loc->zip = $request->input('zip');
            $loc->save();
            return Redirect()->route($rt, ['id' => $id])->with('message', 'Location updated properly!');
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request');
            return Redirect::back();
        }
    }

    public function userLocation()
    {
        $timezones = Timezone::pluck('title', 'id');
        $countries = Country::pluck('title', 'id');
        Mapper::map(40, -100, ['zoom' => 4, 'marker' => false, 'eventBeforeLoad' => 'addMarkerListener(map);']);
        return view('locations.map');
        //return view('users.sections.location', ['user' => auth()->user(), 'j' => 1, 'country' => $countries, 'timezone' => $timezones]);
    }
}
