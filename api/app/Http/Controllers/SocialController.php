<?php

namespace App\Http\Controllers;

use App\Models\Social;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Session;
use Redirect;
use App\Http\Controllers\Controller;


class SocialController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'username' => 'string | between:1,50',
            'category' => 'required | integer',
        ]);


		try {
			$soc = new Social;
			$soc->username = $request->input('username');
			$soc->category_id = $request->input('category');
			$soc->user_id = auth()->user()->id;
			$soc->save();
			return Redirect()->route('profile')->with('message', 'Social assigned correctly!');
			
		} catch (Exception $e) {
		  Session::flash('message-error', 'An error occurred while trying to process your request.');
		  return Redirect::back();
		}

    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'username' => 'string | between:1,50',
        ]);

        $soc = Social::find($id);


        try {
            $soc = Social::find($id);
            $soc->username = $request->input('username');
            $soc->save();
            return Redirect()->route('profile')->with('message', 'Social updated correctly!');
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request');
            return Redirect::back();
        }
    }

    public function destroy(Request $request, $id)
    {
        $soc = Social::find($id);

        try {
            $soc = Social::find($id);
            $soc->delete();
            Session::flash('message', 'Social removed properly!');
            return Redirect::back();
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request.');
            return Redirect::back();
        }
    }
}
