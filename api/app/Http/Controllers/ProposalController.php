<?php

namespace App\Http\Controllers;

use App\Models\Job;
use App\Models\Proposal;
use Illuminate\Http\Request;
use Session;
use Redirect;
use DB;

class ProposalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function create($id)
    {
        try {
            $work = Job::find($id);
            $prop = $work->proposal->where('user_id', auth()->user()->id)->first();
            if (auth()->user()->id !== $work->user_id && empty($prop)) {
                return view('proposals.add', ['work' => $work]);
            }else{
                return abort(403, 'Unauthorized action.');
            }
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request');
            return Redirect::back();
        }


    }

    public function show(Request $request, $id)
    {
        $prop = Proposal::find($id);

        $laborero = false;
        $points = 0;
        
        if(isset($prop->contract->feedback)){
            foreach ($prop->contract->feedback as $feedback) {
                if ($feedback->user->id !== $prop->contract->user_id) {
                    $laborero = true;
                    $points = $feedback->points;
                }
            }
        }

        return view('proposals.show', ['prop' => $prop, 'laborero' => $laborero, 'points' => $points]);

    }
    
    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'amount' => 'required | numeric | between:0,999999.99',
            'esttime' => 'required | integer | between:1,360',
            'message' => 'required | string',
        ]);
        try {
            $work = Job::find($id);
            $prop = $work->proposal->where('user_id', auth()->user()->id)->first();
            if(empty($prop)){
                $proposal = new Proposal;
                if (count($work->proposal) < $work->proposals || $work->status != 0) {
                    if (auth()->user()->id !== $work->user_id) {
                        $proposal->fill($request->all());
                        $proposal->user_id = auth()->user()->id;
                        $proposal->job_id = $work->id;
                        $proposal->save();
                        Session::flash('message', 'Your Job has been successfully created!');
                        return Redirect()->route('work-show', ['id' => $work->id]);
                    }else{
                        return abort(403, 'Unauthorized action.');
                    }
                }else{
                    Session::flash('message-error', 'Sorry, the maximum number of proposals for this work has been reached!');
                    return Redirect()->route('work-show', ['id' => $work->id]);
                }
            }else{
                Session::flash('message-error', 'Sorry, you already have an active proposal in this work.');
                return Redirect()->route('work-show', ['id' => $work->id]);
            }
            
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request');
            return Redirect::back();
        }
    }
    
    public function edit($id)
    {
        try {
            $work = Job::find($id);
            $prop = $work->proposal->where('user_id', auth()->user()->id)->first();
            if (auth()->user()->id !== $work->user_id) {
                return view('proposals.edit', ['work' => $work, 'prop' =>$prop]);
            }else{
                return abort(403, 'Unauthorized action.');
            }
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request');
            return Redirect::back();
        }


    }
    
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'amount' => 'required | numeric | between:0,999999.99',
            'esttime' => 'required | integer | between:1,360',
        ]);
        try {
            $work = Job::find($id);
            $proposal = $work->proposal->where('user_id', auth()->user()->id)->first();
            $proposal->fill($request->all());
            $proposal->save();
            Session::flash('message', 'Your Job has been successfully updated!');
            return Redirect()->route('work-show', ['id' => $work->id]);       
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request');
            return Redirect::back();
        }


    }
}
