<?php
namespace App\Http\Controllers;
use App\Models\ContactCategory as ContactCategory;
use App\Models\AbilityCategory as AbilityCategory;
use App\Models\DocumentCategory as DocumentCategory;
use App\Models\SocialCategory as SocialCategory;
use App\Models\WorkCategory as WorkCategory;
use App\Models\Country as Country;
use App\Models\City as City;
use App\Models\User as User;
use Session;
use Redirect;
use Image;
use DB;
use Mapper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = auth()->user();
        return view('users.view', ['user' => $user]);
    }


    public function edit($id)
    {
        try {
            $user = auth()->user();
            if ($user->id == $id) {
                $concat = ContactCategory::pluck('description','id');
                $abicat = AbilityCategory::pluck('description','id');
                $doccat = DocumentCategory::pluck('description','id');
                $jobcat = WorkCategory::pluck('description','id');
                $timezones = Timezone::pluck('title', 'id');
                $countries = Country::pluck('title', 'id');
                return view('users.edit', ['user' => $user, 'concat' => $concat, 'abicat' => $abicat, 'doccat' => $doccat, 'jobcat' => $jobcat, 'j' => 1, 'country' => $countries, 'timezone' => $timezones]);
            }else{
                return abort(403, 'Unauthorized action.');
            }
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request');
            return Redirect::back();
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'idn' => 'required | regex:"/^[A-Z]{1,2}(?)[0-9]{12}$/" | unique:users,idn,'.$request->user()->id,
            'name' => 'required | string',
            'email' => 'required | unique:users,email,'.$request->user()->id,
            'sex' => 'required | string'
        ]);

        try {
            $user = $request->user();
            $user->fill($request->all());
            $user->save();
            return Redirect()->route('profile')->with('message', 'Your profile has been successfully updated!');
        } catch (Exception $e) {
          Session::flash('message-error', 'An error occurred while trying to process your request');
          return Redirect::back();
        }
    }

    public function destroy(Request $request)
    {
        try {
            $user = $request->user();
            $user->delete();
            Session::flash('message', 'User removed properly!');
            return Redirect::to('login');
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request.');
            return Redirect::back();
        }
    }

    public function update_picture(Request $request)
    {
        $this->validate($request, [
            'avatar' => 'required | dimensions:min_width=100,min_height=100 | image',
        ]);

        try {
            $user = $request->user();
            if($request->hasFile('avatar')){
                $avatar = $request->file('avatar');
                echo hash('ripemd160', 'user'.time());
                $filename = hash('ripemd160', $request->user()). '_' . time() . '.' . $avatar->getClientOriginalExtension();
                Image::make($avatar)->resize(300, 300)->save( public_path('avatars/' . $filename ) );

                try {
                    $user = $request->user();
                    $user->image_path = $filename;
                    $user->save();
                    return Redirect()->route('profile')->with('message', 'Photo successfully updated!');
                } catch (Exception $e) {
                    Session::flash('message-error', 'An error occurred while trying to process your request');
                    return Redirect::back();
                }
            }
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request');
            return Redirect::back();
        }
    }

    public function list(Request $request)
    {
        if ($request->ajax()) {  //CHEQUEA QUE LA SOLICITUD SEA API
            $authUserId = $request->user()->id;
            $users = DB::table('users')
                    ->where('users.id','!=',$authUserId)
                    ->get();
            return $users;
        }
    }

    public function userPicture()
    {
        return view('users.sections.picture', ['user' => auth()->user()]);
    }

    public function userInformation()
    {
        return view('users.sections.information', ['user' => auth()->user()]);
    }

    public function userContact()
    {
        $concat = ContactCategory::pluck('description','id');
        return view('users.sections.contact', ['user' => auth()->user(), 'concat' => $concat, 'j' => 1,]);
    }

    public function userAbilities()
    {
        $abicat = AbilityCategory::pluck('description','id');
        return view('users.sections.abilities', ['user' => auth()->user(), 'abicat' => $abicat, 'j' => 1]);
    }

    public function userLocation()
    {
        $cities = City::pluck('title', 'id');
        return view('users.sections.location', ['user' => auth()->user(), 'j' => 1, 'cities' => $cities,]);
    }

    public function userDocuments()
    {
        $doccat = DocumentCategory::pluck('description','id');
        return view('users.sections.document', ['user' => auth()->user(), 'j' => 1, 'doccat' => $doccat]);
    }

    public function userSocials()
    {
        $soccat = SocialCategory::pluck('description','id');
        return view('users.sections.social', ['user' => auth()->user(), 'j' => 1, 'soccat' => $soccat]);
    }
}
