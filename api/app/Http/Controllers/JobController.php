<?php

namespace App\Http\Controllers;

use App\Notifications\PostProposal;
use App\Models\Work;
use App\Models\User;
use App\Models\Message;
use App\Models\Proposal;
use Illuminate\Http\Request;
use Session;
use Redirect;
use DB;
use Auth;

class JobController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');
    }
    
    public function index()
    {
        //$user = auth()->user();
        $jobs = Work::where('status', 1)->orWhere('status', 2)->orderByDesc('created_at')->paginate(20);
        return view('jobs.view', ['jobs' => $jobs]);
    }
    
    public function show($id)
    {
        $job = Work::find($id);
        $prop = $job->proposal->where('user_id', auth()->user()->id)->first();
        return view('jobs.show', ['job' => $job, 'prop' => $prop]);
    }

    public function proposal(Request $request, $id)
    {
        $this->validate($request, [
            'message' => 'required | string',
        ]);

        $job = Work::find($id);

        /*$mensaje = new Message;
        $mensaje->sender_id = Auth::user()->id;
        $mensaje->receiver_id = $job->user_id;
        $mensaje->message = $request->message;
        $mensaje->subject = $job->title;
        $mensaje->read = 0;
        $mensaje->save();*/

        try {
            $work = Work::find($id);
            $prop = $work->proposal->where('user_id', auth()->user()->id)->first();
            
            if(empty($prop)){
                $proposal = new Proposal;
                if (auth()->user()->id !== $work->user_id) {
                    $proposal->fill($request->all());
                    $proposal->user_id = auth()->user()->id;
                    $proposal->work_id = $work->id;
                    $proposal->save();
                    $work->status = 2;
                    $work->save();

                    $user = User::find($work->user_id);
                    $user->notify(new PostProposal($proposal));
                    
                    Session::flash('message', 'Your Proposal has been sended successfully!');
                    return Redirect()->route('job-show', ['id' => $work->id]);
                }else{
                    return abort(403, 'Unauthorized action.');
                }
       
            }else{
                Session::flash('message-error', 'Sorry, you already have an active proposal in this work.');
                return Redirect()->route('job-show', ['id' => $work->id]);
            }
            
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request');
            return Redirect::back();
        }
    }

    public function search(Request $request)
    {
        $jobs = Work::where('title', 'LIKE', '%'.$request->input('search').'%')->orWhere('description', 'LIKE', '%'.$request->input('search').'%')->where('status', 1)->orderByDesc('created_at')->paginate(20);
        return view('jobs.view', ['jobs' => $jobs]);
    }
}