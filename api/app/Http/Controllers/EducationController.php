<?php

namespace App\Http\Controllers;

use App\Models\Education;
use Illuminate\Http\Request;
use Session;
use Redirect;
use App\Http\Controllers\Controller;

class EducationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'school' => 'required | string | between:3,100',
            'degree' => 'required | string | between:3,100',
            'perioda' => 'required | date',
            'periodb' => 'required | date',
            'area' => 'required | string | between:3,100',
            'description' => 'string',
        ]);

        if (auth()->user()->id != $id) {
            return abort(403, 'Unauthorized action.');
        }
        
        try {
            $edu = new Education;
            $edu->fill($request->all());
            $edu->user_id = $id;
            $edu->save();
            Session::flash('message', 'Education assigned correctly!');
            return Redirect::back();
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request.');
            return Redirect::back();
        }
        return abort(403, 'Unauthorized action.');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'school' => 'required | string | between:3,100',
            'degree' => 'required | string | between:3,100',
            'perioda' => 'required | date',
            'periodb' => 'required | date',
            'area' => 'required | string | between:3,100',
            'description' => 'string',
        ]);

        $edu = Education::find($id);
        if (auth()->user()->id != $edu->user_id) {
            return abort(403, 'Unauthorized action.');
        }

        try {
            $edu = Education::find($id);
            if (auth()->user()->id === $edu->user_id) {
                $edu->fill($request->all());
                $edu->save();
                Session::flash('message', 'Education updated properly!');  
                return Redirect::back();
            }else{
                return abort(403, 'Unauthorized action.');
            }
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request');
            return Redirect::back();
        }
    }

    public function destroy($id)
    {
        $edu = Education::find($id);
        if (auth()->user()->id != $edu->user_id) {
            return abort(403, 'Unauthorized action.');
        }

        try {
            $edu = Education::find($id);
            if (auth()->user()->id === $edu->user_id) {
                $edu->delete();
                Session::flash('message', 'Education removed properly!');
                return Redirect::back();
            }else{
                return abort(403, 'Unauthorized action.');
            }
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request.');
            return Redirect::back();
        }  
    }
}
