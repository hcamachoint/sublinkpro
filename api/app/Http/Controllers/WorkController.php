<?php

namespace App\Http\Controllers;

use App\Events\ChatMessage;
use Illuminate\Support\Facades\Log;

use App\Models\Ability;
use App\Models\Work;
use App\Models\Contact;
use App\Models\WorkCategory;
use App\Models\ProjectCategory;
use App\Models\LevelCategory;
use App\Models\AbilityCategory;
use App\Models\ContactCategory;
use App\Models\Proposal;
use Illuminate\Http\Request;
use Session;
use Redirect;
use Auth;

class WorkController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$user = auth()->user();
        $works = $user->work->sortByDesc('created_at');

        $var = count(auth()->user()->work->where('status', 1));
        $mensaje = false;
        if ($var >= 3) {
            $mensaje = true;
        }

    	return view('works.view', ['works' => $works, 'mensaje' => $mensaje]);
    }

    public function show($id)
    {
        $work = Work::find($id);
        return view('works.show', ['work' => $work]);
    }

    public function create()
    {
        $var = count(auth()->user()->work->where('status', 1));
        if ($var < 3) {
            $workcat = WorkCategory::all();
            $procat = ProjectCategory::all();
            $levcat = LevelCategory::all();
            return view('works.create', ['workcat' => $workcat, 'procat' => $procat, 'levcat' => $levcat]);
        }else{
            Session::flash('message-error', 'I\'m sorry, you\'ve exceeded the number of active works. Please finish other jobs before creating new jobs.');
            return Redirect()->route('works');
        }
    }

    public function create2(Request $request)
    {
        $this->validate($request, [
            'category' => 'integer | required | between:1,4',
            'level' => 'required | integer | between:1,2',
            'project' => 'required | integer | between:1,2',
        ]);

        $input = $request->all();
        Session::put('wcategory', $input['category']);
        Session::put('wproject', $input['project']);
        Session::put('wlevel', $input['level']);

        $abicat = AbilityCategory::all();
        return view('works.create2', ['abicat' => $abicat]);
    }

    public function create3(Request $request)
    {
        $this->validate($request, [
            'title' => 'required | string | max:100',
            'description' => 'required | string',
            'abilities' => 'required',
        ]);
        
        $input = $request->all();

        $abilities = isset($input['abilities']);
        if ($abilities == true) {
            Session::put('wabilities', $input['abilities']);
        }else{
            Session::put('wabilities', null);
        }

        Session::put('wtitle', $input['title']);
        Session::put('wdescription', $input['description']);
        //Session::put('waddress', $input['address']);
        return view('works.create3');
    }

    public function create4(Request $request)
    {
        $this->validate($request, [
            'email' => 'string | max:100',
        ]);

        $input = $request->all();

        $email = isset($input['email']);
        if ($email == true) {
            Session::put('wemail', Auth::user()->email);
        }else{
            Session::put('wemail', null);
        }

        $emaila = isset($input['emaila']);
        if ($emaila == true) {
            Session::put('wemaila', $input['emaila']);
        }else{
            Session::put('wemaila', null);
        }

        $phone = isset($input['phone']);
        if ($phone == true) {
            Session::put('wphone', $input['phone']);
        }else{
            Session::put('wphone', null);
        }
        /*var_dump($email);
        var_dump($emaila);
        var_dump($phone);
        var_dump(Session::get('wemail'));
        var_dump(Session::get('wemaila'));
        var_dump(Session::get('wphone'));*/
        return view('works.create4');
    }

    public function store(Request $request)
    {
        $input = $request->all();

        if ($input['pstructure'] == 1) {
            $this->validate($request, [
                'pstructure' => 'integer | required | between:1,2',
                'budget' => 'required | numeric | between:0,999999.99',
                'startd' => 'required | date',
                'texpect' => 'required | integer | between:1,360',
                'requireds' => 'required | integer | between:1,30',
            ]);
        }else{
            $this->validate($request, [
                'pstructure' => 'integer | required | between:1,2',
                'budget' => 'required | numeric | between:8,50.00',
                'startd' => 'required | date',
                'texpect' => 'required | integer | between:1,360',
                'requireds' => 'required | integer | between:1,30',
            ]);
        }
        Session::put('wpstructure', $input['pstructure']);
        Session::put('wbudget', $input['budget']);
        Session::put('wstartd', $input['startd']);
        Session::put('wtexpect', $input['texpect']);
        Session::put('wrequireds', $input['requireds']);

        //STORAGE ADDRESS
        try {
            $var = count(auth()->user()->work->where('status', 1));
            if ($var < 3) {
                $work = new Work;
                $work->title = Session::get('wtitle');
                $work->description = Session::get('wdescription');
                $work->pstructure = Session::get('wpstructure');
                $work->budget = Session::get('wbudget');
                $work->startd = Session::get('wstartd');
                $work->texpect = Session::get('wtexpect');
                $work->requireds = Session::get('wrequireds');
                $work->category_id = Session::get('wcategory');
                $work->project_id = Session::get('wproject');
                $work->level_id = Session::get('wlevel');
                $work->status = 1;
                $work->user_id = auth()->user()->id;
                $work->save();

                //ABILITIES
                if (Session::get('wabilities') != null) {
                    foreach (Session::get('wabilities') as $ab) {
                        $abi = new Ability;
                        $abi->category_id = $ab;
                        $abi->abilityable_id = $work->id;
                        $abi->abilityable_type = 'app\\Models\\Work';
                        $abi->save();
                    }
                }
                
                //CONTACT INFO
                if (Session::get('wemail') != null) {
                    $con = new Contact;
                    $con->category_id = 1;
                    $con->address = Session::get('wemail');
                    $con->contactable_id = $work->id;
                    $con->contactable_type = 'app\\Models\\Work';
                    $con->save();
                }

                if (Session::get('wemaila') != null) {
                    $con = new Contact;
                    $con->category_id = 1;
                    $con->address = Session::get('wemaila');
                    $con->contactable_id = $work->id;
                    $con->contactable_type = 'app\\Models\\Work';
                    $con->save();
                }

                if (Session::get('wphone') != null) {
                    $con = new Contact;
                    $con->category_id = 2;
                    $con->address = Session::get('wphone');
                    $con->contactable_id = $work->id;
                    $con->contactable_type = 'app\\Models\\Work';
                    $con->save();
                }
                


                Session::flash('message', 'Work assigned correctly!');
                return Redirect()->route('works');
            }else{
                Session::flash('message-error', 'I\'m sorry, you\'ve exceeded the number of active works. Please finish other jobs before creating new jobs.');
                return Redirect()->route('works');
            }
            
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request.');
            return Redirect::back();
        }
    }

    public function edit($id)
    {
        $work = Work::find($id);
        if (auth()->user()->id !== $work->user_id) {
            return abort(403, 'Unauthorized action.');
        }

        try {
            if ($work->status === 1) {
                $workcat = WorkCategory::pluck('description', 'id');
                $procat = ProjectCategory::pluck('description', 'id');
                $levcat = LevelCategory::pluck('description', 'id');
                $abicat = AbilityCategory::pluck('description','id');
                return view('works.edit', ['work' => $work, 'workcat' => $workcat, 'levcat' => $levcat, 'procat' => $procat, 'abicat' => $abicat, 'j' => 2]);
            }elseif($work->status === 2){
                Session::flash('message-error', 'I\'m sorry, you can not edit a work that has an active proposal.');
                return Redirect()->route('works');
            }else{
                Session::flash('message-error', 'I\'m sorry, you can not edit a work that has already completed.');
                return Redirect()->route('works');
            }
            
            
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request');
            return Redirect::back();
        }


    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required | string | max:100',
            'description' => 'required | string',
            'category' => 'integer | required | between:1,4',
            'level' => 'required | integer | between:1,2',
            'project' => 'required | integer | between:1,2',
            'pstructure' => 'integer | required | between:1,2',
            'budget' => 'required | numeric | between:0,999999.99',
            'startd' => 'required | date',
            'texpect' => 'required | integer | between:1,360',
            'requireds' => 'required | integer | between:1,30',
        ]);
        try {
            $work = Work::find($id);
            if (auth()->user()->id === $work->user_id) {
                if ($work->status === 1) {
                    if (count($work->proposal) == 0) {               
                        $work->fill($request->all());
                        $work->save();
                        Session::flash('message', 'Your Work has been successfully updated!');
                        return Redirect()->route('work-show', ['id' => $work->id]);
                    }else{
                        Session::flash('message-error', 'Your work can not be updated because you have active proposals!');
                        return Redirect()->route('work-show', ['id' => $work->id]);
                    }
                }elseif($work->status === 2){
                    Session::flash('message-error', 'I\'m sorry, you can not edit a work that has an active proposal.');
                    return Redirect()->route('works');
                }else{
                    Session::flash('message-error', 'I\'m sorry, you can not edit a work that has already completed.');
                    return Redirect()->route('works');
                }
            }else{
                return abort(403, 'Unauthorized action.');
            }
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request');
            return Redirect::back();
        }
    }

    public function destroy($id)
    {
        try {
            $work = Work::find($id);
            if (auth()->user()->id === $work->user_id) {
                $work->delete();
                Session::flash('message', 'Work removed properly!');
                return Redirect()->route('works');
            }else{
                return abort(403, 'Unauthorized action.');
            }
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request.');
            return Redirect::back();
        }
    }

    public function close($id)
    {
        try {
            $work = Work::find($id);
            if (auth()->user()->id === $work->user_id) {
                $work->status = 0;
                $work->save();
                Session::flash('message', 'Your Work has been successfully closed!');
                return Redirect()->route('works');
            }else{
                return abort(403, 'Unauthorized action.');
            }
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request.');
            return Redirect::back();
        }
    }

    public function contact(Request $request, $id, $proposal){
        $user = auth()->user();
        
        $proposal = Proposal::find($proposal);
        $proposal->status = 1;
        $proposal->save();

        $message = $user->messages()->create([
            'message'=>$request->message,
            'type'=> 1,
        ]);

        $message->receivers()->create([
                'user_id'=>$id
            ]);
        // // new message has beed posted
        try {
            broadcast(new ChatMessage($message,$user,$id))->toOthers();
        } catch (Exception $e) {
            echo "string";
        }
        return Redirect::back();
    }

    public function contacte(Request $request, $id)
    {
        $work = Work::find($id);
        $concat = ContactCategory::pluck('description','id');
        return view('works.contact', ['work' => $work, 'concat' => $concat, 'j' => 2, 'k' => $work->id]);
    }
}
