<?php

namespace App\Http\Controllers;
use Session;
use Redirect;
use App\Models\User;
use App\Models\Ability;
use App\Models\Work;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class AbilityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'abicat' => 'required | integer',
        ]);
        //$request->segment(1) === 'profile'
        if (Input::get('able') == 1) {
            $able = 'App\Models\User';
            $rt = 'profile';
            $ac = User::find($id);
            if (auth()->user()->id != $id) {
                return abort(403, 'Unauthorized action.');
            }
        }else{
            $able = 'App\Models\Work';
            $rt = 'work-show';
            $ac = Work::find($id);
            if (auth()->user()->id != $ac->user->id) {
                return abort(403, 'Unauthorized action.');
            }
        }

        $chk = false;
        foreach ($ac->ability()->get() as $ab)
        {
            if ($ab->category_id == $request->input('abicat'))
            {
                $chk = true;
            }
        }

        if ($chk != true) {
            try {
                $abi = new Ability;
                $abi->category_id = $request->input('abicat');
                $abi->abilityable_id = $id;
                $abi->abilityable_type = $able;
                $abi->save();
                return Redirect()->route($rt, ['id' => $id])->with('message', 'Skill assigned correctly!');
            } catch (Exception $e) {
                Session::flash('message-error', 'An error occurred while trying to process your request.');
                return Redirect::back();
            }
        }else{
            Session::flash('message-error', 'That assigned skill already exists.');
            return Redirect::back();
        }



    }

    public function destroy(Request $request, $id)
    {
        $abi = Ability::find($id);
        if (Input::get('able') == 1) {
            if (auth()->user()->id != $abi->abilityable_id) {
                return abort(403, 'Unauthorized action.');
            }
        }else{
            $work = Work::find($abi->work->id);
            if (auth()->user()->id != $work->user_id) {
                return abort(403, 'Unauthorized action.');
            }
        }

        try {
            $abil = Ability::find($id);
            $abil->delete();
            Session::flash('message', 'Skill removed correctly from work!');
            return Redirect::back();
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request.');
            return Redirect::back();
        }
    }
}
