<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;
use App\Models\User;
use App\Models\Ability;
use App\Models\Work;

class SkillController extends Controller
{
  public function view($cod){
    try {
        $skill = Ability::find($cod);
        return response()->json(['success' => true, 'skill' => $skill], 200);
    } catch (JWTException $e) {
        return response()->json(['success' => false, 'error' => 'Skill not found.'], 400);
    }
  }

  public function list(){
    try {
        $skills = Ability::all();
        return response()->json(['success' => true, 'skills' => $skills], 200);
    } catch (JWTException $e) {
        return response()->json(['success' => false, 'error' => 'Failed to logout, please try again.'], 500);
    }
  }

  public function store(Request $request, $id)
  {
      $rules = [
          'category' => 'required | integer',
          'able' => 'required | integer'
      ];
      $input = $request->only(
          'category',
          'able'
      );
      $validator = Validator::make($input, $rules);
      if($validator->fails()) {
          $error = $validator->messages()->toJson();
          return response()->json(['success'=> false, 'error'=> $error], 405);
      }

      if ($request->able == 1) {
          $able = 'App\Models\User';
          $ac = User::find($id);
          if (auth()->user()->id != $id) {
              return response()->json(['success' => false, 'error' => 'Unauthorized action.'], 403);
          }
      }else{
          $able = 'App\Models\Work';
          $ac = Work::find($id);
          if (auth()->user()->id != $ac->user->id) {
              return response()->json(['success' => false, 'error' => 'Unauthorized action.'], 403);
          }
      }

      $chk = false;
      foreach ($ac->ability()->get() as $ab)
      {
          if ($ab->category_id == $request->category)
          {
              $chk = true;
          }
      }

      if ($chk != true) {
          try {
              $abi = new Ability;
              $abi->category_id = $request->category;
              $abi->abilityable_id = $id;
              $abi->abilityable_type = $able;
              $abi->save();
              return response()->json(['success'=> true, 'message'=> 'Skill assigned correctly!'], 200);
          } catch (Exception $e) {
              $error_message = $e->getMessage();
              return response()->json(['success' => false, 'error' => $error_message], 500);
          }
      }else{
          return response()->json(['success' => false, 'error' => 'That assigned Skill already exists.'], 400);
      }
  }

  public function destroy(Request $request, $id, $cod)
  {
      $rules = [
          'able' => 'required | integer'
      ];
      $input = $request->only(
          'able'
      );
      $validator = Validator::make($input, $rules);
      if($validator->fails()) {
          $error = $validator->messages()->toJson();
          return response()->json(['success'=> false, 'error'=> $error], 405);
      }

      try {
          $abi = Ability::find($cod);
      } catch (\Exception $e) {
          $error_message = $e->getMessage();
          return response()->json(['success' => false, 'error' => $error_message], 500);
      }

      if (empty($abi)) {
        return response()->json(['success' => false, 'error' => 'Skill not found.'], 400);
      }else{
        if ($request->able == 1) {
            if ($request->user()->id != $abi->abilityable_id || $request->user()->id != $id) {
                return response()->json(['success' => false, 'error' => 'Unauthorized action.'], 403);
            }
        }else{
            $work = Work::find($id);
            if ($request->user()->id != $work->user_id) {
                return response()->json(['success' => false, 'error' => 'Unauthorized action.'], 403);
            }
        }

        try {
            $abi->delete();
            return response()->json(['success'=> true, 'message'=> 'Skill removed correctly!!'], 200);
        } catch (Exception $e) {
            $error_message = $e->getMessage();
            return response()->json(['success' => false, 'error' => $error_message], 500);
        }
      }
  }
}
