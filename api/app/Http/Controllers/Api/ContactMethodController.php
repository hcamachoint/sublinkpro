<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;
use App\Models\Contact;
use App\Models\User;
use App\Models\Work;

class ContactMethodController extends Controller
{
  public function view($cod){
    try {
        $contact = Contact::find($cod);
        return response()->json(['success' => true, 'document' => $contact], 200);
    } catch (JWTException $e) {
        return response()->json(['success' => false, 'error' => 'Failed to logout, please try again.'], 500);
    }
  }

  public function list(){
    try {
        $contacts = Contact::all();
        return response()->json(['success' => true, 'documents' => $contacts], 200);
    } catch (JWTException $e) {
        return response()->json(['success' => false, 'error' => 'Failed to logout, please try again.'], 500);
    }
  }

  public function store(Request $request, $id)
  {
      $rules = [];
      $rules['able'] = 'required | integer | between:1,2';
      $rules['category'] = 'required | integer';

      if ($request->category == 1) {//EMAIL
          $rules['address'] = 'required | email | string | between:3,200';
      }else{ //FREECAT
          $rules['address'] = 'required | digits_between:10,200';
      }

      $input = $request->only(
          'address',
          'category',
          'able'
      );
      $validator = Validator::make($input, $rules);
      if($validator->fails()) {
          $error = $validator->messages()->toJson();
          return response()->json(['success'=> false, 'error'=> $error], 405);
      }

      if ($request->able == 1) {
          $able = 'App\Models\User';
          $ac = User::find($id);
          if ($request->user()->id != $id) {
              return response()->json(['success' => false, 'error' => 'Unauthorized action.'], 403);
          }
      }else{
          $able = 'App\Models\Work';
          $ac = Work::find($id);
          if ($request->user()->id != $ac->user->id) {
              return response()->json(['success' => false, 'error' => 'Unauthorized action.'], 403);
          }
      }

      $chk = false;
      foreach ($ac->contact()->get() as $ct)
      {
          if ($ct->category_id == $request->category && $ct->address == $request->address)
          {
              $chk = true;
          }
      }

      if ($chk != true) {
          try {
              $con = new Contact;
              $con->address = $request->address;
              $con->category_id = $request->category;
              $con->contactable_id = $id;
              $con->contactable_type = $able;
              $con->save();
              return response()->json(['success'=> true, 'message'=> 'Contact method created correctly!'], 200);
          } catch (Exception $e) {
            $error_message = $e->getMessage();
            return response()->json(['success' => false, 'error' => $error_message], 500);
          }
      }else{
          return response()->json(['success' => false, 'error' => 'That assigned contact method already exists.'], 400);
      }
  }

  public function update(Request $request, $id, $cod)
  {
      $rules = [];
      $rules['able'] = 'required | integer | between:1,2';
      $rules['category'] = 'required | integer';
      if (!empty($request->address)) {
        if ($request->category == 1) {//EMAIL
            $rules['address'] = 'required | email | string | between:3,200';
        }else{ //FREECAT
            $rules['address'] = 'required | digits_between:10,200';
        }
      }

      $input = $request->only(
          'address',
          'category',
          'able'
      );
      $validator = Validator::make($input, $rules);
      if($validator->fails()) {
          $error = $validator->messages()->toJson();
          return response()->json(['success'=> false, 'error'=> $error], 405);
      }

      try {
          $con = Contact::find($cod);
      } catch (JWTException $e) {
          $error = $validator->messages()->toJson();
          return response()->json(['success'=> false, 'error'=> $error], 500);
      }

      if (empty($con)) {
        return response()->json(['success' => false, 'error' => 'Contact not found.'], 400);
      }else{
        if ($request->able == 1) {
            if ($request->user()->id != $con->contactable_id || $request->user()->id != $id) {
                return response()->json(['success' => false, 'error' => 'Unauthorized action.'], 403);
            }
        }else{
            $work = Work::find($id);
            if ($request->user()->id != $work->user_id) {
                return response()->json(['success' => false, 'error' => 'Unauthorized action.'], 403);
            }
        }

        if (!empty($request->address)) {
          $con->address = $request->address;
        }
        //INTENTAR GUARDAR LOS CAMBIOS
        try {
            $con->category_id = $request->category;
            $con->save();
            return response()->json(['success'=> true, 'message'=> 'Contact updated correctly!'], 200);
        } catch (Exception $e) {
            $error_message = $e->getMessage();
            return response()->json(['success' => false, 'error' => $error_message], 500);
        }
      }

  }

  public function destroy(Request $request, $id, $cod)
  {
      $rules = [
          'able' => 'required | integer'
      ];
      $input = $request->only(
          'able'
      );
      $validator = Validator::make($input, $rules);
      if($validator->fails()) {
          $error = $validator->messages()->toJson();
          return response()->json(['success'=> false, 'error'=> $error], 405);
      }

      try {
          $con = Contact::find($cod);
      } catch (JWTException $e) {
          $error = $validator->messages()->toJson();
          return response()->json(['success'=> false, 'error'=> $error], 500);
      }

      if (empty($con)) {
          return response()->json(['success' => false, 'error' => 'Contact not found.'], 400);
      }else{
          if ($request->able == 1) {
              if ($request->user()->id != $con->contactable_id || $request->user()->id != $id) {
                  return response()->json(['success' => false, 'error' => 'Unauthorized action.'], 403);
              }
          }else{
              $work = Work::find($id);
              if ($request->user()->id != $work->user_id) {
                  return response()->json(['success' => false, 'error' => 'Unauthorized action.'], 403);
              }
          }

          try {
              $con->delete();
              return response()->json(['success'=> true, 'message'=> 'Contact removed correctly from work!!'], 200);
          } catch (Exception $e) {
              $error_message = $e->getMessage();
              return response()->json(['success' => false, 'error' => $error_message], 500);
          }
      }
  }
}
