<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use App\Models\AbilityCategory;
use App\Models\DocumentCategory;
use App\Models\SocialCategory;
use App\Models\WorkCategory;
use App\Models\ProjectCategory;
use App\Models\LevelCategory;
use App\Models\City;

class DataListController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    public function skill(){
      try {
          $skills = AbilityCategory::pluck('description','id');
          return response()->json(['success' => true, 'skills' => $skills], 200);
      } catch (JWTException $e) {
          return response()->json(['success' => false, 'error' => 'Failed to logout, please try again.'], 500);
      }
    }

    public function project(){
      try {
          $projects = ProjectCategory::pluck('description','id');
          return response()->json(['success' => true, 'projects' => $projects], 200);
      } catch (JWTException $e) {
          return response()->json(['success' => false, 'error' => 'Failed to logout, please try again.'], 500);
      }
    }

    public function document(){
      try {
          $documents = DocumentCategory::pluck('description','id');
          return response()->json(['success' => true, 'documents' => $documents], 200);
      } catch (JWTException $e) {
          return response()->json(['success' => false, 'error' => 'Failed to logout, please try again.'], 500);
      }
    }

    public function social(){
      try {
          $socials = SocialCategory::pluck('description','id');
          return response()->json(['success' => true, 'socials' => $socials], 200);
      } catch (JWTException $e) {
          return response()->json(['success' => false, 'error' => 'Failed to logout, please try again.'], 500);
      }
    }

    public function workcat(){
      try {
          $worcats = WorkCategory::pluck('description','id');
          return response()->json(['success' => true, 'workcats' => $worcats], 200);
      } catch (JWTException $e) {
          return response()->json(['success' => false, 'error' => 'Failed to logout, please try again.'], 500);
      }
    }

    public function worklev(){
      try {
          $worklevs = LevelCategory::pluck('description','id');
          return response()->json(['success' => true, 'worklevs' => $worklevs], 200);
      } catch (JWTException $e) {
          return response()->json(['success' => false, 'error' => 'Failed to logout, please try again.'], 500);
      }
    }

    public function city(){
      try {
          $cities = City::pluck('title','id');
          return response()->json(['success' => true, 'cities' => $cities], 200);
      } catch (JWTException $e) {
          return response()->json(['success' => false, 'error' => 'Failed to logout, please try again.'], 500);
      }
    }
}
