<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;
use App\Models\Social;
use App\Models\User;
use App\Models\Work;

class SocialProfileController extends Controller
{
  public function view($cod){
    try {
        $social = Social::find($cod);
        return response()->json(['success' => true, 'document' => $social], 200);
    } catch (JWTException $e) {
        return response()->json(['success' => false, 'error' => 'Failed to logout, please try again.'], 500);
    }
  }

  public function list(){
    try {
        $socials = Social::all();
        return response()->json(['success' => true, 'documents' => $socials], 200);
    } catch (JWTException $e) {
        return response()->json(['success' => false, 'error' => 'Failed to logout, please try again.'], 500);
    }
  }

  public function store(Request $request, $id)
  {
      $rules = [
          'username' => 'string | between:1,50',
          'category' => 'required | integer'
      ];
      $input = $request->only(
          'username',
          'category'
      );
      $validator = Validator::make($input, $rules);
      if($validator->fails()) {
          $error = $validator->messages()->toJson();
          return response()->json(['success'=> false, 'error'=> $error], 405);
      }

      try {
  			$soc = new Social;
  			$soc->username = $request->username;
  			$soc->category_id = $request->category;
  			$soc->user_id = $request->user()->id;
  			$soc->save();
  			return response()->json(['success'=> true, 'message'=> 'Social profile assigned correctly!'], 201);
  		} catch (Exception $e) {
        $error_message = $e->getMessage();
        return response()->json(['success' => false, 'error' => $error_message], 500);
  		}
  }

  public function update(Request $request, $id)
  {
      $rules = [
          'username' => 'string | between:1,50',
          'category' => 'required | integer'
      ];
      $input = $request->only(
          'username',
          'category'
      );
      $validator = Validator::make($input, $rules);
      if($validator->fails()) {
          $error = $validator->messages()->toJson();
          return response()->json(['success'=> false, 'error'=> $error], 405);
      }

      try {
          $soc = Social::find($id);
      } catch (JWTException $e) {
          $error = $validator->messages()->toJson();
          return response()->json(['success'=> false, 'error'=> $error], 500);
      }

      if (empty($soc)) {
        return response()->json(['success' => false, 'error' => 'Social profile not found.'], 400);
      }else{
        try {
            $soc->username = $request->username;
            $soc->category_id = $request->category;
            $soc->save();
            return response()->json(['success'=> true, 'message'=> 'Social profile updated correctly!'], 200);
        } catch (Exception $e) {
            $error_message = $e->getMessage();
            return response()->json(['success' => false, 'error' => $error_message], 500);
        }
      }

  }

  public function destroy(Request $request, $id)
  {

      try {
          $soc = Social::find($id);
      } catch (JWTException $e) {
          $error = $validator->messages()->toJson();
          return response()->json(['success'=> false, 'error'=> $error]), 500;
      }

      if (empty($soc)) {
        return response()->json(['success' => false, 'error' => 'Social profile not found.'], 400);
      }else{
        try {
            $soc->delete();
            return response()->json(['success'=> true, 'message'=> 'Social profile removed correctly from work!!'], 200);
        } catch (Exception $e) {
            $error_message = $e->getMessage();
            return response()->json(['success' => false, 'error' => $error_message], 500);
        }
      }
  }
}
