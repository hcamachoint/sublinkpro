<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;
use App\Models\Location;
use App\Models\User;
use App\Models\Work;

class LocationController extends Controller
{
  public function view($cod){
    try {
        $location = Location::find($cod);
        return response()->json(['success' => true, 'location' => $location], 200);
    } catch (JWTException $e) {
        return response()->json(['success' => false, 'error' => 'Failed to logout, please try again.'], 500);
    }
  }

  public function store(Request $request, $id)
  {
      $rules = [
          'city' => 'required | string | between:1,20',
          'zip' => 'required | string | between:1,10',
          'address' => 'required | string | between:3,200',
          'able' => 'required | integer | between:1,2'
      ];

      $input = $request->only(
          'city',
          'zip',
          'address',
          'able'
      );
      $validator = Validator::make($input, $rules);
      if($validator->fails()) {
          $error = $validator->messages()->toJson();
          return response()->json(['success'=> false, 'error'=> $error], 405);
      }

      if (empty($request->user()->location()->first())) {
        if ($request->able == 1) {
            $able = 'App\Models\User';
            $ac = User::find($id);
            if ($request->user()->id != $id) {
                return response()->json(['success' => false, 'error' => 'Unauthorized action.'], 403);
            }
        }else{
            $able = 'App\Models\Work';
            $ac = Work::find($id);
            if ($request->user()->id != $ac->user->id) {
                return response()->json(['success' => false, 'error' => 'Unauthorized action.'], 403);
            }
        }

        try {
            $loc = new Location;
            $loc->city_id = $request->city;
            $loc->address = $request->address;
            $loc->zip = $request->zip;
            $loc->locationable_id = $id;
            $loc->locationable_type = $able;
            $loc->save();
            return response()->json(['success'=> true, 'message'=> 'Location created correctly!'], 201);
        } catch (Exception $e) {
            $error_message = $e->getMessage();
            return response()->json(['success' => false, 'error' => $error_message], 500);
        }
      }else{
            return response()->json(['success' => false, 'error' => 'An associated location already exists!'], 400);
      }
  }

  public function update(Request $request, $id)
  {
      $rules = [
          'city' => 'required | string | between:1,20',
          'zip' => 'required | string | between:1,10',
          'address' => 'required | string | between:3,200',
          'able' => 'required | integer | between:1,2'
      ];

      $input = $request->only(
          'city',
          'zip',
          'address',
          'able'
      );
      $validator = Validator::make($input, $rules);
      if($validator->fails()) {
          $error = $validator->messages()->toJson();
          return response()->json(['success'=> false, 'error'=> $error], 405);
      }

      if ($request->able == 1) {
          $able = 'App\Models\User';
          $ac = User::find($id);
          $loc = $ac->location()->first();
          if ($request->user()->id != $id) {
              return response()->json(['success' => false, 'error' => 'Unauthorized action.'], 403);
          }
      }else{
          $able = 'App\Models\Work';
          $ac = Work::find($id);
          $loc = $ac->location()->first();
          if ($request->user()->id != $ac->user->id) {
              return response()->json(['success' => false, 'error' => 'Unauthorized action.'], 403);
          }
      }

      try {
          $loc->city_id = $request->city;
          $loc->address = $request->address;
          $loc->zip = $request->zip;
          $loc->save();
          return response()->json(['success'=> true, 'message'=> 'Location updated correctly!'], 200);
      } catch (Exception $e) {
          $error_message = $e->getMessage();
          return response()->json(['success' => false, 'error' => $error_message], 500);
      }
  }
}
