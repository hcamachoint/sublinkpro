<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;
use App\Models\User;
use App\Models\Work;

class WorkController extends Controller
{
  public function view(Request $request, $cod){
    try {
        $work = Work::find($cod);
        if (!empty($work) && ($work->user_id == $request->user()->id)) {
          return response()->json(['success' => true, 'work' => $work, 'proposals' => $work->proposal()->get()], 200);
        }
        return response()->json(['success' => false, 'error' => 'Work not found.'], 400);
    } catch (JWTException $e) {
        $error_message = $e->getMessage();
        return response()->json(['success' => false, 'error' => $error_message], 500);
    }
  }

  public function list(Request $request){
    try {
        $works = $request->user()->work->whereIn('status', [1,2]);
        return response()->json(['success' => true, 'works' => $works], 200);
    } catch (JWTException $e) {
        return response()->json(['success' => false, 'error' => 'Failed to logout, please try again.'], 500);
    }
  }

  public function store(Request $request)
  {
      $rules = [
          'title' => 'required | string | max:100',
          'description' => 'required | string',
          'pstructure' => 'integer | required | between:1,2',
          'startd' => 'required | date',
          'texpect' => 'required | integer | between:1,360',
          'requireds' => 'required | integer | between:1,30',
          'category' => 'integer | required | between:1,4',
          'level' => 'required | integer | between:1,2',
          'project' => 'required | integer | between:1,2',
      ];
      if ($request->pstructure == 1) {
        $rule['budget'] = 'required | numeric | between:0,999999.99';
      }else{
        $rule['budget'] = 'required | numeric | between:8,50.00';
      }
      $input = $request->only(
          'title',
          'description',
          'pstructure',
          'budget',
          'startd',
          'texpect',
          'requireds',
          'category',
          'level',
          'project'
      );
      $validator = Validator::make($input, $rules);
      if($validator->fails()) {
          $error = $validator->messages()->toJson();
          return response()->json(['success'=> false, 'error'=> $error], 405);
      }

      try {
          $var = count($request->user()->work->where('status', 2));
          if ($var < 3) {
              $work = new Work;
              $work->title = $request->title;
              $work->description = $request->description;
              $work->pstructure = $request->pstructure;
              $work->budget = $request->budget;
              $work->startd = $request->startd;
              $work->texpect = $request->texpect;
              $work->requireds = $request->requireds;
              $work->category_id = $request->category;
              $work->project_id = $request->project;
              $work->level_id = $request->level;
              $work->status = 1;
              $work->user_id = $request->user()->id;
              $work->save();
              return response()->json(['success'=> true, 'message'=> 'Post created correctly!'], 201);
          }else{
              return response()->json(['success' => false, 'error' => 'Sorry, you have reached the limit of active works published.'], 400);
          }

      } catch (Exception $e) {
          $error_message = $e->getMessage();
          return response()->json(['success' => false, 'error' => $error_message], 500);
      }
  }

  public function update(Request $request, $id, $cod)
  {
      $rules = [
          'title' => 'required | string | max:100',
          'description' => 'required | string',
          'pstructure' => 'integer | required | between:1,2',
          'startd' => 'required | date',
          'texpect' => 'required | integer | between:1,360',
          'requireds' => 'required | integer | between:1,30',
          'category' => 'integer | required | between:1,4',
          'level' => 'required | integer | between:1,2',
          'project' => 'required | integer | between:1,2',
      ];
      if ($request->pstructure == 1) {
        $rule['budget'] = 'required | numeric | between:0,999999.99';
      }else{
        $rule['budget'] = 'required | numeric | between:8,50.00';
      }
      $input = $request->only(
          'title',
          'description',
          'pstructure',
          'budget',
          'startd',
          'texpect',
          'requireds',
          'category',
          'level',
          'project'
      );
      $validator = Validator::make($input, $rules);
      if($validator->fails()) {
          $error = $validator->messages()->toJson();
          return response()->json(['success'=> false, 'error'=> $error], 405);
      }

      try {
          $work = Work::find($cod);
          if (empty($work)) {
            return response()->json(['success' => false, 'error' => 'Work not found.'], 400);
          }elseif ($work->status == 8 || $work->status == 9) {
            return response()->json(['success' => false, 'error' => 'This work was previously closed!'], 400);
          }else{
            if ($request->user()->id === $work->user_id) {
                if ($work->status == 0 || $work->status == 1) {
                    if (count($work->proposal) == 0) {
                        $work->fill($request->all());
                        $work->save();
                        return response()->json(['success'=> true, 'message'=> 'Your Work has been successfully updated!'], 200);
                    }else{
                        return response()->json(['success' => false, 'error' => 'Your work can not be updated because you have active proposals!'], 400);
                    }
                }elseif($work->status === 2){
                    return response()->json(['success' => false, 'error' => 'I\'m sorry, you can not edit a work that has an active proposal.'], 400);
                }else{
                    return response()->json(['success' => false, 'error' => 'I\'m sorry, you can not edit a work that has already closed or completed.'], 400);
                }
            }else{
                return response()->json(['success' => false, 'error' => 'Unauthorized action.'], 403);
            }
          }
      } catch (Exception $e) {
          $error_message = $e->getMessage();
          return response()->json(['success' => false, 'error' => $error_message], 500);
      }
  }

  public function destroy(Request $request, $cod)
  {
      try {
          $work = Work::find($cod);
          if (empty($work)) {
            return response()->json(['success' => false, 'error' => 'Work not found.'], 400);
          }elseif ($work->status == 8 || $work->status == 9) {
            return response()->json(['success' => false, 'error' => 'This work was previously closed!'], 400);
          }else{
            if ($request->user()->id === $work->user_id) {
                $work->delete();
                return response()->json(['success'=> true, 'message'=> 'Work removed properly!'], 200);
            }else{
                return response()->json(['success' => false, 'error' => 'Unauthorized action.'], 403);
            }
          }
      } catch (Exception $e) {
          $error_message = $e->getMessage();
          return response()->json(['success' => false, 'error' => $error_message], 500);
      }
  }

  public function close(Request $request, $cod)
  {
      try {
          $work = Work::find($cod);
          if (empty($work)) {
            return response()->json(['success' => false, 'error' => 'Work not found.'], 400);
          }elseif ($work->status == 8 || $work->status == 9) {
            return response()->json(['success' => false, 'error' => 'This work was previously closed!'], 400);
          }else{
            if ($request->user()->id === $work->user_id) {
                $work->status = 8;
                $work->save();
                return response()->json(['success'=> true, 'message'=> 'Your Work has been successfully closed!'], 200);
            }else{
                return response()->json(['success' => false, 'error' => 'Unauthorized action.'], 403);
            }
          }
      } catch (Exception $e) {
          $error_message = $e->getMessage();
          return response()->json(['success' => false, 'error' => $error_message], 500);
      }
  }
}
