<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;
use App\Models\User;
use App\Models\Work;
use App\Notifications\PostProposal;
use App\Models\Proposal;
use Illuminate\Support\Facades\Input;

class JobController extends Controller
{
  public function search($text = null, $skill = null)
  {
      //http://localhost:8000/api/job?text=Mi&order=a
      //return [Input::get('text'), Input::get('order')];
      $text = Input::get('text');
      $order = Input::get('order');

      if (empty($text)) {
        $jobs = Work::where('status', 1)->orWhere('status', 2)->orderByDesc('created_at')->get();
      }else{
        $jobs = Work::where('title', 'LIKE', '%'.$text.'%')->orWhere('description', 'LIKE', '%'.$text.'%')->where('status', 1)->orderByDesc('created_at')->get();
      }
      return response()->json(['success' => true, 'jobs' => $jobs], 200);
  }

  public function view($cod){
    try {
        $work = Work::find($cod);
        if (!empty($work) && ($work->status == 8 || $work->status == 9)) {
          return response()->json(['success' => false, 'error' => 'The job you are looking for has already been closed / finished.'], 400);
        }
        return response()->json(['success' => true, 'work' => $work], 200);
    } catch (JWTException $e) {
        $error_message = $e->getMessage();
        return response()->json(['success' => false, 'error' => $error_message], 500);
    }
  }

  public function proposal(Request $request, $cod)
  {
    $rules = [
        'message' => 'required | string'
    ];
    $input = $request->only(
        'message'
    );
    $validator = Validator::make($input, $rules);
    if($validator->fails()) {
        $error = $validator->messages()->toJson();
        return response()->json(['success'=> false, 'error'=> $error], 405);
    }

    try {
        $work = Work::find($cod);
        $prop = $work->proposal->where('user_id', $request->user()->id)->first();

        if(empty($prop) && ($work->status == 1 || $work->status == 2)){
            $proposal = new Proposal;
            if ($request->user()->id !== $work->user_id) {
                $proposal->fill($request->all());
                $proposal->user_id = $request->user()->id;
                $proposal->work_id = $work->id;
                $proposal->save();
                if ($work->status == 1) {
                  $work->status = 2;
                }
                $work->save();

                $user = User::find($work->user_id);
                $user->notify(new PostProposal($proposal));
                return response()->json(['success'=> true, 'message'=> 'Your Proposal has been sended successfully!'], 201);
            }else{
                return response()->json(['success' => false, 'error' => 'You can not send a proposal to your own work.'], 400);
            }

        }else{
            return response()->json(['success' => false, 'error' => 'Sorry, you already have an active proposal in this work.'], 400);
        }

    } catch (Exception $e) {
        $error_message = $e->getMessage();
        return response()->json(['success' => false, 'error' => $error_message], 500);
    }
  }
}
