<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use DB;
//PARA LA API
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;
//PARA LA IMAGEN
use Image;


class ProfileController extends Controller
{
    public function update(Request $request){
        $rules = [
            'name' => 'required|max:100',
        ];
        $input = $request->only(
            'name',
        );

        $validator = Validator::make($input, $rules);
        if($validator->fails()) {
            $error = $validator->messages()->toJson();
            return response()->json(['success'=> false, 'error'=> $error], 405);
        }

        $user = $request->user();
        $user->fill($request->all());
        $user->save();

        return response()->json(['success'=> true, 'message'=> 'Profile Updated!'], 200);
    }

    public function desactive(Request $request){
        $user = $request->user();
        $user->delete();
        return response()->json(['success'=> true, 'message'=> 'Profile Deleted!'], 200);
    }

    public function update_picture(Request $request)
    {
        $rules = [
            'avatar' => 'required | dimensions:min_width=100,min_height=100 | image',
        ];
        $input = $request->only(
            'avatar'
        );

        $validator = Validator::make($input, $rules);
        if($validator->fails()) {
            $error = $validator->messages()->toJson();
            return response()->json(['success'=> false, 'error'=> $error], 405);
        }

        try {
            $user = $request->user();
            if($request->hasFile('avatar')){
                $avatar = $request->file('avatar');
                $filename = hash('ripemd160', $request->user()). '_' . time() . '.' . $avatar->getClientOriginalExtension();
                Image::make($avatar)->resize(300, 300)->save( public_path('avatars/' . $filename ) );

                try {
                    $user = $request->user();
                    $user->image_path = $filename;
                    $user->save();
                    return response()->json(['success'=> true, 'message'=> 'Profile photo Updated!'], 200);
                } catch (Exception $e) {
                    $error_message = $e->getMessage();
                    return response()->json(['success' => false, 'error' => $error_message], 500);
                }
            }
        } catch (Exception $e) {
            $error_message = $e->getMessage();
            return response()->json(['success' => false, 'error' => $error_message], 500);
        }
    }
}
