<?php

namespace App\Http\Controllers;

use App\Models\Briefcase;
use Illuminate\Http\Request;
use Session;
use Redirect;
use App\Http\Controllers\Controller;

class BriefcaseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
  
    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required | string | between:1,30',
            'description' => 'required | string',
            'date' => 'required | date',
        ]);

        if ($request->input('url')) {
            $this->validate($request, [
                'url' => 'required | url',
            ]);
        }

        if (auth()->user()->id != $id) {
            return abort(403, 'Unauthorized action.');
        }

        try {
            $bri = new Briefcase;
            $bri->fill($request->all());
            $bri->user_id = $id;
            $bri->save();

            Session::flash('message', 'Study assigned correctly!');
            return Redirect::back();
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request.');
            return Redirect::back();
        }
        return abort(403, 'Unauthorized action.');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required | string | between:1,30',
            'description' => 'required | string',
            'date' => 'required | date',
        ]);

        if ($request->input('url')) {
            $this->validate($request, [
                'url' => 'required | url',
            ]);
        }

        $bri = Briefcase::find($id);
        if (auth()->user()->id != $bri->user_id) {
            return abort(403, 'Unauthorized action.');
        }

        try {
            $bri = Briefcase::find($id);
            if (auth()->user()->id === $bri->user_id) {
                $bri->fill($request->all());
                $bri->save();
                Session::flash('message', 'Briefcase updated properly!');  
                return Redirect::back();
            }else{
                return abort(403, 'Unauthorized action.');
            }
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request');
            return Redirect::back();
        }
    }

    public function destroy($id)
    {
        $bri = Briefcase::find($id);
        if (auth()->user()->id != $bri->user_id) {
            return abort(403, 'Unauthorized action.');
        }

        try {
            $bri = Briefcase::find($id);
            if (auth()->user()->id === $bri->user_id) {
                $bri->delete();
                Session::flash('message', 'Briefcase removed properly!');
                return Redirect::back();
            }else{
                return abort(403, 'Unauthorized action.');
            }
        } catch (Exception $e) {
            Session::flash('message-error', 'An error occurred while trying to process your request.');
            return Redirect::back();
        }
    }
}
